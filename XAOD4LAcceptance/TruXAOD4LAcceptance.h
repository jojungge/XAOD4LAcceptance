#ifndef TruxAOD4LAcceptance_h
#define TruxAOD4LAcceptance_h

//#include <XAOD4LObjectSelection/xAOD4LObjectSelection.h>
#include <MC15XAOD4LSelection/xAOD4LSelection.h>
class TruxAOD4LAcceptance: public xAOD4LSelection 
{
 protected:
	
   virtual void AnalyzeTruthEvents();   
   virtual void AnalyzeRecoEvents() {GetReco=false;};
   void AnalyzeSUSYParticles();
   void GetNumLepFromVDecay ( std::vector<const xAOD::TruthParticle*> &Lep , unsigned int &NumW , unsigned int &NumZ);
   
   void InitParticleHistograms (  std::string Pre , std::string Particle ,  std::string xTitle, HistManager::DefinedHistos PtType = HistManager::DefinedHistos::low_pt ,HistManager::DefinedHistos MType=HistManager::DefinedHistos::low_pt );
		
   void InitTruthHistos();
   void InitRecoHistos() {};
   void InitCommonHistos(const std::string &Pre);
   const xAOD::TruthParticle* GetLastInChain ( const xAOD::TruthParticle* Part );
   template <typename PartType> void FillParticleHistograms(const std::vector<PartType*> &Particle,const std::string &Name, const double &w=1.);
   void FillLeptonHistograms( std::vector<const xAOD::TruthParticle*> &Lep, std::vector<const xAOD::TruthParticle*> &Photons, const std::string &Name , const double &w=1., xAOD4L::SR SR=xAOD4L::SR::nDef );

   void FillParticleHistograms (const xAOD::IParticle* Particle,const std::string &Name,const double &w=1.);
   void FillParticleHistograms (TLorentzVector Momentum ,const std::string &Name,const double &w=1.);
   void FillDecayHistogram (const xAOD::TruthParticle* Particle, const std::string &Name,const double &w=1.);
   void FillMeffHistograms(const std::string &Pre, const double &Met, const double &Meff, const double &MeffLep, const double &MeffJet, const double &MeffPh, const double &w);
   void PrintAllChildren(const xAOD::TruthParticle* Part, std::string AddInfo="" , int Step=0);
   void FillDRHistograms(const std::string &Pre , std::vector <const xAOD::TruthParticle*> &El,   std::vector <const xAOD::TruthParticle*> &Mu,  std::vector <const xAOD::TruthParticle*> &Tau,  std::vector <const xAOD::Jet*> &Jet, std::vector <const xAOD::TruthParticle*> & Ph, double w=1.);
   void FillDarlitzHistograms ( const xAOD::TruthParticle* LSP);
   void DrellYangVeto (std::vector<const xAOD::IParticle*> &BaseLep , const std::string &Pre , double &w);
   bool Is4LeptonRegion (xAOD4L::SR SR);
public:
  TruxAOD4LAcceptance ();
  virtual void SetOption(const std::string Option, bool Switch);
  virtual void PromptOptions();
  ~TruxAOD4LAcceptance ();
 protected:
		bool GetW = true;
		bool GetZ = true;
		bool GetPhoton = false;
		bool GetHiggs = false;
		bool UseXSection = true;
};
#endif // #ifdef TruxAOD4LAcceptance_cxx
