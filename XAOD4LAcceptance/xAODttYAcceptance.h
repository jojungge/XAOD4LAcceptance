#ifndef XAODttYAcceptance_h
#define xAODttYAcceptance_h

//#include <XAOD4LObjectSelection/xAOD4LObjectSelection.h>
#include <MC15XAOD4LSelection/xAOD4LSelection.h>

class xAODttYAcceptance: public xAOD4LSelection 
{
 protected:
   bool IsBgSample = false;
   float ME_PH_MIN_PT = 15*GeVToMeV;
   float ME_INCLUSIVE_PT =40.*GeVToMeV;
   unsigned int MaxNumBJets=2;
   std::vector<const xAOD::Jet*> B_PreJets;
   std::vector<const xAOD::Jet*> B_BaseJets;
   std::vector<const xAOD::Jet*> B_SignalJets;
   void RetrieveBJets();
   std::string GetRegion(const xAOD::IParticle* Ph);
   virtual void AnalyzeTruthEvents();   
   virtual void AnalyzeRecoEvents();  
   virtual void InitTruthHistos();
   virtual void InitRecoHistos();
   virtual bool InitTools();
   void InitCommonHistos(const std::string &Pre, bool InitBJets=true);
   template <typename PartType> void FillParticleHistograms(const std::vector<PartType*> &Particle,const std::string &Name, const double &w=1.);
   void FillParticleHistograms (const xAOD::IParticle* Particle,const std::string &Name,const double &w=1.);
   void FillParticleHistograms (TLorentzVector Momentum ,const std::string &Name,const double &w=1.);
   
   void FillMeffHistograms(const std::string &Pre, const double &Met, const double &Meff, const double &MeffLep, const double &MeffJet, const double &MeffPh, const double &w);
   bool IsMEPPhoton(const xAOD::TruthParticle* Gamma ,float MinPt=0.);
   bool IsttYEvent(const std::vector <const xAOD::TruthParticle*> &Photons);
   bool IsttYEvent ( const xAOD::TruthParticle* Ph);
   bool CheckttYEvent();
   bool IsttYSample=false;
   bool IsttbarSample = false;
  
public:
  xAODttYAcceptance ();
  virtual void SetOption(const std::string Option, bool Switch);  
  
  virtual void PromptOptions();
  ~xAODttYAcceptance ();   
};
#endif // #ifdef XAOD4LAcceptance_cxx
