#ifndef XAOD4LAcceptance_h
#define XAOD4LAcceptance_h

//#include <XAOD4LObjectSelection/xAOD4LObjectSelection.h>
#include <MC15XAOD4LSelection/xAOD4LSelection.h>
class xAOD4LAcceptance: public xAOD4LSelection 
{
 protected:

		virtual void AnalyzeTruthEvents();   
		virtual void AnalyzeRecoEvents();
		virtual void Execute();
		void WriteEventNumberInFile();
		std::string WriteParticleInformation(const xAOD::IParticle* P);
   
		
		void DrellYangVeto ( std::vector<const xAOD::IParticle*> &BaseLep  , const std::string &Pre , double &w);
		void InitTruthHistos();
		void InitRecoHistos();
		void InitCommonHistos(const std::string &Pre);
		void InitParticleHistograms (  std::string Pre , std::string Particle ,  std::string xTitle, HistManager::DefinedHistos PtType = HistManager::DefinedHistos::low_pt );
		
		template <typename E , typename M , typename T , typename P> void AnalyzeCommonEvent ( const std::string &EvType , std::vector<const E*> &El , 
																							std::vector <const M*> &Mu , std::vector<const xAOD::Jet*> &Jet , std::vector <const T*> & Tau , 
																							std::vector <const P*> &Ph , const xAOD::MissingET* MET , bool IsReco=true , bool RecalcEv = false);
																				 
		
		 void FillBosonHistogram ( const std::string &Pre , std::vector<const xAOD::IParticle*> &Lep , const double & Meff , xAOD4L::SR SR  , bool IsReco=true);
		 
		 template <typename PartType> void FillParticleHistograms(const std::vector<PartType*> &Particle,const std::string &Name, const double &w=1.);
		 void FillParticleHistograms (const xAOD::IParticle* Particle,const std::string &Name,const double &w=1.);
		 void FillParticleHistograms (TLorentzVector Momentum ,const std::string &Name,const double &w=1.);
   
		 void FillIsolationHistograms (const std::string &Name , const xAOD::IParticle* P);
  
		 void FillIsolationHistogram (const  std::string &IsoName , float Iso ,  const std::string &ConeName ,  float Cone , bool Pass4Lep);
		 void FillIsolationHistogram (const  std::string &IsoName , float Cone , float Et,  float dR, bool Pass4Lep , const std::string &NearPart);
		 
		 void FillMeffHistograms(const std::string &Pre, const double &Met, const double &Meff, const double &MeffLep, const double &MeffJet, const double &MeffPh, const double &w);
		 
		 void GetNumLepFromVDecay ( std::vector<const xAOD::TruthParticle*> &Lep , unsigned int &NumW , unsigned int &NumZ);
   
		 template <typename E, typename M, typename T, typename J > void FillDRHistograms(const std::string &Pre , std::vector <E> &El,  std::vector <M> &Mu,  std::vector <T> &Tau, std::vector <J> &Jet, double w=1.);
		 
		 bool Is4LeptonRegion (xAOD4L::SR SR);
		 const xAOD::TruthParticle* MatchByBarCode ( const xAOD::TruthParticle* P);		 
		 
public:
		 xAOD4LAcceptance ();
		 virtual void SetOption(const std::string Option, bool Switch);
		 virtual void PromptOptions();
		 ~xAOD4LAcceptance ();
 protected:
		std::vector<const xAOD::TrackParticle*> Tracks;
		bool GetW = false;
		bool GetZ = false;
		bool GetPhoton = false;
		bool GetHiggs = false;
		
		bool EvTrigger = false;
		bool GetRunIRegions = false;
		bool GetRunIIRegions = true;
		std::vector<const xAOD::IParticle*> rec_NonIsoLep;
		
	};
#endif // #ifdef XAOD4LAcceptance_cxx
