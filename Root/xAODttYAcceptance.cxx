//Class Header
#include <XAOD4LAcceptance/xAODttYAcceptance.h>
#define XAODttYAcceptance_cxx
xAODttYAcceptance::xAODttYAcceptance ():
	xAOD4LSelection()	
	{}
xAODttYAcceptance::~xAODttYAcceptance()
	{}
bool xAODttYAcceptance::CheckttYEvent()
	{
	  if ( IsttYSample ) return IsttYEvent(truth_PrePh);
	  else if (IsttbarSample) return !IsttYEvent(truth_PrePh);
	  return ( IsBgSample || !isMC);   
	}
bool xAODttYAcceptance::InitTools()
	{
	 if ( !isMC ) 		 IsttYSample = IsttbarSample = false; //One runs over data
	 if ( IsttYSample && IsttbarSample)
		{
		 Error("xAOD4LttYAcceptance" , "The sample is ttbar and ttY at the same time");
		 return false;
		}
	return xAOD4LSelection::InitTools();
	}
void xAODttYAcceptance::AnalyzeRecoEvents()
	 {
	  static int MeffCuts[]={300,600,700,800,900,1000,1100,1200};

	  static MCTruthPartClassifier::ParticleDef PDef;
	  RetrieveBJets();
	  const xAOD::IParticle* LeadPh = FindLeadingPtParticle(rec_ph);
	  const xAOD::TruthParticle* TruLeadPh =  NULL;
	  bool PassBgSelection = IsBgSample;
	  if (LeadPh!=NULL)  
		{
		 TruLeadPh = TruthParticleHelpers::getTruthMatchedParticle(LeadPh);
		 if (IsBgSample)
			{
			 int Orig = TruthParticleHelpers::getParticleTruthOrigin(LeadPh);
			 if (SampleNr== 301535 || SampleNr==301536 || (SampleNr>=301899 &&  SampleNr<=301907) )  //Z+Gamma+Jets Sherpa
				{
				 PassBgSelection =( MCTruthPartClassifier::ParticleOrigin::ZBoson==Orig  || MCTruthPartClassifier::ParticleOrigin::SinglePhot == Orig );
				}
			 else if (SampleNr>=361500  && SampleNr<=361514 ) //Z+Jets MadGraph
				{
				 if  ( MCTruthPartClassifier::ParticleOrigin::ZBoson==Orig ||  MCTruthPartClassifier::ParticleOrigin::WBoson==Orig ) PassBgSelection=LeadPh->pt()<1.e4;
				 else if (MCTruthPartClassifier::ParticleOrigin::FSRPhot == Orig) PassBgSelection = !IsMEPPhoton(TruLeadPh , 1.e4);					
				}
			 else if (SampleNr>=361100 &&  SampleNr<=361105 ) // W+Jets PowHeg
				{
				  if  ( MCTruthPartClassifier::ParticleOrigin::ZBoson==Orig ||  MCTruthPartClassifier::ParticleOrigin::WBoson==Orig ) PassBgSelection=LeadPh->pt()<1.e4;
				  else if (MCTruthPartClassifier::ParticleOrigin::FSRPhot == Orig) PassBgSelection = !IsMEPPhoton(TruLeadPh , 1.e4);					
				}
			 else if (SampleNr>=301890 &&  SampleNr<=301898 ) // W+Gamma+Jets Sherpa
				{
				 PassBgSelection = ( MCTruthPartClassifier::ParticleOrigin::WBoson==Orig  || MCTruthPartClassifier::ParticleOrigin::SinglePhot == Orig );
				}
			  std::cout<<PDef.sParticleType.at(TruthParticleHelpers::getParticleTruthType(LeadPh)) <<
		  " "<<PDef.sParticleOrigin.at(TruthParticleHelpers::getParticleTruthOrigin(LeadPh))<<std::endl;			
			}		
		}
	  bool PassME = ( !isMC || PassBgSelection ||   IsttYEvent(TruLeadPh) == IsttYSample );	
	  
	  bool PassSelection = PassEventSelection() && LeadPh!=NULL && PassME;
	  
	  std::string  SR = "";
	  bool nDef=true;
	  SRCounter->NewEvent();
	  while( SR.size() > 0 || nDef==true)
		{
		  if(PassSelection == true)  SR = GetRegion(LeadPh);
		  else SR="";	 
		  if (SR.size() == 0) nDef=false;
		  std::string Pre ="rec_"+(SR.size() > 0 ? SR+"-" : "");
		  FillMeffHistograms(Pre, rec_MET->met(), Reco_Meff, Reco_MeffLep, Reco_MeffJet, Reco_MeffPh,reco_weight);
		  Histos->FillHistogram ( Pre+"PileUp", eventInfo->averageInteractionsPerCrossing() , reco_weight);	 	 
		 
		  Histos->FillNumHistogram(Pre+"NumJet" , Reco_NumMeffJet , reco_weight);
		  Histos->FillNumHistogram(Pre+"NumBJet" , B_SignalJets.size() , reco_weight);
		  FillParticleHistograms(rec_el,Pre+"el",reco_weight);
		  FillParticleHistograms(rec_mu,Pre+"mu",reco_weight);
		  FillParticleHistograms(rec_jets,Pre+"jet",reco_weight);
		  FillParticleHistograms(B_SignalJets,Pre+"bjet",reco_weight);
		  FillParticleHistograms(rec_ph,Pre+"aY",reco_weight);
		  if (LeadPh!=NULL)	
			{
			 FillParticleHistograms(LeadPh,Pre+"Y", reco_weight);
			  for(int i=0; SR!=xAOD4L::SR::nDef && i<8;++i) if(Reco_Meff > MeffCuts[i]*GeVToMeV)FillParticleHistograms(LeadPh,Form("%sY_MeffC_%i",Pre.c_str(),MeffCuts[i]),reco_weight);
			 //~ Histos->FillHistogram(Pre+"Y-pt-Meff",LeadPh->pt() / GeVToMeV , Reco_Meff/ GeVToMeV , weight);
			}
		}
	  //if(SaveTrees==true && CheckLeptonTrigger(rec_PreEl, rec_PreMu)==true) TreeMgr->Save();	  
	}
void xAODttYAcceptance::AnalyzeTruthEvents()
	{
	 if (!CheckttYEvent() ) return;
	 //~ for (std::vector<const xAOD::TruthParticle*>::const_iterator P = truth_ph.begin() ; P!=truth_ph.end() ; ++P)
		//~ {
		 //~ float dR =  DeltaR ( *P , GetNearestParticle(*P , truth_ph, truth_ph));
		 //~ if ( dR>-1. ) PrintAllParents( *P , Form("IsME: %i dR: %.2f", IsMEPPhoton(*P) , dR) );
		//~ }
	 const xAOD::IParticle* LeadPh = FindLeadingPtParticle(truth_ph);
	 bool PassSelection = PassLeptonTrigger( truth_el , truth_mu );
	 xAOD4L::SR SR=xAOD4L::SR::nDef;
	 
	  bool nDef=true;
	  SRCounter->NewEvent();					 
	  while( SR!=xAOD4L::SR::nDef || nDef==true)
		{
		  if(PassSelection==true) 	  SR = SRCounter->GetRegion(truth_Lep ,truth_tau , truth_MET , truth_Meff, truth_ZVeto4Lep , truth_ZVeto3Lep, truth_ZVeto2Lep);
		  if (SR==xAOD4L::SR::nDef) nDef=false;
		  std::string Pre ="tru_"+(SR!=xAOD4L::SR::nDef ? PrintSignalRegion(SR)+"-" : "");
		  FillParticleHistograms(truth_el , Pre+"el" , weight);
		  FillParticleHistograms(truth_mu , Pre+"mu" , weight);
		  FillParticleHistograms(truth_jets , Pre+"jet", weight);
		  if ( LeadPh!=NULL ) FillParticleHistograms(LeadPh,Pre+"Y", weight);		
		}
	}
void xAODttYAcceptance::InitTruthHistos( )
	{
	 InitCommonHistos("tru",false);	
	}
std::string xAODttYAcceptance::GetRegion ( const xAOD::IParticle* Ph)
	{
	 static Long64_t Ev=0;
	 static unsigned int Counter=0;
	 static unsigned int B = 1;
	 static xAOD4L::SR  R= xAOD4L::SR::nDef;
	 static bool ExtendedZVeto=true;
	 unsigned int NumB = B_SignalJets.size();
	 unsigned int NewR = 1 + TMath::Min ( NumB , MaxNumBJets )* (rec_jets.size() >= 2) ;
	 if ( Ev!=readEvents )
		{
		 Ev=readEvents;
		 Counter=0;
		 SRCounter->NewEvent();
		 ExtendedZVeto = Reco_ZVeto2Lep;	
		 for ( std::vector<const xAOD::IParticle*>::const_iterator L = rec_Lep.begin() ; ExtendedZVeto &&L!=rec_Lep.end() ; ++L)
			{
			 for ( std::vector<const xAOD::IParticle*>::const_iterator L1 = L+1  ; ExtendedZVeto && L1!=rec_Lep.end() ; ++L1)
				{
				 if ( !SFOS( *L , *L1) ) continue;
				 float TriMass= TMath::Abs ( ThreePartInvariantMass(*L,*L1,Ph) - xAOD4L::Z_MASS);
				 if ( TriMass < xAOD4L::Z_MASSWINDOW ) ExtendedZVeto = false;
				}
			}
		}
	std::string SR="";
	if (Counter % NewR == 0)  
		{
		 R = SRCounter->GetRegion(rec_Lep ,rec_tau , rec_MET , Reco_Meff, ExtendedZVeto , ExtendedZVeto, ExtendedZVeto);
		 B=0;
		}
	if ( xAOD4LEventRegions::Is2LeptonRegion(R) && rec_Lep.size()==2 && GetLeptonEventCharge(false) == 0 )
		{
		 if (Counter % NewR == 0 ) SR = SRCounter->PrintSignalRegion(R);
		 else 	SR = Form ("%s%uB" , SRCounter->PrintSignalRegion(R).c_str() ,  B );
		}		
	++Counter;
	if (NumB > B && B< MaxNumBJets) ++B;
	return SR;
	}
void xAODttYAcceptance::InitCommonHistos(const std::string &Pre , bool InitBJets)
	{
	  int MeffCuts[]={300,600,700,800,900,1000,1100,1200};
	 std::string Particle[]={"el","mu","jet","bjet","Y", "aY"};
	 std::string ParTitle[]={"e","#mu","Jets","B-Jets","#gamma" , "#gamma"};
	
	 
	 for(unsigned int a=0;a<6;++a)
		{
		 if (!InitBJets && a==3) continue;
		 for (int b=xAOD4L::SR::nDef;b!=xAOD4L::SR::SR0;++b)
			{
			 xAOD4L::SR R=(xAOD4L::SR)b;
		 	 for ( unsigned int c= ( R!=xAOD4L::SR::nDef ? 1 : MaxNumBJets + 1)  ; c < MaxNumBJets + 2 ; ++c)
				{
				 if(!InitBJets && c < MaxNumBJets + 1 ) continue;
				 std::string BR = (c < MaxNumBJets + 1 ? Form("%uB" , c) : "" );
				 std::string SR =Pre+"_"+(R!=xAOD4L::SR::nDef ? PrintSignalRegion(R)+BR+ "-" : "");	
				 Histos->CreateNewHisto(HistManager::DefinedHistos::lowmed_pt,Form("%s%s-pt",SR.c_str(),Particle[a].c_str()),Form("P_{T} of the selected %s",ParTitle[a].c_str()),Form("p_{T}(%s) [GeV]",ParTitle[a].c_str()));
				}
			}
		}
	for (int b=xAOD4L::SR::nDef;b!=xAOD4L::SR::SR0;++b)
		{
		 xAOD4L::SR R=(xAOD4L::SR)b;
		 for ( unsigned int c= ( R!=xAOD4L::SR::nDef ? 1 : MaxNumBJets + 1)  ; c < MaxNumBJets + 2 ; ++c)
			{
			  if(!InitBJets && c < MaxNumBJets + 1 ) continue;
			 std::string BR = (c < MaxNumBJets + 1 ? Form("%uB" , c) : "" );
			 std::string SR =Pre+"_"+(R!=xAOD4L::SR::nDef ? PrintSignalRegion(R)+BR+ "-" : "");	
		     for(unsigned int m=0; m<8;++m)   Histos->CreateNewHisto(HistManager::DefinedHistos::lowmed_pt ,Form ("%sY_MeffC_%i-pt", SR.c_str(), MeffCuts[m]) , "p_{T}","p_{T}(#gamma) [GeV]");	
			 Histos->CreateNewHisto (HistManager::DefinedHistos::PileUp , SR+"PileUp","PileUp"); 
			 Histos->CreateNewHisto(HistManager::DefinedHistos::met,SR+"Met","E^{miss}_{T}");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Meff-Met","M_{eff}  E^{miss}_{T} in 4l events","M_{eff} [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Ht-Met","H_{T}  E^{miss}_{T} in 4l events","H_{T} [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff","M_{eff} in 4l events", "m_{eff}^{#gamma} [GeV]");		 
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Lep","M_{eff} contribution of the Leptons in 4l events","H_{T} (Leptons) [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Jet","M_{eff} contribution of the Jets in 4l events","H_{T} (Jets) [GeV]");
			 if(UseExtendedMeff==true) Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Y","M_{eff} contribution of the #gamma","H_{T} (#gamma) [GeV]");		
			 Histos->CreateNewHisto(HistManager::DefinedHistos::NumJet,SR+"NumJet", "Number of Jets event");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::NumJet,SR+"NumBJet", "Number of Jets event");		
			}
		}
	}
void xAODttYAcceptance::InitRecoHistos()
	{
	 InitCommonHistos("rec");	
	}
void xAODttYAcceptance::FillMeffHistograms(const std::string &Pre, const double &Met, const double &Meff, const double &MeffLep, const double &MeffJet, const double &MeffPh, const double &w)
	{
	 Histos->FillHistogram (Pre+"Meff-Met",Meff/GeVToMeV,Met/GeVToMeV,w);
	 Histos->FillHistogram (Pre+"Ht-Met",(Meff-Met)/GeVToMeV,Met/GeVToMeV,w);	 
	 Histos->FillHistogram (Pre+"Met",Met/GeVToMeV,w);	
	 Histos->FillHistogram (Pre+"Meff",Meff/GeVToMeV,w);	 
	 Histos->FillHistogram (Pre+"Meff-Lep",MeffLep/GeVToMeV,w);
	 Histos->FillHistogram (Pre+"Meff-Jet",MeffJet/GeVToMeV,w);
	 if(UseExtendedMeff==true) Histos->FillHistogram (Pre+"Meff-Y",MeffPh/GeVToMeV,w);
	}
template <typename PartType> void xAODttYAcceptance::FillParticleHistograms(const std::vector<PartType*> &Particle,const std::string &Name, const double &w)
	{
	 for (size_t p=0;p< Particle.size();++p) FillParticleHistograms (Particle.at(p),Name,w);	
	}
void xAODttYAcceptance::FillParticleHistograms (const xAOD::IParticle* Particle,const std::string &Name,const double &w)
	{
	 FillParticleHistograms(Particle->p4(),Name,w);	
	}
void xAODttYAcceptance::FillParticleHistograms (TLorentzVector Momentum ,const std::string &Name,const double &w)
	{
	 Histos->FillHistogram (Name+"-pt",Momentum.Pt()/GeVToMeV,w);
	 //Histos->FillHistogram (Name+"-eta",Momentum.Eta(),w);
	// Histos->FillHistogram (Pre+Name+"-phi",Momentum.Phi(),w);		
	}
void xAODttYAcceptance::SetOption(const std::string Option, bool Switch)
	{
	 xAOD4LSelection::SetOption(Option,Switch);
	 UseExtendedMeff=true;
	 Get4LeptonsEvents=false;
	 Get2LeptonsEvents=true;	 
	 if (Option == "IsttY") IsttYSample=Switch;
	 else if (Option == "Isttbar") IsttbarSample=Switch;
	 else if (Option == "IsBg" ) IsBgSample=Switch; 	
	}
void xAODttYAcceptance::PromptOptions()
	{
	 xAOD4LSelection::PromptOptions();
	 Info ("xAOD4LttYAcceptance" , "Is ttbar:	%s" , BoolToString(IsttbarSample).c_str());
	 Info ("xAOD4LttYAcceptance" , "Is ttY:	%s" , BoolToString(IsttYSample).c_str());
	 Info ("xAOD4LttYAcceptance" , "Is BG:	%s" , BoolToString(IsBgSample).c_str());
	 		
	}

void xAODttYAcceptance::RetrieveBJets()
	{
	 B_SignalJets.clear();
	 B_PreJets.clear();
	 B_BaseJets.clear();
 	 for ( xAOD::JetContainer::const_iterator J = JetContainer->begin(); J!=JetContainer->end() ; ++ J)
		{
		 if ( !PassPreSelection(*J) || !(*J)->isAvailable<char>("bjet_loose") || !(*J)->auxdata<char>("bjet_loose") ) continue;
		 B_PreJets.push_back(*J);
		 if (! PassOverlapRemoval(*J) ) continue;
		 B_BaseJets.push_back(*J);
		 if (SUSYTools->IsBJet(**J) )B_SignalJets.push_back(*J);
		}
	if ( isMC ) reco_weight *= SUSYTools->BtagSF(JetContainer); 
	}
bool xAODttYAcceptance::IsMEPPhoton(const xAOD::TruthParticle* Gamma ,float MinPt)
	{
	 if(Gamma==NULL || Gamma->isPhoton()==false)
		{
		 //~ Warning("xAODttYAcceptance", " In Event: %lli: the given particle is not a photon" , readEvents);
		 return false;
		}
	 if   (Gamma->pt() <  MinPt) return false;
	 for(size_t p=0;p< Gamma->nParents();++p)
		{
		 if(Gamma->parent(p)->isPhoton()==true) return IsMEPPhoton(Gamma->parent(p));
		 else if(TruthParticleHelpers::IsBoson(Gamma->parent(p) )== true) return TruthParticleHelpers::CheckBosonOrigin(Gamma->parent(p)); 
		 else if(Gamma->parent(p)->isChLepton()==true || Gamma->parent(p)->isQuark()==true) return TruthParticleHelpers::IsPromptParticle(Gamma->parent(p));
		 else if( TruthParticleHelpers::isGluon(Gamma->parent(p)) )return true;	 //Gluon	
		 else if( Gamma->parent(p)->absPdgId()==2212 )return true; //WTF?! Proton?!		
		}
	return false;
	}
bool xAODttYAcceptance::IsttYEvent(const std::vector<const xAOD::TruthParticle*> &Photons)
	{
	 // https://indico.cern.ch/event/319996/contribution/2/material/slides/0.pdf
	 for (std::vector<const xAOD::TruthParticle*>::const_iterator P=Photons.begin() ; P!=Photons.end() ; ++P )  if (IsttYEvent(*P)) return true;
	 return false;
	}
bool xAODttYAcceptance::IsttYEvent ( const xAOD::TruthParticle* Ph)
	{
	 if (Ph==NULL || !Ph->isPhoton() ) return false;
	 //~ Warning("xAODttYAcceptance", " In Event: %lli: the given particle is not a photon" , readEvents);		
	 if (IsMEPPhoton(Ph , ME_PH_MIN_PT) )  return  (SampleNr!=410082 || Ph->pt() < ME_INCLUSIVE_PT );
	 return false;
	}
