//Class Header
#include <XAOD4LAcceptance/TruXAOD4LAcceptance.h>
#define TruTruxAOD4LAcceptance_cxx

TruxAOD4LAcceptance::TruxAOD4LAcceptance ():
	xAOD4LSelection()	
	{
	SaveTrees=false;
	GetReco=false;
	}
void TruxAOD4LAcceptance::AnalyzeTruthEvents()
	{
	if (UseXSection==false) 
		{
		 weight=1.;
		 if (ContainsSUSY==true)
			{
			 Histos->SetSUSYProcess(1, weight);
			 Histos->NewEvent();		 
			}
		}
	truth_PreTau.clear();
	truth_BaseTau.clear();
	truth_tau.clear();
	
	 std::vector<const xAOD::TruthParticle*>::const_iterator P;	 
	 AnalyzeSUSYParticles();
	 const xAOD::IParticle* LeadPh=NULL;
	 unsigned int NumElW(0) , NumElZ(0) , NumMuZ(0) , NumMuW(0) , NumTauW(0) , NumTauZ(0);
	 GetNumLepFromVDecay(truth_PreEl, NumElW, NumElZ);
	 GetNumLepFromVDecay(truth_PreMu, NumMuW, NumMuZ);
	 GetNumLepFromVDecay(truth_PreTau, NumTauW, NumTauZ);
	
	 if (GetZ==true)
		{
		 for (P =truth_Z.begin(); P!=truth_Z.end();++P)
			{
			 int ZNumLep(0);		 
			 FillParticleHistograms(*P, "tru_Z",weight);			
			 FillDecayHistogram(*P,"tru_Z-Children",weight);
			 for (size_t c=0 ; c < (*P)->nChildren() ; ++c) if ( (*P)->child(c)->isElectron() || (*P)->child(c)->isMuon() ) ++ZNumLep;
			 if (ZNumLep > 0 && (NumElZ+NumMuZ) < 1 ) PrintAllChildren(*P);
			}
		 
		}
	 if (GetW==true )	
		{
		 for (P =truth_W.begin(); P!=truth_W.end();++P)
			{
			 FillParticleHistograms(*P, "tru_W",weight);			
			 FillDecayHistogram(*P,"tru_W-Children",weight);
			 //~ if (NumElW + NumMuW + NumTauW < 1 )
				//~ {
			     //~ bool HasLepChild=false;
			     //~ for(size_t c =0 ; c< (*P)->nChildren(); ++c) 
					//~ {
					 //~ if ( (*P)->child(c)->isChLepton()) HasLepChild=true;
					 //~ if ( (*P)->child(c)->isTau()) HasLepChild=false;
					//~ 
					//~ }
			     //~ if(HasLepChild)PrintAllChildren(*P);
				//~ }
			}
		}	
	 //~ if ( ContainsSUSY==false && NumMuZ < 2) return;	
	 std::vector<const xAOD::IParticle*> BaseLep =  BuildLeptonVector(truth_BaseEl , truth_BaseMu);
	 DrellYangVeto ( BaseLep, "tru" , weight);
	 if ( GetPhoton==true ) LeadPh=FindLeadingPtParticle(truth_ph);
	 std::vector<const xAOD::TruthParticle*> CandidateEl;
	 std::vector<const xAOD::TruthParticle*> CandidateMu;
	 std::vector<const xAOD::TruthParticle*> CandidateTau;
	 std::vector<const xAOD::TruthParticle*> CandidatePh;
	 std::vector<const xAOD::Jet*>			CandidateJet;
	 
	
	 for (std::vector<const xAOD::TruthParticle*>::const_iterator P= truth_PreEl.begin(); P!= truth_PreEl.end() ; ++ P ) if (KinematicCut(*P)) CandidateEl.push_back(*P);
	 for (std::vector<const xAOD::TruthParticle*>::const_iterator P= truth_PreMu.begin(); P!= truth_PreMu.end() ; ++ P ) if (KinematicCut(*P)) CandidateMu.push_back(*P);
	 for (std::vector<const xAOD::TruthParticle*>::const_iterator P= truth_PreTau.begin(); P!= truth_PreTau.end() ; ++ P ) if (KinematicCut(*P)) CandidateTau.push_back(*P);
	 for (std::vector<const xAOD::TruthParticle*>::const_iterator P= truth_PrePh.begin(); P!= truth_PrePh.end() ; ++ P ) if (KinematicCut(*P)) CandidatePh.push_back(*P);
	 for (std::vector<const xAOD::Jet*>::const_iterator J = truth_PreJets.begin() ; J!= truth_PreJets.end(); ++ J)
		{
		 bool Overlaps= false;
		 for (std::vector<const xAOD::TruthParticle*>::const_iterator P= CandidateEl.begin(); !Overlaps && P!= CandidateEl.end() ; ++ P ) if ( Overlap( *J , *P ,  xAOD4L::OverlapJetElDR )) Overlaps=true;
		 if (!Overlaps) CandidateJet.push_back(*J);
		}
	 //~ FillDRHistograms("tru_" , CandidateEl,CandidateMu,CandidateTau,CandidateJet,CandidateTau , weight);	 
	 //~ FillDRHistograms("tru_W" , CandidateEl,CandidateMu,CandidateTau,CandidateJet,CandidateTau , weight);	 
	 
	 
	 FillDRHistograms("tru_" , truth_PreEl,truth_PreMu,CandidateTau,CandidateJet,CandidatePh , weight);	 
	 FillDRHistograms("tru_W" ,truth_PreEl,truth_PreMu,CandidateTau,CandidateJet,CandidatePh , weight);	 
	 
	 //~ FillDRHistograms("tru_" ,truth_PreEl,truth_PreMu,truth_PreTau,truth_PreJets,truth_PrePh , weight);	 
	 //~ FillDRHistograms("tru_W" ,truth_PreEl,truth_PreMu,truth_PreTau,truth_PreJets,truth_PrePh , weight);		 
	 size_t nll= CandidateEl.size() + CandidateMu.size();
	 Histos->FillNumHistogram("tru_NumLep",nll,weight);
	 Histos->FillNumHistogram("tru_NumJet" , CandidateJet.size() , weight);
	
	
	 FillLeptonHistograms(truth_PreEl , truth_PrePh ,"el",weight);
	 FillLeptonHistograms(truth_PreMu , truth_PrePh ,"mu",weight);
	 FillLeptonHistograms(truth_PreTau , truth_PrePh ,"tau",weight);
	
	 FillParticleHistograms(truth_jets,"tru_jet",weight);
	 if (LeadPh!=NULL)	FillParticleHistograms(LeadPh,"tru_Y",weight);
	 	 
	 float tru_PreMeff (0) , tru_PreMeffJet(0) , tru_PreMeffLep(0) , tru_PreMeffPh(0);
	 std::vector<const xAOD::IParticle*> tru_PreLep = BuildLeptonVector(truth_PreEl , truth_PreMu);
	 
	 unsigned int tru_PreNumMeffJet=0;
	 tru_PreMeff = CalculateMeff(tru_PreLep , truth_PreTau , truth_PreJets , truth_PrePh , tru_PreMeffLep , tru_PreMeffJet,tru_PreMeffPh, truth_MET , tru_PreNumMeffJet,  UseExtendedMeff ); 
	 
	 FillMeffHistograms("tru_", truth_MET->met(), tru_PreMeff, tru_PreMeffLep, tru_PreMeffJet, tru_PreMeffPh, weight);
	 //SUSY variables
	 bool EvTrigger =true; //PassLeptonTrigger(truth_el,truth_mu);
	 xAOD4L::SR SR = SRCounter->GetRegion(truth_Lep , truth_tau, truth_MET ,  truth_Meff , truth_ZVeto4Lep,truth_ZVeto3Lep, truth_ZVeto2Lep);
	 while( EvTrigger==true && SR!=xAOD4L::SR::nDef)
		{
		 if (IsTauRegion(SR)==true  || SRCounter->IsRunIRegion(SR)  || SRCounter->IsRunIIRegion(SR) ) 
			{
			 SR = SRCounter->GetRegion(truth_Lep , truth_tau, truth_MET ,  truth_Meff , truth_ZVeto4Lep,truth_ZVeto3Lep, truth_ZVeto2Lep);
		  	 continue;
			}
		  std::string Pre ="tru_"+(SR!=xAOD4L::SR::nDef ? PrintSignalRegion(SR)+"-" : "");
		  
		  FillDRHistograms(Pre ,truth_el,truth_mu,truth_tau,truth_jets,truth_ph , weight);	 
		  Histos->FillNumHistogram(Pre+"NumJet" , truth_jets.size() , weight);
		
		  FillLeptonHistograms(truth_el , truth_ph ,"el",weight ,SR);
		  FillLeptonHistograms(truth_mu , truth_ph ,"mu",weight, SR);
		  FillLeptonHistograms(truth_tau , truth_ph ,"tau",weight , SR);	  
			 
		  FillParticleHistograms(truth_jets,Pre+"jet",weight);
		  if (LeadPh!=NULL)	FillParticleHistograms(LeadPh,Pre+"Y",weight);
		  FillMeffHistograms(Pre, truth_MET->met(), truth_Meff, truth_MeffLep, truth_MeffJet, truth_MeffPh, weight);
		  SR = SRCounter->GetRegion(truth_Lep , truth_tau, truth_MET ,  truth_Meff , truth_ZVeto4Lep,truth_ZVeto3Lep, truth_ZVeto2Lep);	
		 }		
	}
TruxAOD4LAcceptance::~TruxAOD4LAcceptance()
	{}
void TruxAOD4LAcceptance::AnalyzeSUSYParticles()
	{
	 if (ContainsSUSY==true)
		{
		 for(size_t n=0;n< truth_Neutrino.size();++n)
			{
			 if(TruthParticleHelpers::HasSUSYParent(truth_Neutrino.at(n))==false || truth_Neutrino.at(n)->parent(0)->absPdgId()!=1000022)continue;
			 TLorentzVector InMom = truth_Neutrino.at(n)->p4();
			 uint ChargedLep=0;
			 for(size_t e=0;e<truth_PreEl.size();++e)
				{
				 if(TruthParticleHelpers::ComeFromSameDecay(truth_PreEl.at(e),truth_Neutrino.at(n))==false) continue;
				 InMom = InMom + truth_PreEl.at(e)->p4();
				 ChargedLep++;
				}
			 for(size_t e=0;e<truth_PreMu.size();++e)
				{
				 if(TruthParticleHelpers::ComeFromSameDecay(truth_PreMu.at(e),truth_Neutrino.at(n))==false) continue;
				 InMom = InMom + truth_PreMu.at(e)->p4();
				 ChargedLep++;
				}
			if(ChargedLep!=2) 
				{
				 Warning("xAOD4LTruAccceptance","In Event %llu: Could not find all charged Particles from Neutralino Decay. Found %u",nEvents,ChargedLep);
				 Info("xAOD4LTruAcceptance","##################################################################");			
				 PromptParticleInformation(truth_Neutrino.at(n));
				 Info("xAOD4LTruAcceptance","Parent");
				 const xAOD::TruthParticle* P = truth_Neutrino.at(n)->parent(0);
				 PromptParticleInformation(P);
				 Info("xAOD4LTruAcceptance","Children");
				 for (size_t c=0;c < P->nChildren();++c) PromptParticleInformation(P->child(c));				 
				}
			else FillParticleHistograms(InMom , "tru_iLSP",weight);
			}
		 size_t NumE,NumM,NumT;
		 NumE=NumM=NumT=0;
		 for(size_t s=0; s< truth_Sparticles.size();++s)
			{
			 const xAOD::TruthParticle* S= truth_Sparticles.at(s);
				 
			 if ( S->absPdgId()==1000022 ) 
				{
				 const xAOD::TruthParticle* LSP = GetLastInChain(S);
				 TLorentzVector Neutralino= LSP->p4();				 
				 FillDarlitzHistograms(S);
				 FillParticleHistograms(Neutralino,"tru_LSP",weight);
				 GetChargedLeptonMultiplicity(S, NumE,NumM,NumT);
				 
				 for(size_t c=0;c< S->nChildren();++c)
					{
					 if (S->child(c)->isNeutrino()==true)
						{
						 TLorentzVector Neut = S->child(c)->p4();
						 Neut.Boost( -GetRestBoost(Neutralino));
						 double sin = TMath::Sin ( Neut.Vect().Angle( GetRestBoost(Neutralino) ));
						 double cos = TMath::Cos ( Neut.Vect().Angle( GetRestBoost(Neutralino) ));
						 //~ std::cout << " Sin = "<<sin<<" and cos = "<<cos <<" --> s^2+c^2 = "<<(sin*sin+cos*cos)<<std::endl;
						 Histos->FillHistogram ("tru_dPhi-LSP-neutS",TMath::Sin ( Neut.Vect().Angle( GetRestBoost(Neutralino) ) ),weight);
						 Histos->FillHistogram ("tru_dPhi-LSP-neut",TMath::Cos ( Neut.Vect().Angle( GetRestBoost(Neutralino) ) ),weight);
						}
					}				
				 for(size_t p=0;p< LSP->nParents();++p)
					{
					 if (TruthParticleHelpers::IsSparticle(LSP->parent(p))==false)continue;					
					 Neutralino.Boost( -GetRestBoost(LSP->parent(p)));
					 Histos->FillHistogram ("tru_dPhi-LSP-NLSP",TMath::Cos(Neutralino.Vect().Angle( GetRestBoost(LSP->parent(p)) ) ) , weight);
					 break;
					}
				}
			 else// if (Sparticles.at(s)->absPdgId()>=1000021 && Sparticles.at(s)->absPdgId()<=1000024)  
				{ 
				 while (TruthParticleHelpers::HasSameChild(S)==true)
					{
					 for(size_t c=0;c<S->nChildren();++c) 
						{
						 if(S->pdgId()==S->child(c)->pdgId())
							{
							 S=S->child(c);
							 break;
							}
						}
					} 
				 if (S->absPdgId()==1000024)
					{
					 bool HasWChild=false;					 
					 for(size_t c=0;HasWChild==false && c < S->nChildren();++c) if(S->child(c)->isW()==true)HasWChild=true;
					 if(HasWChild==false)
						{
						 FillDecayHistogram(S,"tru_W-Children",weight);
						 TLorentzVector W (0,0,0,0);
						 for(size_t c=0;c<S->nChildren();++c) 
							{							 
							 if(TruthParticleHelpers::IsSparticle(S->child(c))==true)continue;
							 W = W + S->child(c)->p4();							 
							}
						FillParticleHistograms(W,"tru_W",weight);
						}
					}
				else if(S->absPdgId()==1000023)
					{
					 bool HasZChild = false;
					 bool HasHChild = false;
					 if (nEvents < 10) PrintAllChildren(S);
					  for (size_t c=0;c<S->nChildren();++c)
						{
						 if (S->child(c)->isZ()==true) 
							{
							 PromptParticleInformation(S->child(c));
							 HasZChild=true;
							}
						 if (S->child(c)->isHiggs()==true) HasHChild=true;						
						}
					if (HasZChild ==false && HasHChild == false)
						{
						 TLorentzVector Boson (0,0,0,0);
						 uint nC =0;
						 for (size_t c=0; c< S->nChildren() ;++c)
							{
							 if( TruthParticleHelpers::IsSparticle(S->child(c))==true)continue;
							 ++nC;
							 //Boson = Boson + S->child(c)->p4();
							}
						}					
					}
				 FillDecayHistogram(S,"tru_NLSP-Children",weight);
				 for(size_t s1=s+1;s1<truth_Sparticles.size();++s1)
					{
					 const xAOD::TruthParticle* S1 = truth_Sparticles.at(s1);
					 if ( S1->absPdgId()==1000022)continue;
					 Histos->FillHistogram ("tru_dPhi-NLSP-NLSP",S1->p4().DeltaR(S->p4()),weight);
					}
				 FillParticleHistograms(S,"tru_NLSP",weight);
				}
			}
		Histos->FillLepMultHistogram("tru_RPVdecay",NumE,NumM,NumT,weight);		
		}	
	 
	}
void TruxAOD4LAcceptance::FillLeptonHistograms( std::vector<const xAOD::TruthParticle*> &Lep, std::vector<const xAOD::TruthParticle*> &Photons, const std::string &Name , const double &w, xAOD4L::SR SR )
	{
	 std::string Pre ="tru_"+(SR!=xAOD4L::SR::nDef ? PrintSignalRegion(SR)+"-" : "");
	 std::string PreS ="tru_"+(SR!=xAOD4L::SR::nDef ? PrintSignalRegion(SR)+"-" : "")+"SUSY_";
	 std::string PreV ="tru_"+(SR!=xAOD4L::SR::nDef ? PrintSignalRegion(SR)+"-" : "")+"V_";
	 for (size_t l=0; l< Lep.size();++l)
		{
		 const xAOD::TruthParticle* L = Lep.at(l);
		 const xAOD::IParticle* PH = GetNearestParticle(L,Photons,Lep,false);
		 FillParticleHistograms(L,Pre+Name,w);
		 std::string p ="";
		 if(TruthParticleHelpers::HasSUSYParent(L)==true)p=PreS;  
		 else p=PreV;
		 
		 if(PH!=NULL) Histos->FillHistogram(p+"dR_"+Name+"ph",L->p4().DeltaR(PH->p4()),w);
		 FillParticleHistograms(L,p+Name,w);				
		}
	}
  
void TruxAOD4LAcceptance::FillDecayHistogram (const xAOD::TruthParticle* Particle, const std::string &Name, const double &w)
	{
	 if(Particle==NULL)
		{
		 Warning("TruxAOD4LAcceptance","In Event %llu the given Particle pointer is empty",nEvents);
		 return;
		}
	//~ if ( !(Particle->isW() || Particle->absPdgId()==1000024) ) return;
	while (TruthParticleHelpers::HasSameChild(Particle)==true)
		{
		 for(size_t c=0;c<Particle->nChildren();++c) 
			{
			 if(Particle->pdgId()==Particle->child(c)->pdgId())
				{
				 Particle=Particle->child(c);
				 break;
				}
			}
		}
	for(size_t c=0; c< Particle->nChildren();++c) Histos->FillHistogram(Name , Particle->child(c)->absPdgId(), w);	
	//~ for(size_t c=0; c< Particle->nChildren();++c) if(Particle->child(c)->isLepton())PromptParticleInformation(Particle->child(c));
	}
void TruxAOD4LAcceptance::InitParticleHistograms (  std::string Pre ,std::string Particle , std::string xTitle , HistManager::DefinedHistos PtType , HistManager::DefinedHistos MType )
	{
	 Histos->CreateNewHisto(MType , Pre+Particle+"-M" , "M of the selected "+xTitle, "E("+xTitle+") [GeV]" );
	 Histos->CreateNewHisto( PtType , Pre+Particle+"-pt" , "P_{T} of the selected "+xTitle , "p_{T}("+xTitle+") [GeV]");
	 Histos->CreateNewHisto(HistManager::DefinedHistos::eta,  Pre+Particle+"-eta", "#eta of the selected"+xTitle , "#eta ("+xTitle+")");
	 Histos->CreateNewHisto(HistManager::DefinedHistos::phi,  Pre+Particle+"-phi", "#phi of the selected"+xTitle ,"#phi ("+xTitle+")");
	}
	
void TruxAOD4LAcceptance::InitTruthHistos( )
	{
	 InitCommonHistos("tru"); 
	 if (ContainsSUSY==true)
		{
		 Histos->CreateNewHisto(HistManager::DefinedHistos::LepMul,"tru_RPVdecay", "Decay modes of the RPV LSP decays");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::pdgId,"tru_NLSP-Children","NLSP children","#tilde{#chi}#rightarrow#bar{f}_{1}f_{2}");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::Iso,"tru_dPhi-LSP-neut", "Angular distirbution of the Neutrino from the LSP decay","cos(#theta^{*}_{ #nu} )");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::Iso,"tru_dPhi-LSP-neutS", "Angular distirbution of the Neutrino from the LSP decay","sin(#theta^{*}_{ #nu} )");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::phi,"tru_dPhi-NLSP-NLSP", "Angular distribution between the two NLSP","sin^{2}(#Delta#Phi(NLSP,NLSP) )");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::Iso,"tru_dPhi-LSP-NLSP", "Angular distribution between the NLSP decay and the LSP","cos(#theta^{*}_{#tilde{#chi}^{0}_{1}})");			
		 Histos->CreateNewHisto(HistManager::DefinedHistos::medPt_medPt , "tru_Da_LEP-NeuL" , "Dalitz Plot","m^{2} (L^{+}L^{-}) [GeV]^{2}"  ,"m^{2} (#nu L^{-}) [GeV]^{2}");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::medPt_medPt , "tru_Da_LEP-NeuR" , "Dalitz Plot","m^{2} (L^{+}L^{-}) [GeV]^{2}"  ,"m^{2} (#nu L^{+}) [GeV]^{2}");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::medPt_medPt , "tru_Da_Neu-Neu" , "Dalitz Plot","m^{2} (#nu L^{-}) [GeV]^{2}"  ,"m^{2} (#nu L^{+}) [GeV]^{2}");		 
		}
	 if(GetW==true) Histos->CreateNewHisto(HistManager::DefinedHistos::pdgId,"tru_W-Children","W children","W decay modes");	 
	 if(GetZ==true) Histos->CreateNewHisto(HistManager::DefinedHistos::pdgId,"tru_Z-Children","Z children","Z decay modes");	 
	
	}
void TruxAOD4LAcceptance::InitCommonHistos(const std::string &Pre)
	{
	 std::string Particle[]={"el","mu","tau","jet","Y" , "Z" , "W" , "LSP" ,"iLSP", "NLSP"};
	 std::string ParTitle[]={"e","#mu","#tau","jets","#gamma" , "Z^{0}", "W^{#pm}","#tilde{#chi}_{1}^{0}","P_{Lep}","NLSP"};
	 std::string DeltaR[]={"ee","emu","mumu","ej","muj","jj","etau","mutau","jtau","tautau","jph","elph","muph","tauph"};
	 std::string DeltaRTit[]={"electron - electron","electron - #mu", "#mu - #mu","electron - jet", "#mu -jet", "jet - jet","electron - #tau","#mu - #tau","jet-#tau","#tau-#tau","jet-#gamma","e-#gamma","#mu-#gamma","#tau-#gamma"};
	 std::string DeltaRxAxis[]={"e,e","e,#mu","#mu,#mu","e,jet","#mu,jet","jet,jet","e,#tau","#mu,#tau" , "jet,#tau" , "#tau,#tau","jet,#gamma","e,#gamma","#mu,#gamma","#tau,#gamma"};
	 std::string Origin [] = {"" ,"SUSY_","V_"};	 
	 std::string LPair [] = {"DFOS" , "SFOS"};
		
	 for (int c=0; c< 3 ; ++c)
	{
	 for (int b=xAOD4L::SR::nDef;b!=xAOD4L::SR::R1SR0Z;b++)
		{
		 xAOD4L::SR R=(xAOD4L::SR)b;
		 if( IsTauRegion(R)==true) continue;
		 std::string SR =Pre+"_"+(R!=xAOD4L::SR::nDef?PrintSignalRegion(R)+"-":"")+Origin[c];
			
		 for(unsigned int a=0;a<10;++a)
			 {
			 if ( (R!=xAOD4L::SR::nDef && a > 4) || (c >0 && a>2) )continue;
			 HistManager::DefinedHistos PT = HistManager::DefinedHistos::lowmed_pt;
			 HistManager::DefinedHistos M = HistManager::DefinedHistos::low_pt;
			 if (a>=6) 
				{
				 M = HistManager::DefinedHistos::medium_pt;
				 PT = HistManager::DefinedHistos::medium_pt;
				}
		 	 InitParticleHistograms ( SR , Particle[a] ,ParTitle[a] , PT , M);
			}
		if (c==0)
			{
			 Histos->CreateNewHisto(HistManager::DefinedHistos::medium_pt,SR+"Met","E^{miss}_{T} in 4l events", "E^{miss}_{T}");
		
			 //~ Histos->CreateNewHisto(HistManager::DefinedHistos::met,SR+"Met","E^{miss}_{T} in 4l events");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Meff-Met","M_{eff}  E^{miss}_{T} in 4l events","M_{eff} [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Ht-Met","H_{T}  E^{miss}_{T} in 4l events","H_{T} [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff","M_{eff} in 4l events");		 
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Lep","M_{eff} contribution of the Leptons in 4l events","H_{T} (Leptons) [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Jet","M_{eff} contribution of the Jets in 4l events","H_{T} (Jets) [GeV]");
			 if(UseExtendedMeff==true) Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Y","M_{eff} contribution of the #gamma in 4l events","H_{T} (#gamma) [GeV]");		
			 Histos->CreateNewHisto(HistManager::DefinedHistos::NumJet,SR+"NumJet", "Number of Jets per 4l event","Number(jets)");
			 if (R==xAOD4L::SR::nDef) Histos->CreateNewHisto(HistManager::DefinedHistos::NumLep , SR+"NumLep", "Number of Leptons per event","Number(Lep)"); 
			 if (R==xAOD4L::SR::nDef || R==xAOD4L::SR::SR0)
				{
				 for( int l=0; l < 2 ; ++l)
					{
					 Histos->CreateNewHisto(HistManager::DefinedHistos::dR_M , Form( "%s_dR-M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the %s pair vs. its separation",LPair[l].c_str()), "#DeltaR (l_{1} , l_{2} )", Form("m_{%s} [GeV]",LPair[l].c_str()) );
					 Histos->CreateNewHisto(HistManager::DefinedHistos::dR_M , Form( "%s-C_dR-M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the %s pair vs. its separation",LPair[l].c_str()), "#DeltaR (l_{1} , l_{2} )", Form("m_{%s} [GeV]",LPair[l].c_str()) );
					 Histos->CreateNewHisto(HistManager::DefinedHistos::dR05 ,  Form( "%s-C_dR",(SR+LPair[l]).c_str()) , Form("Separation of a the leptons in a %s pair",LPair[l].c_str()) ,Form("#Delta R_{%s}",LPair[l].c_str()));
					
					 Histos->CreateNewHisto(HistManager::DefinedHistos::low_pt ,  Form( "%s-C_M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the closest %s pair",LPair[l].c_str()) ,Form("m_{%s} [GeV]",LPair[l].c_str()));
					 Histos->CreateNewHisto(HistManager::DefinedHistos::low_pt ,  Form( "%s-B_M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the best %s pair",LPair[l].c_str()) ,Form("m_{%s} [GeV]",LPair[l].c_str()));					
					}
				}
			}
		 for (unsigned int a=0;a<14;++a) 
			{
			 if (c!=0 && a<11) continue;
			 Histos->CreateNewHisto( HistManager::DefinedHistos::dR05 , Form("%sdR_%s",SR.c_str(),DeltaR[a].c_str()),Form("Separation of the %s",DeltaRTit[a].c_str()),Form("#DeltaR(%s)",DeltaRxAxis[a].c_str() ));
			 Histos->CreateNewHisto( HistManager::DefinedHistos::dR30 , Form("%sWdR_%s",SR.c_str(),DeltaR[a].c_str()),Form("Separation of the %s",DeltaRTit[a].c_str()),Form("#DeltaR(%s)",DeltaRxAxis[a].c_str() ));
		
			}
		}
	  }
	}
void TruxAOD4LAcceptance::FillDRHistograms(const std::string &Pre , std::vector <const xAOD::TruthParticle*> &El,   std::vector <const xAOD::TruthParticle*> &Mu,  std::vector <const xAOD::TruthParticle*> &Tau,  std::vector <const xAOD::Jet*> &Jet,  std::vector <const xAOD::TruthParticle*> & Ph , double w)
	{
	 const xAOD::IParticle* NearPart=NULL;
	 std::vector<const xAOD::TruthParticle*>::const_iterator Ref;
	 std::vector<const xAOD::Jet*>::const_iterator J;
	 for(Ref=El.begin() ; Ref!=El.end(); ++Ref)
		{
		 NearPart=GetNearestParticle(*Ref,El,El,false);
		 Histos->FillHistogram(Pre+"dR_ee", DeltaR(NearPart,*Ref) , w / 2.);
		 NearPart=GetNearestParticle(*Ref,Mu,El,false);
		 Histos->FillHistogram(Pre+"dR_emu",DeltaR(NearPart,*Ref),w);
		 NearPart=GetNearestParticle(*Ref,Jet,El,false);
		 Histos->FillHistogram(Pre+"dR_ej",DeltaR(NearPart,*Ref),w);		 
		 NearPart=GetNearestParticle(*Ref,Ph,El,false);
		 Histos->FillHistogram(Pre+"dR_elph",DeltaR(NearPart,*Ref),w);		 
		}
	for(Ref=Mu.begin() ; Ref!=Mu.end(); ++Ref)
		{
		 NearPart=GetNearestParticle(*Ref,Mu,Mu,false);
		 Histos->FillHistogram(Pre+"dR_mumu" , DeltaR(NearPart,*Ref) , w / 2. );
		 NearPart=GetNearestParticle(*Ref,Jet,Mu,false);
		 Histos->FillHistogram(Pre+"dR_muj" , DeltaR(NearPart,*Ref) , w);
		 NearPart=GetNearestParticle(*Ref,Ph,Mu,false);
		 Histos->FillHistogram(Pre+"dR_muph" , DeltaR(NearPart,*Ref) , w);		 		
		}
	for(J=Jet.begin() ; J!=Jet.end(); ++J)
		{
		  NearPart=GetNearestParticle(*J,Jet,Jet,false);
		  Histos->FillHistogram(Pre+"dR_jj" , DeltaR(NearPart,*J) , w / 2.);			
		  NearPart=GetNearestParticle(*J,Ph,Jet,false);
		  Histos->FillHistogram(Pre+"dR_jph" , DeltaR(NearPart,*J) , w);	 
		}
	for(Ref=Tau.begin() ; Ref!=Tau.end(); ++Ref)
		{
		 NearPart=GetNearestParticle(*Ref,Tau,Tau,false);
		 Histos->FillHistogram(Pre+"dR_tautau",DeltaR(NearPart,*Ref),w / 2.);
		 NearPart=GetNearestParticle(*Ref,Mu,Tau,false);
		 Histos->FillHistogram(Pre+"dR_mutau",DeltaR(NearPart,*Ref),w);			 
		 NearPart=GetNearestParticle(*Ref,Jet,Tau,false);
		 Histos->FillHistogram(Pre+"dR_jtau",DeltaR(NearPart,*Ref),w);
		 NearPart=GetNearestParticle(*Ref,El,Tau,false);
		 Histos->FillHistogram(Pre+"dR_etau",DeltaR(NearPart,*Ref),w);
		 NearPart=GetNearestParticle(*Ref,Ph,Tau,false);
		 Histos->FillHistogram(Pre+"dR_tauph",DeltaR(NearPart,*Ref),w);		 		 		 
		}
	}
void TruxAOD4LAcceptance::FillMeffHistograms(const std::string &Pre, const double &Met, const double &Meff, const double &MeffLep, const double &MeffJet, const double &MeffPh, const double &w)
	{
	 Histos->FillHistogram (Pre+"Meff-Met",Meff/GeVToMeV,Met/GeVToMeV,w);
	 Histos->FillHistogram (Pre+"Ht-Met",(Meff-Met)/GeVToMeV,Met/GeVToMeV,w);	 
	 Histos->FillHistogram (Pre+"Met",Met/GeVToMeV,w);	
	 Histos->FillHistogram (Pre+"Meff",Meff/GeVToMeV,w);	 
	 Histos->FillHistogram (Pre+"Meff-Lep",MeffLep/GeVToMeV,w);
	 Histos->FillHistogram (Pre+"Meff-Jet",MeffJet/GeVToMeV,w);
	 if(UseExtendedMeff==true) Histos->FillHistogram (Pre+"Meff-Y",MeffPh/GeVToMeV,w);
	}
template <typename PartType> void TruxAOD4LAcceptance::FillParticleHistograms(const std::vector<PartType*> &Particle,const std::string &Name, const double &w)
	{
	 typename std::vector<PartType*>::const_iterator Itr;
	 for (Itr=Particle.begin(); Itr!=Particle.end();++Itr) FillParticleHistograms (*Itr,Name,w);	
	}
void TruxAOD4LAcceptance::FillParticleHistograms (const xAOD::IParticle* Particle,const std::string &Name,const double &w)
	{
	 FillParticleHistograms(Particle->p4(),Name,w);	
	}
void TruxAOD4LAcceptance::FillParticleHistograms (TLorentzVector Momentum ,const std::string &Name,const double &w)
	{
	 Histos->FillHistogram (Name+"-M",Momentum.E()/GeVToMeV,w);	 
	 Histos->FillHistogram (Name+"-pt",Momentum.Pt()/GeVToMeV,w);
	 Histos->FillHistogram (Name+"-eta",Momentum.Eta(),w);
	 Histos->FillHistogram (Name+"-phi",Momentum.Phi(),w);		
	}
void TruxAOD4LAcceptance::SetOption(const std::string Option, bool Switch)
	{
	 xAOD4LSelection::SetOption(Option,Switch);
	 if (Option=="xSecWeight") UseXSection=Switch;
	 
	 GetReco=false;
	 GetPhoton=true;
	 GetW=true;
	 GetZ=true;
	 Get2LeptonsEvents=true;
	 Get4LeptonsEvents=true;
	 	 	
	}
void TruxAOD4LAcceptance::PromptOptions()
	{
	 xAOD4LSelection::PromptOptions();		
	}
const xAOD::TruthParticle* TruxAOD4LAcceptance::GetLastInChain ( const xAOD::TruthParticle* Part )
	{
	 if (Part==NULL)
		{
		 Error("TruxAOD4LAcceptance", "The given Particle pointer is NULL");
		 return NULL;
		}
	 for (size_t p =0; p< Part->nParents();++p)	
		{
		 if(Part->parent(p)->pdgId()==Part->pdgId()) return GetLastInChain(Part->parent(p));
		}
	 return Part;
	}
void TruxAOD4LAcceptance::PrintAllChildren(const xAOD::TruthParticle* Part, std::string AddInfo , int Step)
	{
	 if(Part==NULL) return;
	 Info("TruxAOD4LAcceptance","################################################################################");
	 Info("TruxAOD4LAcceptance","Print all Particle Children in Event %lli (%i)", nEvents,Step);
	 PromptParticleInformation(Part,AddInfo);
	 Info("TruxAOD4LAcceptance","################################################################################");		 
	 for (size_t c=0;c< Part->nChildren();++c)
		{
		 if(Part->child(c)==NULL) 
			{
			 Warning("TruxAOD4LAcceptance" ,"In Event %lli Child Pointer points to nothing",readEvents);
			 continue;
			}
		 if(Part->child(c)->nChildren() > 0) PrintAllChildren(Part->child(c),AddInfo,Step + 1);
		 else PromptParticleInformation(Part->child(c),Form("Level %i	%s",Step,AddInfo.c_str() ) );
		}
	if(Step==0)
		{
		 Info("TruxAOD4LAcceptance","################################################################################");
		 Info("TruxAOD4LAcceptance","Done");
		 Info("TruxAOD4LAcceptance","################################################################################");		 
		}	
	}
void TruxAOD4LAcceptance::GetNumLepFromVDecay ( std::vector<const xAOD::TruthParticle*> &Lep , unsigned int &NumW , unsigned int &NumZ)
	{
	 NumW=NumZ=0;
	std::vector<const xAOD::TruthParticle*>::const_iterator L;
	
	  for ( L = Lep.begin() ; L!=Lep.end() ; ++L)
		{
		 if (TruthParticleHelpers::OriginatesFromZDecay(*L)  || TruthParticleHelpers::OriginatesFromYDecay(*L)  )  ++NumZ;
		 else if (TruthParticleHelpers::OriginatesFromWDecay(*L) ) ++NumW;
		}
	}
void TruxAOD4LAcceptance::FillDarlitzHistograms ( const xAOD::TruthParticle* LSP)
	{
	 const xAOD::TruthParticle* Neut=NULL;
	 const xAOD::TruthParticle* PosLep=NULL;
	 const xAOD::TruthParticle* NegLep=NULL;
	 for ( size_t c =0; c < LSP->nChildren(); ++c)
		{
		 //~ PromptParticleInformation(LSP->child(c));
		 if (LSP->child(c)->isNeutrino() == true)
			{
			 if (Neut==NULL) Neut =LSP->child(c);
			 else 
				{
				 Warning("TruxAOD4LAcceptance" , "It exists already a neutrino from LSP decay");
				 PromptParticleInformation(LSP);
				 PromptParticleInformation(Neut);				 
				}			
			}
		else if  (LSP->child(c)->isChLepton() == true)
			{
			 bool PromptWarning=false;
			 if (LSP->child(c)->charge() > 0)
				{
				 if(PosLep ==NULL) PosLep=LSP->child(c);
				 else PromptWarning=true;
				}
			else
				{
				 if(NegLep ==NULL) NegLep=LSP->child(c);
				 else PromptWarning=true;
				}
			 if (PromptWarning==false) continue;
			 Warning("TruxAOD4LAcceptance" , "It exists already a charged Lepton from LSP decay");
			 PromptParticleInformation(LSP);
			 PromptParticleInformation( PosLep!=NULL ? PosLep : NegLep);
			}		 
		}
	 if (Neut ==NULL || PosLep ==NULL || NegLep==NULL) return;
	 bool IsAntiNeut = (Neut->pdgId() < 0);
	 float M_NeuLeLep = TwoPartInvariantMass( Neut , IsAntiNeut== false ? NegLep : PosLep) / GeVToMeV;
	 float M_NeuRiLep = TwoPartInvariantMass( Neut , IsAntiNeut== false ? PosLep : NegLep)  / GeVToMeV;
	 float M_LepLep = TwoPartInvariantMass ( PosLep , NegLep)  / GeVToMeV;
	 //Square the invariant masses
	 float M_NeuLeLep2 = TMath::Power ( M_NeuLeLep ,2 );
	 float M_NeuRiLep2 = TMath::Power ( M_NeuRiLep ,2 );
	 float M_LepLep2 = TMath::Power ( M_LepLep ,2 );
	 Histos->FillHistogram( "tru_Da_LEP-NeuL" , M_LepLep2  , M_NeuLeLep2);
	 Histos->FillHistogram( "tru_Da_LEP-NeuR" , M_LepLep2  , M_NeuRiLep2);
	 Histos->FillHistogram( "tru_Da_Neu-Neu" , M_NeuLeLep2 , M_NeuRiLep2);			
	}
void TruxAOD4LAcceptance::DrellYangVeto (std::vector<const xAOD::IParticle*> &BaseLep , const std::string &Pre , double &w)
	{
	 if(BaseLep.size() < 2) return;
	 bool Has4BaseLep = (BaseLep.size() >= 4);
	 float_t BestSFOSPair(1.e9), BestDFOSPair(1.e9), ClosestSFOSPair(1.e9),dR_SFOS(1.e4),ClosestDFOSPair(1.e9),dR_DFOS(1.e4);
	 for ( std::vector<const xAOD::IParticle*>::iterator Lep = BaseLep.begin() ; Lep  != BaseLep.end() ; ++ Lep)
		{
		 for( std::vector<const xAOD::IParticle*>::iterator Lep1 = Lep + 1; Lep1  != BaseLep.end() ; ++ Lep1)
			{
			 float DiMass = TwoPartInvariantMass( *Lep ,*Lep1);
			 if (SFOS (*Lep , *Lep1) == true && TMath::Abs(BestSFOSPair) > TMath::Abs(DiMass) ) BestSFOSPair = DiMass;
			 else if (DFOS(*Lep,*Lep1) == true &&  BestDFOSPair > DiMass   ) BestDFOSPair = DiMass;
			}	
			const xAOD::IParticle* NearLep = GetNearestParticle( *Lep , BaseLep , BaseLep);	
			if(NearLep!=NULL && SameSign(*Lep,NearLep) == false)
				{
				 float DiMass = TwoPartInvariantMass( *Lep , NearLep);
				 float dR =DeltaR( *Lep , NearLep ,false );
		 	  	 if ( SFOS(*Lep,NearLep)==true && Overlap(*Lep , NearLep , dR_SFOS) )
					{
					 dR_SFOS = dR;
					 ClosestSFOSPair = DiMass;	
					}				
				else if ( DFOS(*Lep,NearLep)==true && Overlap(*Lep , NearLep , dR_DFOS) )
					{
					 dR_DFOS = dR;
					 ClosestDFOSPair = DiMass;
					 }
		 	  	 //Filling the distributions of each Pair;
		 	  	 std::string N = DFOS(*Lep , NearLep)==true ? "DFOS" : "SFOS"; 
		 	  	 Histos->FillHistogram(Pre+"_"+N+"_dR-M" , dR , DiMass / GeVToMeV , w);
				 if (Has4BaseLep==true)Histos->FillHistogram(Pre+"_4l-"+N+"_dR-M" , dR , DiMass / GeVToMeV , w);	
				}				 
		}		
	  Histos->FillHistogram(Pre+"_SFOS-B_M" ,  BestSFOSPair / GeVToMeV , w);
	  Histos->FillHistogram(Pre+"_DFOS-B_M" ,  BestDFOSPair / GeVToMeV , w);
	  
	  Histos->FillHistogram(Pre+"_SFOS-C_M" ,  ClosestSFOSPair / GeVToMeV , w);
	  Histos->FillHistogram(Pre+"_DFOS-C_M" ,  ClosestDFOSPair / GeVToMeV , w);
	  
	  Histos->FillHistogram(Pre+"_SFOS-C_dR" , dR_SFOS , w);
	  Histos->FillHistogram(Pre+"_DFOS-C_dR" , dR_DFOS , w);	  
	  
	  Histos->FillHistogram(Pre+"_SFOS-C_dR-M" , dR_SFOS ,ClosestSFOSPair / GeVToMeV, w);
	  Histos->FillHistogram(Pre+"_DFOS-C_dR-M" , dR_DFOS ,ClosestDFOSPair / GeVToMeV, w);
	  
	  if (Has4BaseLep==true)
		{
		 Histos->FillHistogram(Pre+"_4l-SFOS-B_M" ,  BestSFOSPair / GeVToMeV , w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-B_M" ,  BestDFOSPair / GeVToMeV , w);
		
		 Histos->FillHistogram(Pre+"_4l-SFOS-C_M" ,  ClosestSFOSPair / GeVToMeV , w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-C_M" ,  ClosestDFOSPair / GeVToMeV , w);
		
		 Histos->FillHistogram(Pre+"_4l-SFOS-C_dR" , dR_SFOS , w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-C_dR" , dR_DFOS , w);	  
		
		 Histos->FillHistogram(Pre+"_4l-SFOS-C_dR-M" , dR_SFOS ,ClosestSFOSPair / GeVToMeV, w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-C_dR-M" , dR_DFOS ,ClosestDFOSPair / GeVToMeV, w);	
		}
	}
