//Class Header
#include <XAOD4LAcceptance/xAOD4LAcceptance.h>
#define XAOD4LAcceptance_cxx

xAOD4LAcceptance::xAOD4LAcceptance ():
	xAOD4LSelection()	
	{
	//SaveTrees=true;
	}
void xAOD4LAcceptance::FillIsolationHistograms (const std::string &Name , const xAOD::IParticle* P)
	{
	 const xAOD::IParticle* N = GetNearestParticle( P , rec_NonIsoLep , rec_NonIsoLep);
	 float dR = DeltaR( N , P);
	 const xAOD::TrackParticle* T = N!=NULL ? Track(N) : NULL;
	 const xAOD::CaloCluster* C = ( N!=NULL && !isDAOD ? Cluster(N) : NULL );
	 
	 float Pt = T!=NULL ? T->pt() : 1.;
	 float Et = IsoCorrect->ClusterEtMinusTile(C);
	 if ( Et < 1.e-10 ) Et=-1.e-8;
	 bool Pass4Lep = (rec_NonIsoLep.size() > 3 );
	 bool Correct =  DoIsoCorrection;
	 FillIsolationHistogram ( Name+"_IsoTopoEtCone", GetCaloTopoIsolation ( P , Correct )  ,Name+"_TopoEtCone", CaloTopoCone( P , Correct ) , Pass4Lep);
	 FillIsolationHistogram ( Name+"_IsoPtCone", GetTrackIsolation ( P , Correct ), Name+"_PtCone", TrackPtCone (P , Correct )  , Pass4Lep);
	 FillIsolationHistogram ( Name+"_IsoPtVarCone" , GetTrackVarIsolation (P , Correct ) ,Name+"_PtVarCone",   TrackVarPtCone (P , Correct ) , Pass4Lep);
	 std::string NType = "";
	 if (N!=NULL)
		{
		 if(N->type() == xAOD::Type::ObjectType::Electron) NType="el";
		 else NType="mu";
		}
	if (!isDAOD) FillIsolationHistogram(Name+"_IsoTopoEtCone" , CaloTopoCone(P , Correct) , Et , dR , Pass4Lep, NType);
	
	FillIsolationHistogram(Name+"_IsoPtCone" ,  TrackPtCone(P , Correct) , Pt , dR , Pass4Lep, NType);
	FillIsolationHistogram(Name+"_IsoPtVarCone" ,  TrackVarPtCone(P , Correct) , Pt , dR , Pass4Lep, NType);		 
	}

template <typename E , typename M , typename T , typename P> void xAOD4LAcceptance::AnalyzeCommonEvent ( const std::string &EvType , std::vector<const E*> &El , 
																							std::vector <const M*> &Mu , std::vector<const xAOD::Jet*> &Jet , std::vector <const T*> & Tau , 
																							std::vector <const P*> &Ph , const xAOD::MissingET* MET , bool IsReco , bool RecalcEv)
	{
	 static int MeffCuts[]={300,600,700,800,900,1000,1100,1200};
	 float Meff (0) , Ht_Lep(0) , Ht_Jet(0), Ht_Ph(0)  , Met(0) ;
	 double w (1.);
	 std::vector<const xAOD::Jet*>::const_iterator J , J1;
	 unsigned int NumMeffJet (0);
	 int EvCharge=-100;
	 bool ZVeto4Lep (false) , ZVeto3Lep (false) , ZVeto2Lep (false);
	 const xAOD::IParticle* LeadPh=NULL;
	
	 std::vector <const xAOD::IParticle*> Lep = BuildLeptonVector ( El , Mu );	 
	 
	 if ( GetPhoton==true ) LeadPh=FindLeadingPtParticle(Ph);
	
	 Met 		=  MET!=NULL ? MET->met() : 0 ;
	
	 if (RecalcEv == false)
		{
		 Meff 		= (IsReco ? Reco_Meff 	 : truth_Meff);
		 Ht_Lep		= (IsReco ? Reco_MeffLep : truth_MeffLep);
		 Ht_Jet		= (IsReco ? Reco_MeffJet : truth_MeffJet);
		 Ht_Ph 		= (IsReco ? Reco_MeffPh  : truth_MeffPh);
		 NumMeffJet	= (IsReco ? Reco_NumMeffJet  : truth_NumMeffJet);
		}
	 else
		{
		 PassZVeto ( Lep , IsReco );
	 	 Meff = CalculateMeff (Lep , Tau , Jet , Ph  ,Ht_Lep , Ht_Jet ,  Ht_Ph , MET , NumMeffJet ,  UseExtendedMeff ); 
	 	}
	  ZVeto4Lep = ( IsReco ? Reco_ZVeto4Lep : truth_ZVeto4Lep ); 
	  ZVeto3Lep = ( IsReco ? Reco_ZVeto3Lep : truth_ZVeto3Lep ); 
	  ZVeto2Lep = ( IsReco ? Reco_ZVeto2Lep : truth_ZVeto2Lep ); 
	  w = ( IsReco ? reco_weight : weight) ;
	  if (Get4LeptonsEvents==true && EvTrigger == true)
		{
		 Histos->FillLepMultHistogram( EvType+"_LepMul" , El.size() , Mu.size() , Tau.size() , w);	 
		 Histos->FillLepMultHistogram( EvType+"_LepMulnT" , El.size() , Mu.size() , Tau.size() , w);		
		 if (IsReco) EvCharge = 2 + GetLeptonEventCharge(true);	 		
		}
	 Histos->FillNumHistogram(EvType+"_NumLep" ,  Lep.size() , w);
	
	 xAOD4L::SR SR=xAOD4L::SR::nDef;
	 
	 bool nDef=true;
	 SRCounter->NewEvent();					 
	 while( SR!=xAOD4L::SR::nDef || nDef==true)
		{
		  if(EvTrigger==true) 
			{
			  SR = SRCounter->GetRegion(Lep ,Tau , MET , Meff, ZVeto4Lep , ZVeto3Lep, ZVeto2Lep);
			  if ( !(SRCounter->IsRunIIRegion(SR) || SR==xAOD4L::SR::nDef) ) Histos->FillHistogram( EvType+"_SRw_ev" , SRCounter->GetEvHistoBin(SR) , w);
			  if ( !(SRCounter->IsRunIIRegion(SR) || SR==xAOD4L::SR::nDef) ) Histos->FillHistogram( EvType+"_SR_ev" , SRCounter->GetEvHistoBin(SR) , w);
			  //~ if ( SR == xAOD4L::SR::SR0 )  Histos->FillHistogram(EvType+"_SR_ev_runII", 1. ,w);
			  //~ if ( SR == xAOD4L::SR::SR0Z )  Histos->FillHistogram(EvType+"_SR_ev_runII", 2. ,w);
			  if ( SR == xAOD4L::SR::SR0noZ)
				{
				 //~ Histos->FillHistogram(EvType+"_SR_ev_runII", 3. ,w);
				 if(Meff> 700.*GeVToMeV)
					{
					 Histos->FillHistogram(EvType+"_SR_ev_runII", 4. ,w);
					 if (Met> 25.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 5 ,w);
					 if (Met> 50.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 6. ,w);
					 if (Met> 75.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 7. ,w);
					}
				if(Meff> 800.*GeVToMeV)
					{
					 Histos->FillHistogram(EvType+"_SR_ev_runII", 8. ,w);
					 if (Met> 25.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 9 ,w);
					 if (Met> 50.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 10. ,w);
					 if (Met> 75.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 11. ,w);
					}
				 if(Meff> 900.*GeVToMeV)
					{
					 Histos->FillHistogram(EvType+"_SR_ev_runII", 12. ,w);
					 if (Met> 25.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 13 ,w);
					 if (Met> 50.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 14. ,w);
					 if (Met> 75.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 15. ,w);
					}
				 if(Meff> 1000.*GeVToMeV)
					{
					 Histos->FillHistogram(EvType+"_SR_ev_runII", 16. ,w);
					 if (Met> 25.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 17 ,w);
					 if (Met> 50.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 18. ,w);
					 if (Met> 75.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 19. ,w);
					}
				 if(Meff> 1100.*GeVToMeV)
					{
					 Histos->FillHistogram(EvType+"_SR_ev_runII", 20. ,w);
					 if (Met> 25.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 21 ,w);
					 if (Met> 50.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 22. ,w);
					 if (Met> 75.*GeVToMeV) Histos->FillHistogram(EvType+"_SR_ev_runII", 23. ,w);
					}
				} 
			}
		  if (SR==xAOD4L::SR::nDef) nDef=false;
		  if ( !GetRunIRegions && SRCounter->IsRunIRegion(SR) ) continue;
		  if ( !GetRunIIRegions && SRCounter->IsRunIIRegion(SR) ) continue;
		  
		  std::string Pre =EvType+"_"+(SR!=xAOD4L::SR::nDef ? PrintSignalRegion(SR)+"-" : "");
		 
		  FillBosonHistogram ( Pre , Lep , Meff , SR , IsReco);
		  if ( xAOD4LEventRegions::Is2LeptonRegion(SR) || !( xAOD4LEventRegions::IsRunIRegion(SR) || xAOD4LEventRegions::IsRunIIRegion(SR) ) )FillMeffHistograms (Pre,  Met , Meff, Ht_Lep , Ht_Jet , Ht_Ph , w);
		  if ( Is4LeptonRegion (SR) && Lep.size()==4) 
			{
			 float FourMass = FourPartInvariantMass (Lep.at(0) , Lep.at(1) , Lep.at(2) , Lep.at(3) ) /GeVToMeV;
			 Histos->FillHistogram(Pre+"M_4Lep" , FourMass < 490. ? FourMass : 490. , w );
			}
		  if ( Is4LeptonRegion(SR) )Histos->FillHistogram(Pre+"EvCharge" ,EvCharge , w ); 
		  Histos->FillNumHistogram(Pre+"NumJet" , NumMeffJet , w);
		 
		  FillParticleHistograms(El , Pre+"el" , w);
		  FillParticleHistograms(Mu , Pre+"mu" , w);
		  FillParticleHistograms(Jet , Pre+"jet", w);
		//FillParticleHistograms(Ph , Pre+"Y" , w);
		  if (GetTau==true) FillParticleHistograms(Tau , Pre+"tau" , w);		  
		  	  
		  if ( LeadPh!=NULL )	
			{
			 FillParticleHistograms(LeadPh , Pre+"Y" , w );
			 Histos->FillHistogram(Pre+"Y-pt-Meff" , LeadPh->pt() / GeVToMeV , Meff/ GeVToMeV , w);
			 for(int i=0;SR!=xAOD4L::SR::nDef && i<8;++i)  
				if( Meff > MeffCuts[i]*GeVToMeV )FillParticleHistograms(LeadPh,Form("%sY_MeffC_%i",Pre.c_str(),MeffCuts[i]) , w);
	
			}		
	 	}
	 }		
void xAOD4LAcceptance::DrellYangVeto (std::vector<const xAOD::IParticle*> &BaseLep , const std::string &Pre , double &w)
	{
	 if(BaseLep.size() < 2) return;
	 bool Has4BaseLep = (BaseLep.size() >= 4);
	 float_t BestSFOSPair(1.e9), BestDFOSPair(1.e9), ClosestSFOSPair(1.e9),dR_SFOS(1.e4),ClosestDFOSPair(1.e9),dR_DFOS(1.e4),
			 SecondSFOSPair(1.e9) , SecondDFOSPair(1.e9);
	 for ( std::vector<const xAOD::IParticle*>::iterator Lep = BaseLep.begin() ; Lep  != BaseLep.end() ; ++ Lep)
		{
		 for( std::vector<const xAOD::IParticle*>::iterator Lep1 = Lep + 1; Lep1  != BaseLep.end() ; ++ Lep1)
			{
			 float DiMass = TwoPartInvariantMass( *Lep ,*Lep1);
			 if (SFOS (*Lep , *Lep1) == true  ) 
				{
				 if (TMath::Abs(BestSFOSPair - xAOD4L::Z_MASS) > TMath::Abs(DiMass - xAOD4L::Z_MASS))
					{
					 SecondSFOSPair = BestSFOSPair;
					 BestSFOSPair = DiMass;
					}
				else if  (TMath::Abs(SecondSFOSPair - xAOD4L::Z_MASS) > TMath::Abs(DiMass - xAOD4L::Z_MASS)) SecondSFOSPair=DiMass;
				}
			
			 else if (DFOS(*Lep,*Lep1) == true &&  BestDFOSPair > DiMass   ) 
				{
				 SecondDFOSPair = BestDFOSPair;				
				 BestDFOSPair = DiMass;
				}
			}	
			const xAOD::IParticle* NearLep = GetNearestParticle( *Lep , BaseLep , BaseLep);	
			if(NearLep!=NULL && SameSign(*Lep,NearLep) == false)
				{
				 float DiMass = TwoPartInvariantMass( *Lep , NearLep);
				 float dR =DeltaR( *Lep , NearLep ,false );
		 	  	 if ( SFOS(*Lep,NearLep)==true && Overlap(*Lep , NearLep , dR_SFOS) )
					{
					 dR_SFOS = dR;
					 ClosestSFOSPair = DiMass;	
					}				
				else if ( DFOS(*Lep,NearLep)==true && Overlap(*Lep , NearLep , dR_DFOS) )
					{
					 dR_DFOS = dR;
					 ClosestDFOSPair = DiMass;
					 }
		 	  	 //Filling the distributions of each Pair;
		 	  	 std::string N = DFOS(*Lep , NearLep)==true ? "DFOS" : "SFOS"; 
		 	  	 Histos->FillHistogram(Pre+"_"+N+"_dR-M" , dR , DiMass / GeVToMeV , w);
				 if (Has4BaseLep==true)Histos->FillHistogram(Pre+"_4l-"+N+"_dR-M" , dR , DiMass / GeVToMeV , w);	
				}				 
		}		
	  Histos->FillHistogram(Pre+"_SFOS-B_M" ,  BestSFOSPair / GeVToMeV , w);
	  Histos->FillHistogram(Pre+"_DFOS-B_M" ,  BestDFOSPair / GeVToMeV , w);
	  
	  Histos->FillHistogram(Pre+"_SFOS-B_M" ,  SecondSFOSPair / GeVToMeV , w);
	  Histos->FillHistogram(Pre+"_DFOS-B_M" ,  SecondDFOSPair / GeVToMeV , w);
	  
	  Histos->FillHistogram(Pre+"_SFOS-C_M" ,  ClosestSFOSPair / GeVToMeV , w);
	  Histos->FillHistogram(Pre+"_DFOS-C_M" ,  ClosestDFOSPair / GeVToMeV , w);
	  
	  Histos->FillHistogram(Pre+"_SFOS-C_dR" , dR_SFOS , w);
	  Histos->FillHistogram(Pre+"_DFOS-C_dR" , dR_DFOS , w);	  
	  
	  Histos->FillHistogram(Pre+"_SFOS-C_dR-M" , dR_SFOS ,ClosestSFOSPair / GeVToMeV, w);
	  Histos->FillHistogram(Pre+"_DFOS-C_dR-M" , dR_DFOS ,ClosestDFOSPair / GeVToMeV, w);
	  
	  if (Has4BaseLep==true)
		{
		 Histos->FillHistogram(Pre+"_4l-SFOS-B_M" ,  BestSFOSPair / GeVToMeV , w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-B_M" ,  BestDFOSPair / GeVToMeV , w);
		
		 Histos->FillHistogram(Pre+"_4l-SFOS-C_M" ,  ClosestSFOSPair / GeVToMeV , w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-C_M" ,  ClosestDFOSPair / GeVToMeV , w);
		
		 Histos->FillHistogram(Pre+"_4l-SFOS-C_dR" , dR_SFOS , w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-C_dR" , dR_DFOS , w);	  
		
		 Histos->FillHistogram(Pre+"_4l-SFOS-C_dR-M" , dR_SFOS ,ClosestSFOSPair / GeVToMeV, w);
		 Histos->FillHistogram(Pre+"_4l-DFOS-C_dR-M" , dR_DFOS ,ClosestDFOSPair / GeVToMeV, w);	
		}
	}
 void xAOD4LAcceptance::Execute()
	{
	 xAOD4LSelection::Execute();
	 WriteEventNumberInFile();
	
	}
void xAOD4LAcceptance::AnalyzeRecoEvents()
	 {
	  EvTrigger = PassEventSelection();
	  
	  AnalyzeCommonEvent("rec" , rec_el , rec_mu , rec_jets , rec_tau , rec_ph , rec_MET);
	    
	  std::vector<const xAOD::Electron*>::const_iterator E;
	  std::vector<const xAOD::Muon*>::const_iterator M;
	  std::vector<const xAOD::IParticle*>::const_iterator P;
	
	  std::vector<const xAOD::Electron*> NonIsoEl = GetNonIsoSignal (rec_BaseEl);
	  std::vector<const xAOD::Muon*> NonIsoMu = GetNonIsoSignal(rec_BaseMu);
	  rec_NonIsoLep =  BuildLeptonVector ( NonIsoEl , NonIsoMu );
	  
		
	 //DrellYangVeto ( rec_NonIsoLep , "rec" , reco_weight);
	 DrellYangVeto ( rec_Lep , "rec" , reco_weight);
	 bool FourNonIsoLep = (rec_NonIsoLep.size() > 3);
	 for(E = NonIsoEl.begin(); E!= NonIsoEl.end();++E)  
		{
		 if (FourNonIsoLep && PassSignalSelection(*E) ) FillParticleHistograms(*E , "rec_4lIso-el" , weight);
		 FillIsolationHistograms("el",*E);
		}
	 for(M = NonIsoMu.begin(); M!= NonIsoMu.end();++M)  
		{
		 if (FourNonIsoLep && PassSignalSelection(*M) ) FillParticleHistograms(*M , "rec_4lIso-mu" , weight);
		 FillIsolationHistograms("mu",*M);
		}
	 FillDRHistograms("rec_BOR_" ,rec_PreEl,rec_PreMu,rec_PreTau,rec_PreJets,reco_weight);	 
	 //~ FillDRHistograms("rec_" ,rec_BaseEl,rec_BaseMu,rec_BaseTau,rec_BaseJets,reco_weight);	 	 
	 FillDRHistograms("rec_" ,NonIsoEl,NonIsoMu,rec_BaseTau,rec_BaseJets,reco_weight);	 	 
	}
void xAOD4LAcceptance::FillIsolationHistogram (const  std::string &IsoName , float Iso ,  const std::string &ConeName ,  float Cone , bool Pass4Lep)
	{
	 Histos->FillHistogram("rec_"+IsoName , Iso  < 0.98 ? Iso : 0.98 , reco_weight);
	 Histos->FillHistogram("rec_"+ConeName, (Cone / GeVToMeV < 200. ? Cone / GeVToMeV : 199.99), reco_weight);
	 if (Pass4Lep)
		{
		 Histos->FillHistogram("rec_4l-"+IsoName , Iso  < 0.98 ? Iso : 0.98 , reco_weight);
		 Histos->FillHistogram("rec_4l-"+ConeName,  (Cone / GeVToMeV < 200. ? Cone / GeVToMeV : 199.99) , reco_weight);	
		}
	}
void xAOD4LAcceptance::FillIsolationHistogram (const  std::string &IsoName , float Cone , float Et,  float dR, bool Pass4Lep , const std::string &NearPart)
	{
	 float Value = Cone / Et;
	// if ( Value > 2.9) Value=2.9;
//	 else if ( Value < -2.) Value=-2.;
	
	 Histos->FillHistogram("rec_"+IsoName+"dR" , dR,  Value , weight);
	 if (NearPart.size() > 0) Histos->FillHistogram("rec_"+IsoName+"dR-"+NearPart , dR,  Value , weight);
	 
	 if(Pass4Lep==true)  
		{
		 Histos->FillHistogram("rec_4l-"+IsoName+"dR" , dR, Value, reco_weight);		
		 if (NearPart.size() > 0) Histos->FillHistogram("rec_4l-"+IsoName+"dR-"+NearPart , dR, Value, weight);
		}
	}
void  xAOD4LAcceptance::GetNumLepFromVDecay ( std::vector<const xAOD::TruthParticle*> &Lep , unsigned int &NumW , unsigned int &NumZ)
	{
	 NumW=NumZ=0;
	 std::vector<const xAOD::TruthParticle*>::const_iterator L;
	
	  for ( L = Lep.begin() ; L!=Lep.end() ; ++L)
		{
		 if (TruthParticleHelpers::OriginatesFromZDecay(*L) || TruthParticleHelpers::OriginatesFromYDecay(*L)  )  ++NumZ;
		 else if (TruthParticleHelpers::OriginatesFromWDecay(*L) ) ++NumW;
		}
	}
   
void xAOD4LAcceptance::AnalyzeTruthEvents()
	{
	 EvTrigger= PassLeptonTrigger(truth_el,truth_mu);
	 AnalyzeCommonEvent("tru" , truth_el , truth_mu , truth_jets , truth_tau , truth_ph , truth_MET , false);
	 
	 std::vector<const xAOD::IParticle*> BaseLep =  BuildLeptonVector( truth_BaseEl , truth_BaseMu );
	 DrellYangVeto ( BaseLep, "tru" , weight);
	 if (truth_BaseEl.size() == 1 && truth_BaseMu.size()== 3 && truth_Lep.size() < 4)
		{
		 PrintAllParents( truth_BaseEl.at(0));
		 for (std::vector<const xAOD::TruthParticle*>::const_iterator L = truth_BaseMu.begin() ; L!=truth_BaseMu.end() ; ++L) PrintAllParents (*L);
		}
	 FillDRHistograms("tru_BOR_" ,truth_PreEl,truth_PreMu,truth_PreTau,truth_PreJets,weight);
	 FillDRHistograms("tru_", truth_el,truth_mu,truth_tau,truth_jets,weight);
	 //~ if (truth_el.size() == 4)
		//~ {
		 //~ for (std::vector<const xAOD::TruthParticle*>::const_iterator L= truth_el.begin() ; L!= truth_el.end() ; ++L)
			//~ {
			 //~ float_t M(-1),M1(-1);
			 //~ for (std::vector<const xAOD::TruthParticle*>::const_iterator L1= L +1 ; L1!= truth_el.end() ; ++L1)
				//~ {
				 //~ if (SFOS (*L , *L1) == false ) continue;
				 //~ float m = TwoPartInvariantMass(*L , *L1);
				 //~ if (M < 0 ) M=m;
				 //~ else if (M1 < 0 ) M1=m;
				 //~ else std::cout<<"WTF"<<std::endl;
				//~ }
			 //~ PrintAllParents(*L, Form("M1: %.2f GeV M2: %.2f GeV", M,M1));
			//~ }
		//~ }
	 //SUSY variables
	 if (ContainsSUSY==true)
		{
		size_t NumE (0),NumM(0),NumT(0);
		std::vector<const xAOD::TruthParticle*>::const_iterator TruItr;
		for(TruItr=truth_Sparticles.begin();TruItr!=truth_Sparticles.end();++TruItr)
			{
			 if( (*TruItr)->absPdgId()==1000022) 
				{
				 TLorentzVector Neutralino = (*TruItr)->p4();
				 FillParticleHistograms(Neutralino,"tru_LSP",weight);
				 GetChargedLeptonMultiplicity((*TruItr), NumE,NumM,NumT);				 
				}
			 else
				{ 
				 FillParticleHistograms(*TruItr,"tru_NLSP",weight);
				}
			}
		Histos->FillLepMultHistogram("tru_RPVdecay",NumE,NumM,NumT,weight);		
		}	
	}
xAOD4LAcceptance::~xAOD4LAcceptance()
	{}
void xAOD4LAcceptance::InitTruthHistos( )
	{
	 InitCommonHistos("tru");
	 if(ContainsSUSY==true)  Histos->CreateNewHisto(HistManager::DefinedHistos::LepMul,"tru_RPVdecay", "Decay modes of the RPV LSP decays");
	 std::string Boson[]={"W","LSP","NLSP"};
	 std::string BosTitle[]={"W^{#pm}","#tilde{#chi}^{0}_{1}","NLSP"};
	 
	 int MeffCuts[]={300,600,700,800,900,1000,1100,1200};	 
	 for (unsigned int a=1;Get2LeptonsEvents==true && a<6;++a) 
		{Histos->CreateNewHisto(HistManager::DefinedHistos::meff,Form("tru_2l-Meff-%uJet",a),"M_{eff} contribution of the jets splitted up in the jet multiplicity",Form("M_{eff}(%i jets) [GeV]",a));}
	 
	 //Boson Histograms
	 for (unsigned int a=0;a<3;++a)
		{
		 if (a==0 && GetW==false)continue;
		 else if (a>0 && ContainsSUSY==false)break;
		 for (int b=xAOD4L::SR::nDef;b!=xAOD4L::SR::R1SR0Z;b++)
			{
			 xAOD4L::SR R=(xAOD4L::SR)b;
			 if(Get2LeptonsEvents==false && R!=xAOD4L::SR::nDef && Is4LeptonRegion(R)==false)continue;
			 if(Get4LeptonsEvents==false && R!=xAOD4L::SR::nDef && Is4LeptonRegion(R)==true)continue;
			 if(GetTau==false && IsTauRegion(R)==true) continue;
			 std::string SR = "tru_"+(R!=xAOD4L::SR::nDef?PrintSignalRegion(R)+"-":"");	
			 InitParticleHistograms(SR , Boson[a] , BosTitle[a] , HistManager::DefinedHistos::lowmed_pt);	
			 if(R==xAOD4L::SR::nDef) continue;
			 for(unsigned int c=0; c<8;++c)  InitParticleHistograms( SR , Form ("%s_MeffC_%i",Boson[a].c_str() , MeffCuts[c]) , BosTitle[a] , HistManager::DefinedHistos::lowmed_pt);
			 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,Form("%s%s-pt-Meff",SR.c_str(),Boson[a].c_str()),Form("P_{T} of the selected %s",BosTitle[a].c_str()),"m_{eff} [GeV]", Form("p_{T}(%s) [GeV]",BosTitle[a].c_str()));			 
			}		
		}	
	}
void xAOD4LAcceptance::InitCommonHistos(const std::string &Pre)
	{
	 int MeffCuts[]={300,600,700,800,900,1000,1100,1200};	 
	 std::string Particle[]={"el","mu","jet","tau"};
	 std::string ParTitle[]={"e","#mu","jets","#tau"};
	 std::string DeltaR[]={"ee","emu","mumu","ej","muj","jj","etau","mutau","jtau","tautau"};
	 std::string DeltaRTit[]={"electron - electron","electron - #mu", "#mu - #mu","electron - jet", "#mu -jet", "jet - jet","electron - #tau","#mu - #tau"};
	 std::string DeltaRxAxis[]={"e,e","e,#mu","#mu,#mu","e,jet","#mu,jet","jet,jet","e,#tau","#mu,#tau" , "jet,#tau" , "#tau,#tau"};
	 std::string LPair [] = {"DFOS" , "SFOS"};
	 std::string Boson[]={"Y","Z"};
	 std::string BosTitle[]={"#gamma","Z^{0}"};
	
	 for (int b=xAOD4L::SR::nDef;b!=xAOD4L::SR::R2SR1Z;++b)
		{
		 xAOD4L::SR R=(xAOD4L::SR)b;
		 if ( !Get2LeptonsEvents && xAOD4LEventRegions::Is2LeptonRegion(R) )continue;
		 if ( !Get4LeptonsEvents && xAOD4LEventRegions::Is4LeptonRegion(R) )continue;
		 if ( GetTau==false && IsTauRegion(R)==true) continue;
		 if (!GetRunIIRegions && xAOD4LEventRegions::IsRunIIRegion(R) ) continue;
		 if (!GetRunIRegions  && xAOD4LEventRegions::IsRunIRegion(R) ) continue;
		 std::string SR =Pre+"_"+(R!=xAOD4L::SR::nDef?PrintSignalRegion(R)+"-":"");				
		 for(unsigned int a=0;a<4;++a)
			{
			 if(GetTau==false && a==3) continue;
			 InitParticleHistograms (SR , Particle[a] , ParTitle[a], a!=2 ? HistManager::DefinedHistos::low_pt : HistManager::DefinedHistos::Vlow_pt );
			}
		 for (unsigned int a=0;a<2;++a)
			{
			 if(a==0 && GetPhoton==false) continue;
			 if( a==1 && GetZ==false) continue;		
				{
				 if(GetZ==false && IsZRegion(R)==true) continue;
				 InitParticleHistograms( SR , Boson[a] , BosTitle[a] , HistManager::DefinedHistos::lowmed_pt);
				 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,Form("%s%s-pt-Meff",SR.c_str(),Boson[a].c_str()),Form("P_{T} of the selected %s",BosTitle[a].c_str()),"m_{eff} [GeV]", Form("p_{T}(%s) [GeV]",BosTitle[a].c_str()));			 
				 if(R==xAOD4L::SR::nDef) continue;
				 for(unsigned int c=0; c<8;++c)  InitParticleHistograms( SR , Form ("%s_MeffC_%i",Boson[a].c_str() , MeffCuts[c]) , BosTitle[a] , HistManager::DefinedHistos::lowmed_pt);
				}	
			}		
		 Histos->CreateNewHisto(HistManager::DefinedHistos::NumJet,SR+"NumJet", "Number of Jets per event");
		 if (Get4LeptonsEvents==true && Is4LeptonRegion(R)) 
			{
			 Histos->CreateNewHisto(HistManager::DefinedHistos::lowmed_pt ,  Form( "%sM_4Lep",SR.c_str() ) ,"Invariant mass of the 4 lepton  pair" , "m_{4 Lepton} [GeV]");
			 Histos->CreateNewHisto(HistManager::DefinedHistos::NumLep,SR+"EvCharge", "Charge of four lepton evnts", "Total lepton charge Q");
			 Histos->SetBinLabelX(SR+"EvCharge" , 1 , "-2");		 
			 Histos->SetBinLabelX(SR+"EvCharge" , 2 , "-1");
			 Histos->SetBinLabelX(SR+"EvCharge" , 3 , "0");
			 Histos->SetBinLabelX(SR+"EvCharge" , 4 , "1");
			 Histos->SetBinLabelX(SR+"EvCharge" , 5 , "2");	
			 Histos->SetBinLabelX(SR+"EvCharge" , 6 , "3");		 		
			}
		 if (GetZ==true) Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Z-DiLepMass","M_{eff}  E^{miss}_{T} in 4l events","M_{#el#el} [GeV]","M_{#el#el} [GeV]");
		 
		 if ( xAOD4LEventRegions::IsRunIIRegion(R) || xAOD4LEventRegions::IsRunIRegion(R) ) continue;
		
		 Histos->CreateNewHisto(HistManager::DefinedHistos::met,SR+"Met","E^{miss}_{T} in 4l events");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Meff-Met","M_{eff}  E^{miss}_{T} in 4l events","m_{eff} [GeV]");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::meff_met,SR+"Ht-Met","H_{T}  E^{miss}_{T} in 4l events","H_{T} [GeV]");
		 Histos->CreateNewHisto( !ContainsSUSY ? HistManager::DefinedHistos::meff :HistManager::DefinedHistos::high_pt,SR+"Meff","M_{eff} in 4l events","m_{eff} [GeV]");		 
		 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Lep","M_{eff} contribution of the Leptons in 4l events","H_{T} (leptons) [GeV]");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Jet","M_{eff} contribution of the Jets in 4l events","H_{T} (jets) [GeV]");
		 if(UseExtendedMeff==true) Histos->CreateNewHisto(HistManager::DefinedHistos::meff,SR+"Meff-Y","M_{eff} contribution of the #gamma in 4l events","H_{T} (#gamma) [GeV]");		
		 
		 if (R==xAOD4L::SR::nDef || R==xAOD4L::SR::SR0)
			{
			 for( int l=0; l < 2 ; ++l)
				{
				 Histos->CreateNewHisto(HistManager::DefinedHistos::dR_M , Form( "%s_dR-M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the %s pair vs. its separation",LPair[l].c_str()), "#DeltaR (l_{1} , l_{2} )", Form("m_{%s} [GeV]",LPair[l].c_str()) );
				 Histos->CreateNewHisto(HistManager::DefinedHistos::dR_M , Form( "%s-C_dR-M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the %s pair vs. its separation",LPair[l].c_str()), "#DeltaR (l_{1} , l_{2} )", Form("m_{%s} [GeV]",LPair[l].c_str()) );
				 Histos->CreateNewHisto(HistManager::DefinedHistos::dR05 ,  Form( "%s-C_dR",(SR+LPair[l]).c_str()) , Form("Separation of a the leptons in a %s pair",LPair[l].c_str()) ,Form("#Delta R_{%s}",LPair[l].c_str()));
				 Histos->CreateNewHisto(HistManager::DefinedHistos::low_pt ,  Form( "%s-C_M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the closest %s pair",LPair[l].c_str()) ,Form("m_{%s} [GeV]",LPair[l].c_str()));
				 Histos->CreateNewHisto(HistManager::DefinedHistos::low_pt ,  Form( "%s-B_M",(SR+LPair[l]).c_str()) , Form("Invariant mass of the best %s pair",LPair[l].c_str()) ,Form("m_{%s} [GeV]",LPair[l].c_str()));					
				}
			}
		 		
		}
	 Histos->CreateNewHisto(HistManager::DefinedHistos::SR , Pre+"_SR_ev","Number of events passing the SR cuts");
	 Histos->CreateNewHisto(HistManager::DefinedHistos::SRw , Pre+"_SRw_ev","Number of events passing the SR cuts");
	 Histos->CreateNewHisto(HistManager::DefinedHistos::SRw , Pre+"_SR_ev_runII","Number of events passing the SR cuts");
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,4, "m_{eff}>700" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,5, "m_{eff}>700, E_{T}^{miss}>25" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,6, "m_{eff}>700, E_{T}^{miss}>50" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,7, "m_{eff}>700, E_{T}^{miss}>75" );	
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,8, "m_{eff}>800" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,9, "m_{eff}>800, E_{T}^{miss}>25" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,10, "m_{eff}>800, E_{T}^{miss}>50" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,11, "m_{eff}>800, E_{T}^{miss}>75" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,12, "m_{eff}>900" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,13, "m_{eff}>900, E_{T}^{miss}>25" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,14, "m_{eff}>900, E_{T}^{miss}>50" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,15, "m_{eff}>900, E_{T}^{miss}>75" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,16, "m_{eff}>1000" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,17, "m_{eff}>1000, E_{T}^{miss}>25" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,18, "m_{eff}>1000, E_{T}^{miss}>50" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,19, "m_{eff}>1000, E_{T}^{miss}>75" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,20, "m_{eff}>1100" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,21, "m_{eff}>1100, E_{T}^{miss}>25" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,22, "m_{eff}>1100, E_{T}^{miss}>50" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,23, "m_{eff}>1100, E_{T}^{miss}>75" );
	 Histos->SetBinLabelX(Pre+"_SR_ev_runII" ,24, "" );
	 
	 Histos->CreateNewHisto(HistManager::DefinedHistos::NumLep , Pre+"_NumLep", "Number of Leptons per event");	
	 if (Get4LeptonsEvents==true) 
		{
		 Histos->CreateNewHisto(HistManager::DefinedHistos::LepMul , Pre+"_LepMul", "Multiplicity of each lepton flavour in 4l events");
		 Histos->CreateNewHisto(HistManager::DefinedHistos::LepMulnT , Pre+"_LepMulnT", "Multiplicity of each lepton flavour in 4l events");		 
		}
		
	 for (unsigned int a=0;a<10;++a)
		{
		 if(GetTau==false && a>5)continue;
		 Histos->CreateNewHisto( HistManager::DefinedHistos::dR15 , Form("%s_BOR_WdR_%s",Pre.c_str(),DeltaR[a].c_str()),Form("Separation of the %s",DeltaRTit[a].c_str()),Form("#DeltaR(%s)",DeltaRxAxis[a].c_str() ));
		 Histos->CreateNewHisto( HistManager::DefinedHistos::dR15 , Form("%s_WdR_%s",Pre.c_str(),DeltaR[a].c_str()),Form("Separation of the %s",DeltaRTit[a].c_str()),Form("#DeltaR(%s)",DeltaRxAxis[a].c_str() ));
		 Histos->CreateNewHisto( HistManager::DefinedHistos::dR05 , Form("%s_BOR_dR_%s",Pre.c_str(),DeltaR[a].c_str()),Form("Separation of the %s",DeltaRTit[a].c_str()),Form("#DeltaR(%s)",DeltaRxAxis[a].c_str() ));
		 Histos->CreateNewHisto( HistManager::DefinedHistos::dR05 , Form("%s_dR_%s",Pre.c_str(),DeltaR[a].c_str()),Form("Separation of the %s",DeltaRTit[a].c_str()),Form("#DeltaR(%s)",DeltaRxAxis[a].c_str() ));
		}	
	}
void xAOD4LAcceptance::InitParticleHistograms (  std::string Pre ,std::string Particle , std::string xTitle, HistManager::DefinedHistos PtType )
	{
	 Histos->CreateNewHisto( PtType , Pre+Particle+"-pt" , "P_{T} of the selected "+xTitle , "p_{T}("+xTitle+") [GeV]");
	 Histos->CreateNewHisto(HistManager::DefinedHistos::eta,  Pre+Particle+"-eta", "#eta of the selected"+xTitle , "#eta ("+xTitle+")");
	 Histos->CreateNewHisto(HistManager::DefinedHistos::phi,  Pre+Particle+"-phi", "#phi of the selected"+xTitle ,"#phi ("+xTitle+")");
	}
	
void xAOD4LAcceptance::InitRecoHistos()
	{
	 InitCommonHistos("rec");
	 std::string Particle[]={"el","mu" , "Trkel" , "Trkmu"};
	 std::string ParTitle[]={"e","#mu" , "T (e)" , "T (#mu)"};
	 std::string Cone[]= { "TopoEt" , "Pt" , "PtVar", "Et"};
	 
	 std::string ConeN[] = {"E_{T}^{TopoCone20}" , "p_{T}^{Cone30}" , "p_{T}^{VarCone30}" ,  "E_{T}^{Cone20}" };
	 std::string yTit[] = {"E_{T}(Cluster)" , "p_{T}(Track)" , "p_{T}(Track)" ,  "E_{T}(Cluster)" };
	
	 for (int a=0 ; a< 2 ;++a)
		{	 
		 InitParticleHistograms("rec_4lIso-" , Particle[a] ,  ParTitle[a] , HistManager::DefinedHistos::lowmed_pt);
		 //~ InitParticleHistograms("rec_" ,Particle[a]+"-C" , "Calo_{"+Particle[a]+"}");
		 for (int b=0 ; b <  3;++b)
			{ 
			 //~ Histos->CreateNewHisto (HistManager::DefinedHistos::lowPt_lowPt , "rec_"+Particle[a]+"_"+Cone[b]+"ConeComp" ,"Calorimeter Isolation","Original "+ConeN[b]+" ("+ParTitle[a]+")" , "Recalculated "+ConeN[b]+" ("+ParTitle[a]+")" );
			 //~ Histos->CreateNewHisto (HistManager::DefinedHistos::lowPt_lowPt , "rec_4l-"+Particle[a]+"_"+Cone[b]+"ConeComp" ,"Calorimeter Isolation","Original "+ConeN[b]+" ("+ParTitle[a]+")" , "Recalculated "+ConeN[b]+" ("+ParTitle[a]+")" );
			//~ 
			 Histos->CreateNewHisto (HistManager::DefinedHistos::Iso , "rec_"+Particle[a]+"_Iso"+Cone[b]+"Cone" ,"Calorimeter Isolation", ConeN[b]+" / "+ParTitle[a]+"");
			 Histos->CreateNewHisto (HistManager::DefinedHistos::Iso , "rec_4l-"+Particle[a]+"_Iso"+Cone[b]+"Cone" ,"Calorimeter Isolation", ConeN[b]+" /"+ParTitle[a]+"");
		
			Histos->CreateNewHisto (HistManager::DefinedHistos::IsoDR , "rec_"+Particle[a]+"_Iso"+Cone[b]+"ConedR" ,"Calorimeter Isolation", "#DeltaR("+ParTitle[a]+",Lep)", ConeN[b]+" ("+ParTitle[a]+") / "+yTit[b]+"");
			Histos->CreateNewHisto (HistManager::DefinedHistos::IsoDR , "rec_4l-"+Particle[a]+"_Iso"+Cone[b]+"ConedR" ,"Calorimeter Isolation", "#DeltaR("+ParTitle[a]+",Lep)", ConeN[b]+" ("+ParTitle[a]+") / ("+yTit[b]+"");
		 	
		 	Histos->CreateNewHisto (HistManager::DefinedHistos::IsoDR , "rec_"+Particle[a]+"_Iso"+Cone[b]+"ConedR-el" ,"Calorimeter Isolation", "#DeltaR("+ParTitle[a]+",Lep)", ConeN[b]+" ("+ParTitle[a]+") / "+yTit[b]+"");
			Histos->CreateNewHisto (HistManager::DefinedHistos::IsoDR , "rec_4l-"+Particle[a]+"_Iso"+Cone[b]+"ConedR-el" ,"Calorimeter Isolation", "#DeltaR("+ParTitle[a]+",Lep)", ConeN[b]+" ("+ParTitle[a]+") / ("+yTit[b]+"");
		 	
		 	Histos->CreateNewHisto (HistManager::DefinedHistos::IsoDR , "rec_"+Particle[a]+"_Iso"+Cone[b]+"ConedR-mu" ,"Calorimeter Isolation", "#DeltaR("+ParTitle[a]+",Lep)", ConeN[b]+" ("+ParTitle[a]+") / "+yTit[b]+"");
			Histos->CreateNewHisto (HistManager::DefinedHistos::IsoDR , "rec_4l-"+Particle[a]+"_Iso"+Cone[b]+"ConedR-mu" ,"Calorimeter Isolation", "#DeltaR("+ParTitle[a]+",Lep)", ConeN[b]+" ("+ParTitle[a]+") / ("+yTit[b]+"");
		 	
		 		
		 	Histos->CreateNewHisto (HistManager::DefinedHistos::IsoPt , "rec_"+Particle[a]+"_"+Cone[b]+"Cone" ,"Calorimeter Isolation",ConeN[b]+" ("+ParTitle[a]+") [GeV]");
		 	Histos->CreateNewHisto (HistManager::DefinedHistos::IsoPt , "rec_4l-"+Particle[a]+"_"+Cone[b]+"Cone" ,"Calorimeter Isolation",ConeN[b]+" ("+ParTitle[a]+") [GeV]");
			}
		 } 	
	}
template <typename E, typename M, typename T, typename J > void xAOD4LAcceptance::FillDRHistograms(const std::string &Pre , std::vector <E> &El,  std::vector <M> &Mu, std::vector <T> &Tau, std::vector <J> &Jet, double w)
	{
	 const xAOD::IParticle* NearPart=NULL;
	 typename std::vector<E>::const_iterator e;
	 typename std::vector<M>::const_iterator m;
	 typename std::vector<T>::const_iterator t;
	 typename std::vector<J>::const_iterator j;	
	 for ( e = El.begin() ; e != El.end() ; ++e )
		{
		 NearPart=GetNearestParticle( *e , El , El , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_ee", DeltaR( *e , NearPart ) , w / 2. );
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_ee", DeltaR( *e , NearPart ) , w / 2. );
		 NearPart=GetNearestParticle( *e , Mu , El , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_emu", DeltaR( *e , NearPart ) , w);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_emu", DeltaR( *e , NearPart ) , w);		 
		 NearPart=GetNearestParticle( *e , Jet , El , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_ej", DeltaR( *e , NearPart ) ,  w);		 
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_ej", DeltaR( *e , NearPart ) ,  w);	
		}
	 for ( m=Mu.begin() ; m != Mu.end() ; ++m )
		{
		 NearPart=GetNearestParticle( *m , Mu , Mu , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_mumu", DeltaR( *m , NearPart ) , w / 2. );
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_mumu", DeltaR( *m , NearPart ) , w / 2. );
		 NearPart=GetNearestParticle(*m , Jet , Mu , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_muj", DeltaR( *m , NearPart ) , w);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_muj", DeltaR( *m , NearPart ) , w);
		 		
		}
	for ( j=Jet.begin() ; j != Jet.end() ; ++j )
		{
		  NearPart=GetNearestParticle(*j , Jet , Jet , false);
		  if (NearPart!=NULL) Histos->FillHistogram(Pre+"dR_jj", DeltaR( *j , NearPart ), w / 2. );
		  if (NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_jj", DeltaR( *j , NearPart ), w / 2. );				
		}
	for ( t=Tau.begin() ; GetTau==true && t != Tau.end() ; ++t )
		{
		 NearPart=GetNearestParticle( *t , Tau , Tau , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_tautau" , DeltaR( *t , NearPart ) , w / 2. );
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_tautau" , DeltaR( *t , NearPart ) , w / 2. );
		 
		 NearPart=GetNearestParticle( *t ,Mu,Tau,false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_mutau", DeltaR( *t , NearPart ) , w);			 
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_mutau", DeltaR( *t , NearPart ) , w);			 
		
		 NearPart=GetNearestParticle( *t , Jet , Tau , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_jtau", DeltaR( *t , NearPart ) , w);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_jtau", DeltaR( *t , NearPart ) , w);
		
		 NearPart=GetNearestParticle(  *t , El , Tau , false);
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"dR_etau", DeltaR( *t , NearPart ) , w);		 		 
		 if(NearPart!=NULL) Histos->FillHistogram(Pre+"WdR_etau", DeltaR( *t , NearPart ) , w);		 		 
		
		}
	}
void xAOD4LAcceptance::FillMeffHistograms(const std::string &Pre, const double &Met, const double &Meff, const double &MeffLep, const double &MeffJet, const double &MeffPh, const double &w)
	{
	 Histos->FillHistogram (Pre+"Meff-Met",Meff/GeVToMeV,Met/GeVToMeV,w);
	 Histos->FillHistogram (Pre+"Ht-Met",(Meff-Met)/GeVToMeV,Met/GeVToMeV,w);	 
	 Histos->FillHistogram (Pre+"Met",Met/GeVToMeV,w);	
	 Histos->FillHistogram (Pre+"Meff",Meff/GeVToMeV,w);	 
	 Histos->FillHistogram (Pre+"Meff-Lep",MeffLep/GeVToMeV,w);
	 Histos->FillHistogram (Pre+"Meff-Jet",MeffJet/GeVToMeV,w);
	 if(UseExtendedMeff==true) Histos->FillHistogram (Pre+"Meff-Y",MeffPh/GeVToMeV,w);
	}
template <typename PartType> void xAOD4LAcceptance::FillParticleHistograms(const std::vector<PartType*> &Particle,const std::string &Name, const double &w)
	{
	 typename std::vector<PartType*>::const_iterator Itr;
	 for (Itr=Particle.begin(); Itr!=Particle.end();++Itr) FillParticleHistograms (*Itr,Name,w);	
	}
void xAOD4LAcceptance::FillParticleHistograms (const xAOD::IParticle* Particle,const std::string &Name,const double &w)
	{
	 if(Particle == NULL) return;
	 Histos->FillHistogram (Name+"-pt",Particle->pt()/GeVToMeV,w);
	 Histos->FillHistogram (Name+"-eta",Particle->eta(),w);
	 Histos->FillHistogram (Name+"-phi",Particle->phi(),w);		;	
	}
void xAOD4LAcceptance::FillParticleHistograms (TLorentzVector Momentum ,const std::string &Name,const double &w)
	{
	 Histos->FillHistogram (Name+"-pt",Momentum.Pt()/GeVToMeV,w);
	//Histos->FillHistogram (Name+"-eta",Momentum.Eta(),w);
	//Histos->FillHistogram (Name+"-phi",Momentum.Phi(),w);		
	}
void xAOD4LAcceptance::SetOption(const std::string Option, bool Switch)
	{
	 if ( strcmp(Option.c_str(), "GetW" )==0 )   GetW=Switch;
	 else if ( strcmp(Option.c_str(), "GetZ" )==0 ) GetZ=Switch;
	 else if ( strcmp(Option.c_str(), "GetH" )==0 ) GetHiggs=Switch;	
	 else if ( strcmp(Option.c_str(), "GetPh" )==0 ) GetPhoton=Switch;	
	 xAOD4LSelection::SetOption(Option,Switch);	 
	}
void xAOD4LAcceptance::PromptOptions()
	{
	 xAOD4LSelection::PromptOptions();		
	}
bool xAOD4LAcceptance::Is4LeptonRegion (xAOD4L::SR SR)
	{return  !(SR >=xAOD4L::SR::SR02L && SR<=xAOD4L::SR::SR02LnoZ);}
void xAOD4LAcceptance::FillBosonHistogram ( const std::string &Pre , std::vector<const xAOD::IParticle*> &Lep , const double & Meff , xAOD4L::SR SR ,  bool IsReco)
	{
	 static int MeffCuts[]={300,600,700,800,900,1000,1100,1200};
	 if (IsReco == false)
		{
		 if (GetZ==true)  	
			{
			 FillParticleHistograms(truth_Z,Pre+"Z",weight);
			 for(std::vector<const xAOD::TruthParticle*>::const_iterator Z = truth_Z.begin() ; Z!=truth_Z.end(); ++Z ) Histos->FillHistogram(Pre+"Z-pt-Meff", (*Z)->pt() / GeVToMeV , Meff/ GeVToMeV , weight);
			}
		  if (GetW==true)	FillParticleHistograms(truth_W , Pre+"W" , weight);
		  if (SR!=xAOD4L::SR::nDef)
			{  
			  for(int i=0; i<8;++i) 
				{
				 if(GetZ==true && truth_Meff > MeffCuts[i]*GeVToMeV) 	FillParticleHistograms(truth_Z,Form("%sZ_MeffC_%i",Pre.c_str(),MeffCuts[i]),weight);
				 if(GetW==true && truth_Meff > MeffCuts[i]*GeVToMeV) 	FillParticleHistograms(truth_W,Form("%sW_MeffC_%i",Pre.c_str(),MeffCuts[i]),weight);				
				}
			} 
		}
	else
		{
		 if ( GetZ==true)
			{
			 float BestZPair(1.e12),SecZPair(0.);
			 const xAOD::IParticle* L = NULL;
			 const xAOD::IParticle* L1 = NULL;
			 for(std::vector<const xAOD::IParticle*>::const_iterator l = Lep.begin() ; l != Lep.end(); ++l )
				{
				 for(std::vector<const xAOD::IParticle*>::const_iterator l1 = l+1 ; l1 != Lep.end(); ++l1 )
					{
					 if ( SFOS(*l , *l1 ) ==false) continue;
					 bool ZPairIsMC=false;
					 if (isMC && SampleNr >= 400000)
						{
						 const xAOD::TruthParticle* TruLep =TruthParticleHelpers::getTruthMatchedParticle (*l);
						 const xAOD::TruthParticle* TruLep1 =TruthParticleHelpers::getTruthMatchedParticle (*l1);
						 ZPairIsMC = !( TruthParticleHelpers::OriginatesFromWDecay(TruLep) ||	TruthParticleHelpers::OriginatesFromWDecay(TruLep1) );						
						}					 				 
					 if (  IsZPair( *l , *l1 , (BestZPair < xAOD4L::Z_MASSWINDOW ? BestZPair : xAOD4L::Z_MASSWINDOW) )  || ZPairIsMC )
						{
						 SecZPair = BestZPair;
						 L=*l;
						 L1=*l1;
						 BestZPair = TwoPartInvariantMass( *l , *l1 );
						}				 
					}
				}			 
			 if ( L == NULL || L1 == NULL ) return;
			 TLorentzVector DiLep = L->p4() + L1->p4();
			 FillParticleHistograms(DiLep,Pre+"Z",reco_weight);
			 Histos->FillHistogram(Pre+"Z-DiLepMass", BestZPair / GeVToMeV , SecZPair / GeVToMeV , reco_weight);
			 for(int i=0; SR!=xAOD4L::SR::nDef && i<8;++i) if(Reco_Meff > MeffCuts[i]*GeVToMeV)FillParticleHistograms(DiLep,Form("%sZ_MeffC_%i",Pre.c_str(),MeffCuts[i]),reco_weight);
			}		 
		}
	}
const xAOD::TruthParticle* xAOD4LAcceptance::MatchByBarCode ( const xAOD::TruthParticle* P)
	{
	 if( P==NULL )
		{
		 Error("xAOD4LAcceptance" , "In event %lli MatchByBarCode given NullPtr", readEvents);
		 return NULL;
		}
	 if (P->isElectron() == true)  for (std::vector<const xAOD::TruthParticle*>::const_iterator E =truth_PreEl.begin() ; E!= truth_PreEl.end() ; ++E ) if ( (*E)->barcode() == P->barcode()) return (*E);
	 else if (P->isMuon() == true) for (std::vector<const xAOD::TruthParticle*>::const_iterator M =truth_PreMu.begin() ; M!= truth_PreMu.end() ; ++M ) if ( (*M)->barcode() == P->barcode()) return (*M);
	 else if (P->isTau() == true ) for (std::vector<const xAOD::TruthParticle*>::const_iterator T =truth_PreTau.begin() ; T!= truth_PreTau.end() ; ++T ) if ( (*T)->barcode() == P->barcode()) return (*T);				
	 else if (P->isPhoton() == true ) for (std::vector<const xAOD::TruthParticle*>::const_iterator Ph =truth_PrePh.begin() ; Ph!= truth_PrePh.end() ; ++Ph ) if ( (*Ph)->barcode() == P->barcode()) return (*Ph);				
	return NULL;
	}

void xAOD4LAcceptance::WriteEventNumberInFile()
	{
	 bool Pass = PassEventSelection();
	 static std::ofstream OutList;
	 if (!OutList.is_open())
		{
		 std::string ListName= file_histo->GetName();
		 ListName = ListName.substr(0, ListName.size()-4)+"txt";
		 OutList.open(ListName);		
		}
	 else if (Pass) OutList<<SampleNr<<"	"<<eventInfo->eventNumber()<<std::endl;
	 if (!isMC)
		{
		 static std::vector<unsigned int> RunNumbers;
		 static std::vector<long long unsigned int> EvNumbers;
		 if (RunNumbers.empty())
			{
			 char* ROOTCOREDIR = getenv("ROOTCOREBIN");
			 if(!ROOTCOREDIR) Error("xAOD4LAcceptance", "Could not retireve the enviroment variable ROOTCOREBIN");
			 else
				{
				 std::string InputFile = Form("%s/../XAOD4LAcceptance/scripts/RunsOfInterest.txt", ROOTCOREDIR);
				 std::ifstream InList;
				 InList.open(InputFile);
				 if (!InList.good())  Error("xAOD4LAcceptance", "Could not  read in the runs of interest from %s", InputFile.c_str() );
				 else
					{
				     unsigned int Run(0);
				     long long unsigned int Event;
				     while ( InList >>Run >> Event)
						{
						 if (!IsEntryInList (Run , RunNumbers) ) RunNumbers.push_back(Run);						 
						 if (!IsEntryInList (Event , EvNumbers) ) EvNumbers.push_back(Event); 					 
						 Info("xAOD4LAcceptance" , "Found event number of interest %llu in run: %u", Event, Run); 
						}
				    }
				 InList.close();				 
				}		
			}
		 if ( Pass || (IsEntryInList(SampleNr, RunNumbers) && IsEntryInList(eventInfo->eventNumber() , EvNumbers) ) ) 
			{
			  std::ofstream OutEvent;
			  std::string ListName= file_histo->GetName();
			  ListName = Form("%s/EvOfInterst_%u_%llu.txt", ListName.substr(0, ListName.rfind("/") ).c_str() , SampleNr,eventInfo->eventNumber() );
			  OutEvent.open(ListName);
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  OutEvent<<"GRL: "<<BoolToString(m_grl->passRunLB(*eventInfo))<<std::endl;
			  OutEvent<<"Tile: "<<BoolToString( !(eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error) )<<std::endl;
			  OutEvent<<"Core: "<<BoolToString( !(eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) )<<std::endl;
			  OutEvent<<"LAr: "<<BoolToString( !(eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error) )<<std::endl;
			  OutEvent<<"HasBadJet: "<<BoolToString( HasBadJet )<<std::endl;
			  OutEvent<<"HasPrimVtx: "<<BoolToString( CheckVertices() )<<std::endl;
			  OutEvent<<"HasBadMuon: "<<BoolToString( HasBadMuon )<<std::endl;
			  OutEvent<<"HasCosmic: "<<BoolToString( HasCosmic )<<std::endl;
			  OutEvent<<"Event passed selection: "<<BoolToString(Pass)<<std::endl;					
			  OutEvent<<"###########################################################################################################"<<std::endl;
			 
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  OutEvent<<"				Write Electrons"<<std::endl;		  
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  for (xAOD::ElectronContainer::const_iterator E = ElectronContainer->begin() ; E!=ElectronContainer->end() ; ++E)
					OutEvent<<WriteParticleInformation(*E)<<std::endl;
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  OutEvent<<"				Write Muons"<<std::endl;		  
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  for (xAOD::MuonContainer::const_iterator M = MuonContainer->begin() ; M!=MuonContainer->end() ; ++M)
					OutEvent<<WriteParticleInformation(*M)<<std::endl;
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  OutEvent<<"				Write Taus"<<std::endl;		  
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  for (xAOD::TauJetContainer::const_iterator T = TauContainer->begin() ; T!=TauContainer->end() ; ++T)
					OutEvent<<WriteParticleInformation(*T)<<std::endl;			  		
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  OutEvent<<"				Write Jets"<<std::endl;		  
			  OutEvent<<"###########################################################################################################"<<std::endl;
			  for (xAOD::JetContainer::const_iterator J = JetContainer->begin() ; J!=JetContainer->end() ; ++J)
					OutEvent<<WriteParticleInformation(*J)<<std::endl;			  		
			
			OutEvent.close();
			}
		}
	 }
	 
std::string xAOD4LAcceptance::WriteParticleInformation(const xAOD::IParticle* P)
	{
	 std::string PartType="";
	 if (P->type() == xAOD::Type::ObjectType::Electron) PartType="Electron";
	 else if (P->type() == xAOD::Type::ObjectType::Muon) PartType="Muon";
	 else if (P->type() == xAOD::Type::ObjectType::Tau) PartType="Tau";
	 else if (P->type() == xAOD::Type::ObjectType::Jet) PartType="Jet";
	 else if (P->type() == xAOD::Type::ObjectType::Photon) PartType="Photon";
	 else if (P->type() == xAOD::Type::ObjectType::TrackParticle) PartType="Track";
	 else if (P->type() == xAOD::Type::ObjectType::CaloCluster ) PartType = "CaloCluster";
	
	 std::string Out=Form("xAOD4LAcceptance.WriteParticleInformation(%s) in Event %lli : 	M: %.4f GeV		pT: %.4f GeV		Eta:	%.4f	Phi: %.4f |	IsCandidate: %u	IsBaseline:	%u	PassOR:	%u	PassIsolation: %u	PassNonIsoSignal: %u	PassSignal:	%u", 
		PartType.c_str() , readEvents , P->m() / GeVToMeV , P->pt() / GeVToMeV , P->eta() , P->phi() , PassPreSelection(P) , 
		PassBaseSelection(P) , PassOverlapRemoval(P) , PassIsolation(P), PassNonIsoSignal(P) , PassSignalSelection(P));
		return Out;
		 
	 
	
	}

	 
