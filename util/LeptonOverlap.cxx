#include "CanvasPlot/GenericPlot.h"
#include "CanvasPlot/HistContainer.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
int main(int argc, char **argv)
	{
	 std::string InputFile="";
	 std::string DataType="D";
	 std::string Name="";
	 std::string Title="";
	 std::string OutDir="";
	 unsigned int NLegends = 3;
	 unsigned int NPlots = 8;	
	 std::string LegendTitle [] = { "#DeltaR(e, e)" , "#DeltaR(e, #mu)" , "#DeltaR(#mu, #mu)"};
	 std::string PlotName [] = { "ee" , "emu" , "mumu"};
	 std::string PlotDR  []=  { "tru_dR" , "tru_BOR_dR" , "rec_dR" , "rec_BOR_dR" , "tru_WdR" , "tru_BOR_WdR" , "rec_WdR" , "rec_BOR_WdR" };
	 
	 float Lumi=1000.;
	 bool Normalize=false;
	 bool DrawLabels=false;
	 //~ std::vector<std::string>	
	 
	 HistContainer HistMgr;		 
	 GenericPlot Plot;
	 
	 for (int a=1;a<argc;++a)
		{
		 if (strcmp(argv[a], "-I") == 0 && (a+1)!=argc) InputFile=argv[a+1];
		 else if (strcmp(argv[a], "-N")==0 && (a+1)!=argc) Name=argv[a+1]; //Name
		 else if (strcmp(argv[a], "-T")==0  && (a+1)!=argc) Title=argv[a+1];	
    	 else if (strcmp(argv[a], "-O")==0 && (a+1)!=argc) OutDir=Form("%s/",argv[a+1]); 
    	 else if (strcmp(argv[a], "-l")==0 && (a+1)!=argc) 
			{
			 Lumi=atof(argv[a+1]);
			 Normalize=true;	 
			}
		}
	 Info ("LeptonDRPlots" , "#########################################################################################################");
	 Info ("LeptonDRPlots" , "					Given Parameters to the code");	 
	 Info ("LeptonDRPlots" , "#########################################################################################################");
	 Info ("LeptonDRPlots" , "Name:		%s" , Name.c_str());
	 if ( Title.size()==0 ) Title=Name;
	 else Info("LeptonDRPlots" , "Title:		%s" , Title.c_str());
	 Info("LeptonDRPlots" ,"OutputDir:	%s" , OutDir.c_str());
	 Info("LeptonDRPlots" ,"InputFile:	%s", InputFile.c_str());
	 
	 if  (Normalize==true) HistMgr.SetLuminosity(Lumi);
	 
	 Plot.setAtlasStatus("Internal");
	 Plot.setLumi(Lumi);
	 
	 double x0=0.155;
	 double y0=0.85;
	 double yw=0.24;
	 double xw=0.54;		 
	 if( !HistMgr.ReadInTFiles(InputFile,DataType))
		{
		 Error ("LeptonDRPlots" , "Could not read all the Files");
		 return EXIT_FAILURE;
		}
	Info("LeptonDRPlots" , "File read in successful" );
	Plot.setSqrts(HistMgr.GetSqrtS()!=-1?HistMgr.GetSqrtS():13);	
	std::vector<std::string> HistList = HistMgr.GetCommonHistos();
	Info("LeptonDRPlots" , "Found %li Histograms" , HistList.size() );
	HistMgr.SetDrawOptions( DataType , HistContainer::DrawOptions::Component);
	for (unsigned int p =0 ; p < NPlots ; ++p)
		{
		 HistMgr.Reset();
		 Plot.PrepareLegend(Title,x0,y0,xw,yw);
		 unsigned int Found=0;
		 for (std::vector<std::string>::const_iterator H = HistList.begin(); Found < NLegends && H!=HistList.end() ; ++H)
			{
			 if ( (*H).find(PlotDR[p]) > (*H).size() ) continue;
			 for (unsigned int d=0 ; d < NLegends ; ++d )
				{
				 if ( (*H).find( PlotName[d] ) <  (*H).size() )
					{
					 HistMgr.AddHistogram( *H , LegendTitle[d] ,  DataType );
					 ++Found; 
					}
				}
			}
		HistMgr.AddHistosToLegend( Plot.l );
		if (Found == NLegends) std::cout<<"WUUUH SPRINGFIELD"<<std::endl;
		Plot.prepare_canvas(Form("LeptonOverlap-%s",PlotDR[p].c_str() ), 600.,1.);
		if ( HistMgr.Draw(Plot.p1,"Sc,xTit #DeltaR,")==true) Plot.SaveCanvas(OutDir+"Background_"+PlotDR[p] , true  , DrawLabels  );	
		}
	//~ 
	return EXIT_SUCCESS;
	}	 
