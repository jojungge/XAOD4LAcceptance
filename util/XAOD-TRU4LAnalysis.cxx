#include "sys/stat.h"
#include "sys/types.h"
#include <cerrno>
#include <unistd.h>
#include <stdlib.h>
#include <memory>
#include "XAOD4LAcceptance/TruXAOD4LAcceptance.h"
#ifdef ROOTCORE
#   include "xAODRootAccess/Init.h"
#   include "xAODRootAccess/TEvent.h"
#endif 
#include <TFile.h>

class TruTruxAOD4LAcceptance;
bool ConvertStrToBool(std::string Str);
int main( int argc, char* argv[] ) 
	{
	  TruxAOD4LAcceptance* Analysis= new TruxAOD4LAcceptance();
	   std::string outDir;
	  std::string InputFileList;
	  std::string SystematicFileList;
	  std::string InputFile;
	  std::string ProcessName;
	  size_t StartLine=0;
	  size_t EndLine=0;
	  size_t SkippedLines=0;
	  unsigned int JobNumber=0;
	  std::vector<std::string> Files;
	  std::vector<std::string> Systematic;
	  for ( int a=1;a<argc;++a)//Reading the Arguments parsed to the executable
		{
		 if (strcmp(argv[a], "-I") == 0 && (a+1)!=argc) 		InputFileList=argv[a+1];//InputFileList
		 else if (strcmp(argv[a], "-Ib")==0 && (a+1)!=argc) 	StartLine=(atoi(argv[a+1])-1);	
    	 else if (strcmp(argv[a], "-Ie")==0 && (a+1)!=argc) 	EndLine=(atoi(argv[a+1])-1);	
    	 else if (strcmp(argv[a], "-i")==0 && (a+1)!=argc) 	InputFile=argv[a+1];
    	 else if (strcmp(argv[a], "-JN")==0 && (a+1)!=argc)	JobNumber=atoi(argv[a+1]);	
    	 else if (strcmp(argv[a], "-N")==0 && (a+1)!=argc) 	ProcessName=argv[a+1];	
    	 else if (strcmp(argv[a], "-O")==0 && (a+1)!=argc) 	outDir=argv[a+1];	
    	 else if (strcmp(argv[a], "-s")==0 && (a+1)!=argc && Analysis->SetCMSEnergy(atoi(argv[a+1]))==false ) return EXIT_FAILURE;
		 else if (strcmp(argv[a], "--GetPhoton")==0 && (a+1)!=argc) 	Analysis->SetOption("Photon",ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--GetZ")==0 && (a+1)!=argc) 		Analysis->SetOption("Z", ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--GetW")==0 && (a+1)!=argc) 		Analysis->SetOption("W", ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--GetTau")==0 && (a+1)!=argc) 		Analysis->SetOption("Tau", ConvertStrToBool(argv[a+1]) );		
		 else if (strcmp(argv[a], "--Get4L")==0 && (a+1)!=argc) 		Analysis->SetOption("4LeptonEv",ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--Get2L")==0 && (a+1)!=argc) 		Analysis->SetOption("2LeptonEv",ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--SUSY")==0 ) 						Analysis->SetOption("SUSY",true);
		 else if (strcmp(argv[a], "--IsAtlfast")==0 ) 					Analysis->SetOption("AtlFAST",true);
		 else if (strcmp(argv[a], "--NoXSec")==0 ) 					Analysis->SetOption("xSecWeight",false);
		 else if (strcmp(argv[a], "--DFOSVeto")==0 && (a+1)!=argc) 	Analysis->SetOption("DFOSVeto",ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--GetTrees")==0  && (a+1)!=argc) 	Analysis->SetOption("Trees",ConvertStrToBool(argv[a+1]) );
		 else if (strcmp(argv[a], "--GetHistos")==0 && (a+1)!=argc)	Analysis->SetOption("Histos",ConvertStrToBool(argv[a+1]) );	
		 }
	 if (ProcessName.size()==0)
		{
		 Error("xAOD-SUSY4LAnalysis","The Process-Name is not defined");
		 return EXIT_FAILURE;
		}
	if (outDir.size()==0)
		{
		 Error(ProcessName.c_str(),"The Output directory is not defined");
		 return EXIT_FAILURE;		 
		}
	Info(ProcessName.c_str(),"#########################################################################################################");
	Info(ProcessName.c_str(),"						SUSY4LAnalysis in xAOD");
	Info(ProcessName.c_str(),"#########################################################################################################");  
	//Check the InputFiles
	if (InputFileList.size()>0)//InputFileList is parsed
		{
		 std::fstream list(InputFileList, std::fstream::in);
		 if(list.good())
			{	
			 std::string Line="";
			 while(getline(list,Line)) 
				{
				 if (Line.find("#")==0) 
					{
					 ++SkippedLines;
					 continue;
					}
				 if (Line.find(",")<Line.size())
					{
					 while (Line.find(",") <Line.size())
						{
						 std::size_t Pos=Line.find(",");	
						 std::string Data=Line.substr(0,Pos);
						 Files.push_back(Data);
						 Line=Line.substr ( Pos+1 , Line.size());		 
						}
					}
				 else Files.push_back(Line);
				}	
			}
		 else 
			{
			 Error(ProcessName.c_str(),"The given FileList does not exist");
			 return EXIT_FAILURE;		
			}
		}
	else if (InputFile.size()>0) 
		{
		 Files.push_back(InputFile); //1 single Input File
		 StartLine=EndLine=0;
		}
    else // No input file was given to the CODE
		{
		 Error(ProcessName.c_str(),"No Input File was parsed");
		 return EXIT_FAILURE;
		}
	if(StartLine > EndLine)
		{
		 size_t TempLine=StartLine;
		 StartLine=EndLine;
		 EndLine=TempLine;
		}
  	if (StartLine >= Files.size() || EndLine > Files.size())
		{
		 Error(ProcessName.c_str(),"The given LineRange (%lu,%lu) is out of list range",StartLine+1,EndLine+1);
		 return EXIT_FAILURE;
		}
	// Read in The Systematic List	
  	if(InputFileList.size()>0) 
		{
		 Info(ProcessName.c_str(),"InputFileList: %s",InputFileList.c_str());
		 if (StartLine!=EndLine) Info (ProcessName.c_str(), "Analyze file numbers from %lu to %lu (In Total: %lu files)",StartLine+1,EndLine+1,EndLine-StartLine);		
		 else Info(ProcessName.c_str(),"Analyze all files from list");
		}
	else if (InputFile.size() >0) 	 Info(ProcessName.c_str(),"InputFile: %s",InputFile.c_str());	
	Info(ProcessName.c_str(),"OutputDir: %s",outDir.c_str());
    Info(ProcessName.c_str(),"#########################################################################################################");
    
    if (xAOD::Init( ProcessName.c_str() ).isSuccess()==false) // Set up the job for xAOD access:
		{
		 Error(ProcessName.c_str(),"Could not setup xAOD");
		 return EXIT_FAILURE;    
		}	
	Analysis->SetOutFile (outDir, ProcessName, Form("%d",JobNumber));
	if(Files.size() > 1 && StartLine != EndLine)
		{
		 std::vector <std::string> TempList=Files;
		 Files.clear();
		 for (size_t l=StartLine;l < EndLine ; ++l) Files.push_back(TempList.at(l));
		}	
	if (Analysis->CheckInputFiles(Files)==false)return EXIT_FAILURE;	
	Analysis->PromptOptions();
	if(Analysis->Run()==false) return EXIT_FAILURE;
	Analysis->Terminate(); 
	return EXIT_SUCCESS;
	}
bool ConvertStrToBool(std::string Str)
	{
	 if(Str=="true")return true;
	 else return false;
	}
