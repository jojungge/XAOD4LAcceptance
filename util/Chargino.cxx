#include "CanvasPlot/GenericPlot.h"
#include <TGraphAsymmErrors.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TSystem.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TCanvas.h>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
int main(int argc, char **argv)
	{
 
 double x0=0.15;
 double y0=0.9;
 double yw=0.24;
 double xw=0.44;
 
 GenericPlot Plot;
 Plot.PrepareLegend("#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1} RPV-LLE grid points #lambda_{121}#neq0, #lambda_{122}#neq0",x0,y0,xw,yw);
 Plot.prepare_canvas("Request" ,800.,0. );
 Plot.p1->cd();
  
  // Plot: p8549_d24x1y1
  double Wino121X[] = { 769.2, 770.0, 770.0, 770.9, 771.8, 772.7, 773.5, 774.4, 774.3, 
    773.2, 772.2, 771.1, 770.0, 770.0, 769.0, 768.0, 767.6, 767.1, 765.8, 
    764.5, 763.2, 761.9, 760.6, 758.9, 755.4, 751.4, 750.0, 747.6, 743.8, 
    740.0, 735.6, 730.0, 727.9, 715.1, 710.0, 690.0, 670.0, 650.0, 632.0, 
    630.0, 610.0, 590.0, 570.0, 550.0, 530.0 };
 
  double Wino121Y[] = { 757.2, 733.3, 732.8, 708.2, 683.8, 659.2, 634.8, 610.2, 585.8, 
    561.2, 536.8, 512.2, 487.8, 486.6, 463.2, 438.8, 414.2, 389.8, 365.2, 
    340.8, 316.2, 291.8, 267.2, 242.8, 218.2, 193.8, 184.8, 169.2, 144.8, 
    120.2, 95.75, 77.67, 71.25, 46.75, 44.45, 36.8, 30.77, 25.9, 22.25, 
    21.98, 19.5, 16.75, 14.08, 12.06, 10.49 };
 
  int p8549_d24x1y1_numpoints = 45;  
  
  TGraph* Wino121Exclusion = new TGraph  (p8549_d24x1y1_numpoints,Wino121X, Wino121Y);
  Wino121Exclusion->SetName("/HepData/8549/d24x1y1");
  Wino121Exclusion->SetTitle("121");
  Wino121Exclusion->SetMaximum(1500);

  Wino121Exclusion->GetXaxis()->SetLimits(200,1500);
  Wino121Exclusion->GetXaxis()->SetTitle("m_{#tilde{#chi}^{#pm}_{1} }");
  Wino121Exclusion->GetYaxis()->SetTitle("m_{#tilde{#chi}^{0}_{1} }");
 
  Wino121Exclusion->Draw("APel");
 
  double NLSPPoints[] = {500,700,800,900,1000,1100,1200,1300};
  
  bool AddedNow=false;
  bool AddedLater=false;
  for (int g=0;g < 8;++g)
	{
	 std::vector<double> GridX (2,NLSPPoints[g]);	
	 std::vector<double> GridY;	
	 GridY.push_back(10);
	 GridY.push_back(50);
	 for (double l=200; l != NLSPPoints[g]; )
		{
		 GridY.push_back(l);
		 GridX.push_back(NLSPPoints[g]);
		 std::cout<<"Add: "<<NLSPPoints[g]<<"	"<<l<<std::endl;	 
		 if (l+200 < NLSPPoints[g]) l=l+200;
		 else if (l==NLSPPoints[g]-10) l=NLSPPoints[g];
		 else  l=NLSPPoints[g]-10;		
		}
	double Gridx [150];
	double Gridy [150];
	for(size_t a=0; a< GridX.size();++a) Gridx[a]=GridX.at(a);
	for(size_t a=0; a< GridY.size();++a) Gridy[a]=GridY.at(a);
	TGraph* NewGrid = new TGraph ( GridX.size(), Gridx,Gridy);
	if (NLSPPoints[g] < 1100)  
		{
		 NewGrid->SetMarkerStyle(20);	
		 if (AddedNow==false)
			{
			Plot.AddToLegend(NewGrid, "Produced");
			AddedNow=true;
			}
		}
	else
		{
		 NewGrid->SetMarkerStyle(21);
		 NewGrid->SetMarkerColor(kRed);
		  if (AddedLater==false)
			{
			Plot.AddToLegend(NewGrid, "Extension");
			AddedLater=true;
			}		 
		}	
	NewGrid->Draw("same,P");
	}
 Plot.SaveCanvas( "./Wino121",false,false);
 return EXIT_SUCCESS;
}
