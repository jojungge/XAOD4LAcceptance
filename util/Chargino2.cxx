#include "CanvasPlot/GenericPlot.h"
#include <TGraphAsymmErrors.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TSystem.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TCanvas.h>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
int main(int argc, char **argv)
	{
 
 double x0=0.15;
 double y0=0.9;
 double yw=0.24;
 double xw=0.44;
 
 GenericPlot Plot;
 Plot.PrepareLegend("#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1} RPV-LLE grid points #lambda_{133}#neq0, #lambda_{233}#neq0",x0,y0,xw,yw);
 Plot.prepare_canvas("Request" ,800.,0. );
 Plot.p1->cd();
  
  // Plot: p8549_d24x1y1
 double Wino133X[] = { 502.6, 507.9, 510.0, 516.7, 525.0, 530.0, 533.2, 541.5, 546.9, 
    546.6, 545.9, 545.2, 544.7, 544.3, 539.4, 530.0, 521.7, 510.0, 503.2, 
    490.0, 482.8, 470.0, 460.0, 450.0, 437.2, 430.0, 411.9, 410.0, 390.0, 
    370.0, 350.0, 330.0, 310.0, 298.0, 290.0, 270.0, 250.0, 230.0, 210.0 };   
 double Wino133Y[] = { 487.8, 463.2, 458.5, 438.8, 414.2, 399.3, 389.8, 365.2, 340.8, 
    316.2, 291.8, 267.2, 242.8, 218.2, 193.8, 180.7, 169.2, 153.2, 144.8, 
    128.2, 120.2, 106.4, 95.75, 84.83, 71.25, 63.98, 46.75, 46.27, 41.99, 
    35.4, 30.88, 27.34, 24.49, 22.25, 20.46, 15.9, 12.87, 10.77, 11.12 };
 
  int p8549_d24x1y1_numpoints = 39; 
  
  TGraph* Wino133Exclusion = new TGraph  (p8549_d24x1y1_numpoints,Wino133X, Wino133Y);
  Wino133Exclusion->SetName("/HepData/8549/d24x1y1");
  Wino133Exclusion->SetTitle("133");
  Wino133Exclusion->SetMaximum(1100);

  Wino133Exclusion->GetXaxis()->SetLimits(0,1100);
  Wino133Exclusion->GetXaxis()->SetTitle("m_{#tilde{#chi}^{#pm}_{1} }");
  Wino133Exclusion->GetYaxis()->SetTitle("m_{#tilde{#chi}^{0}_{1} }");
 
  Wino133Exclusion->Draw("APel");
 
  double NLSPPoints[] = {200,300,400,500,600,700,800,900};
  double LaterGridx [150];
  double LaterGridy [150];
  double Gridx [150];
  double Gridy [150];
	
 std::vector<double> LowGridX;	
 std::vector<double> LowGridY;
 std::vector<double> GridX;	
 std::vector<double> GridY;	
 for (int g=0;g < 8;++g)
	{
	 LowGridX.push_back(NLSPPoints[g]);
	 LowGridX.push_back(NLSPPoints[g]);
	 LowGridY.push_back(10);
	 LowGridY.push_back(50);
	 for (double l=100; l != NLSPPoints[g]; )
		{
		 if (NLSPPoints[g] < 900 && NLSPPoints[g] > 200)  
			{
			 GridY.push_back(l);
			 GridX.push_back(NLSPPoints[g]);
			}
		 else
			{
			 LowGridY.push_back(l);
			 LowGridX.push_back(NLSPPoints[g]);
			}
		 std::cout<<"Add: "<<NLSPPoints[g]<<"	"<<l<<std::endl;	 
		 if (l+100 < NLSPPoints[g]) l=l+100;
		 else if (l==NLSPPoints[g]-10) l=NLSPPoints[g];
		 else  l=NLSPPoints[g]-10;		
		}	
	}
 
  for(size_t a=0; a< LowGridX.size();++a) LaterGridx[a]=LowGridX.at(a);
  for(size_t a=0; a< LowGridY.size();++a) LaterGridy[a]=LowGridY.at(a);
  
  for(size_t a=0; a< GridX.size();++a) Gridx[a]=GridX.at(a);
  for(size_t a=0; a< GridY.size();++a) Gridy[a]=GridY.at(a);
	
  TGraph* NewGridLater = new TGraph ( LowGridX.size(), LaterGridx , LaterGridy);
  NewGridLater->SetMarkerStyle(21);
  NewGridLater->SetMarkerColor(kRed);
  NewGridLater->Draw("same,P");
  Plot.AddToLegend(NewGridLater, "Produced");
  TGraph* NewGrid = new TGraph ( GridX.size(), Gridx,Gridy);
  NewGrid->SetMarkerStyle(20);
  NewGrid->Draw("same,P");	
  Plot.AddToLegend(NewGrid, "Extension");  
  Plot.SaveCanvas( "./Wino133",false,false);
 return EXIT_SUCCESS;
}
