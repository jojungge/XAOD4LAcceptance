import os
import commands
import math
import sys
import re
import random
import subprocess

TestArea = os.getenv("ROOTCOREBIN")+"/../"
ROOTCOREDIR = os.getenv("ROOTCOREDIR")
ROOTCORECONFIG = os.getenv("ROOTCORECONFIG")
ROOTCOREOBJ = os.getenv("ROOTCOREOBJ")
ROOTCOREBIN = os.getenv("ROOTCOREBIN")


def BuildFileList (DS, File):
	Path="%s/MC15XAOD4LSelection/data/mc15_13TeV/SampleLists/Signal"%(TestArea)
	BPath="%s/MC15XAOD4LSelection/data/mc15_13TeV/BatchFileLists"%(TestArea)
	Time=commands.getoutput("date +'%Y-%m-%d_%H:%M:%S'")
	StatFile ="/tmp/Chk%s_%s"%(Time,DS)
	Ev=0
	#if os.path.exists ("%s/%s.txt"%(BPath,DS))==False:
		#os.system("python RucioListBuilder.py "+DS)
		#if os.path.exists("./%s.txt"%(DS) ) ==True:
			#os.system( "mv ./%s.txt %s/%s.txt"%(DS,BPath,DS) )
		#else:
			#os.system ( "echo %s >> MISSING.txt"%( DS[0 : DS.find(".")]+":"+DS) )
	#if os.path.exists ("%s/%s.txt"%(BPath,DS))==True:
		##~ os.system ("%s/bin/%s/CreateBatchJobSplit -I %s/%s.txt -O /tmp/EvP%s_%s -EpJ 10000000 -EvO %s"%(ROOTCOREBIN,ROOTCORECONFIG ,BPath,DS,Time,DS,StatFile) )
		##~ if os.path.exists(StatFile)==True:
			##~ S = open (StatFile,"r")
			##~ for L in S:
				##~ Ev= int(L)
		##~ os.system("echo \"%s	%d\" >> DSID.txt"%(DS,Ev) )
		#os.system("echo \"%s \" >> DSID.txt"%(DS) )
	if os.path.exists(Path) == False: 
		os.system("mkdir -p "+Path)
	if os.path.exists( "%s/%s"%(Path,File) ) == True:
		os.system("rm  %s/%s"%(Path,File) )
		
	print "Create File: "+File
	File = open("%s/%s"%(Path,File),'w')
	File.write("%s/\n"%(DS) )
	File.close()	

GMSBInputPath=TestArea+"MC15XAOD4LSelection/data/mc15_13TeV/SampleLists/GMSB.txt"
InFile = open ( GMSBInputPath, "r")

for D in InFile:
	AOD=D.replace("#" ,"").strip("/\n")
	DAOD=AOD.replace("AOD" ,"DAOD_SUSY2")+"_p2411"
	OutFile = D [ D.find("GMSB") : D.find(".merge") ].replace("Lambda","").replace("tanbeta" ,"")+".txt"
	BuildFileList(AOD , OutFile)
	BuildFileList(DAOD , "SUSY2_"+OutFile)	
	
	

