import os
import commands
import math
import sys
import re
import random
import subprocess

TestArea = os.getenv("ROOTCOREBIN")+"/../"
ROOTCOREDIR = os.getenv("ROOTCOREDIR")
ROOTCORECONFIG = os.getenv("ROOTCORECONFIG")
ROOTCOREOBJ = os.getenv("ROOTCOREOBJ")
ROOTCOREBIN = os.getenv("ROOTCOREBIN")

def BuildFileList (DS, File):
	Path="%s/MC15XAOD4LSelection/data/mc15_13TeV/SampleLists/Signal"%(TestArea)
	BPath="%s/MC15XAOD4LSelection/data/mc15_13TeV/BatchFileLists"%(TestArea)
	Time=commands.getoutput("date +'%Y-%m-%d_%H:%M:%S'")
	StatFile ="/tmp/Chk%s_%s"%(Time,DS)
	Ev=0
	if os.path.exists ("%s/%s.txt"%(BPath,DS))==False:
		#os.system("python RucioListBuilder.py "+DS)
		if os.path.exists("./%s.txt"%(DS) ) ==True:
			os.system( "mv ./%s.txt %s/%s.txt"%(DS,BPath,DS) )
		else:
			os.system ( "echo %s >> MISSING.txt"%( DS[0 : DS.find(".")]+":"+DS) )
	if os.path.exists ("%s/%s.txt"%(BPath,DS))==True:
		#~ os.system ("%s/bin/%s/CreateBatchJobSplit -I %s/%s.txt -O /tmp/EvP%s_%s -EpJ 10000000 -EvO %s"%(ROOTCOREBIN,ROOTCORECONFIG ,BPath,DS,Time,DS,StatFile) )
		#~ if os.path.exists(StatFile)==True:
			#~ S = open (StatFile,"r")
			#~ for L in S:
				#~ Ev= int(L)
		#~ os.system("echo \"%s	%d\" >> DSID.txt"%(DS,Ev) )
		os.system("echo \"%s \" >> DSID.txt"%(DS) )
	if os.path.exists(Path) == False: 
		os.system("mkdir -p "+Path)
	if os.path.exists( "%s/%s"%(Path,File) ) == True:
		os.system("rm  %s/%s"%(Path,File) )
		
	print "Create File: "+File
	File = open("%s/%s"%(Path,File),'w')
	File.write("%s/\n"%(DS) )
	File.close()	

def WriteFile (DSID=999999, NLSP=0,LSP=0, ProdMode="C1C1" , Couplings="LLE12k"):
	Path="%s/MC15XAOD4LSelection/data/mc15_13TeV/SampleLists"%(TestArea)
	BPath="%s/MC15XAOD4LSelection/data/mc15_13TeV/BatchFileLists"%(TestArea)
	File="%s_%i_%i_%s.txt"%(ProdMode,NLSP,LSP,Couplings)
	DFile="SUSY2_%s_%i_%i_%s.txt"%(ProdMode,NLSP,LSP,Couplings)
	
	DS = "mc15_13TeV.%i.MGPy8EG_A14N_%s_%i_%i_%s.merge.AOD.e4097_a766_a768_r6282"%(DSID,ProdMode,NLSP,LSP,Couplings)
	DAODS = "mc15_13TeV.%i.MGPy8EG_A14N_%s_%i_%i_%s.merge.DAOD_SUSY2.e4097_a766_a768_r6282_p2419"%(DSID,ProdMode,NLSP,LSP,Couplings)
	#BuildFileList(DS , File)
	BuildFileList(DAODS , DFile)
	
	
def WriteGrid (DSID=999999 , NLSP=[] , Spacing=100 , ProdMode="C1C1" , Couplings="121_122"):
	for n in range (0, len(NLSP)):	
		LSP = [10 ,50]	
		#This writes the column
		for l in range(Spacing , NLSP[n] + Spacing , Spacing):
			if l < NLSP[n] - 10:
				LSP.append(l)
			else:
				LSP.append(NLSP[n]-10)
				break	
		for l in range(0,len(LSP)):
			WriteFile(DSID=DSID , NLSP=NLSP[n],LSP=LSP[l],ProdMode=ProdMode,Couplings=Couplings)
			DSID= DSID + 1
	
DSID121 = 402200
DSID133 = 402275
NLSP121 = [500 , 700 , 800 , 900 , 1000 ] # , 1100 , 1200 , 1300]
NLSP133 = [200 , 300 , 400 , 500 , 600 , 700 , 800 ] #, 900 ]

WriteGrid(DSID=DSID121,NLSP=NLSP121 , Spacing=200 , Couplings="LLE12k")
WriteGrid(DSID=DSID133,NLSP=NLSP133 , Spacing=100 , Couplings="LLEi33")

