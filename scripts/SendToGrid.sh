#!/bin/bash

cd $ROOTCOREBIN/../
#~ rc compile
if [ $? -eq 1 ]; then
	exit
fi
cd $ROOTCOREBIN/../MC15XAOD4LSelection/scripts/
PreN=""

#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "Data" --InList "Data.txt" --Campaign "data15_13TeV" --AnalOpt "SigIso=true"
Backgrounds="ttLL-13TeV ZZ-13TeV Higgs ZZ-13TeV-4e ZZ-13TeV-4mu ZZ-13TeV-4tau ZZ-13TeV-2e2mu ZZ-13TeV-2mu2tau ZZ-13TeV-2e2tau ZZ-13TeV-2nu tttt Sherpa_VV Sherpa_VVV ttW-13TeV VH ttLLonShell-13TeV"
python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name  Test  --InList "ZZ-13TeV.txt" --AnalOpt "PileUp=false" --AnalOpt "GetTruth=false"  --AnalOpt "SigIso=true" --AnalOpt "CorrectIso=true" --AnalOpt "GetPhoton=true" --AnalOpt "Get2L=true" ${1}
exit
for B in ${Backgrounds};do
	python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}${B}" --InList "${B}.txt" --AnalOpt "PileUp=true" --AnalOpt "GetTruth=false"  --AnalOpt "SigIso=true" --AnalOpt "CorrectIso=true" --AnalOpt "GetPhoton=true" --AnalOpt "Get2L=true" ${1}
#	python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "SUSY2_${PreN}${B}" --InList "SUSY2_${B}.txt" --AnalOpt "PileUp=false" --AnalOpt "SigIso=true" --AnalOpt "GetPhoton=true" --AnalOpt "Get2L=true" --AnalOpt "GetW=true" --AnalOpt "GetZ=true"  ${1}
done
NLSP="500 700 800 900 1000 1100 1200 1300"
LSP="10 50 100 200 290 300 400 490 500 600 690 700 790 800 890 990 1000 1090 1190 1200 1290"
NLSP="800"
for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
		 #~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLE12k" --InList  "C1C1_${N}_${L}_LLE12k.txt" --AnalOpt "PileUp=false" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"  --AnalOpt "GetTau=false" ${1}
		 #~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLE12k-Iso" --InList  "C1C1_${N}_${L}_LLE12k.txt" --AnalOpt "PileUp=false" --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false"  --AnalOpt "GetTau=false"  --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
		 
		 python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLE12k" --InList  "C1C1_${N}_${L}_LLE12k.txt" --AnalOpt "CorrectIso=true"  --AnalOpt "PileUp=true" --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false"  --AnalOpt "GetTau=false"  --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
		 #~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLE12k-CIso" --InList  "C1C1_${N}_${L}_LLE12k.txt" --AnalOpt "PileUp=false" --AnalOpt "CorrectIso=true" --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "GetTau=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
		#~ 
		#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}SU-C1C1_${N}_${L}_LLE12k" --AnalOpt "SusyOR" --InList  "C1C1_${N}_${L}_LLE12k.txt" --CMS 13 --Lumi 5000 --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
		#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}SU-C1C1_${N}_${L}_LLE12k-Iso" --AnalOpt "SusyOR" --InList  "C1C1_${N}_${L}_LLE12k.txt" --CMS 13 --Lumi 5000  --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
		#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}SU-C1C1_${N}_${L}_LLE12k-CIso" --AnalOpt "SusyOR" --InList  "C1C1_${N}_${L}_LLE12k.txt" --CMS 13 --Lumi 5000  --AnalOpt "CorrectIso=true"  --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
	
	done
done
exit
NLSP="300 400 500 600 700 800 900 1000 1100 1200 1300"
LSP="100 200 290 300 400 490 500 590 600 690 700 790 800 890 900 990"

for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
		python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLEi33" --InList   "C1C1_${N}_${L}_LLEi33.txt" --AnalOpt "SigIso=true" --AnalOpt "CorrectIso=true" --AnalOpt "PileUp=false" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "fast"       ${1}
		#python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLEi33-Iso" --InList  "C1C1_${N}_${L}_LLEi33.txt" --AnalOpt "PileUp=false" --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"        ${1}
		#python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}C1C1_${N}_${L}_LLEi33-CIso" --InList  "C1C1_${N}_${L}_LLEi33.txt" --AnalOpt "PileUp=false" --AnalOpt "CorrectIso=true"  --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"        ${1}
		#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}SU-C1C1_${N}_${L}_LLEi33" --AnalOpt "SusyOR" --InList  "C1C1_${N}_${L}_LLEi33.txt" --CMS 13 --Lumi 5000 --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"       ${1}
		#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}SU-C1C1_${N}_${L}_LLEi33-Iso" --AnalOpt "SusyOR" --InList  "C1C1_${N}_${L}_LLEi33.txt" --CMS 13 --Lumi 5000  --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"        ${1}
		#~ python SubmitToGrid.py --Exec "XAOD-SUSY4LAnalysis" --Name "${PreN}SU-C1C1_${N}_${L}_LLEi33-Iso" --AnalOpt "SusyOR" --InList  "C1C1_${N}_${L}_LLEi33.txt" --CMS 13 --Lumi 5000   --AnalOpt "CorrectIso=true" --AnalOpt "SigIso=true" --AnalOpt "GetZ=false" --AnalOpt "GetW=false" --AnalOpt "GetTau=false" --AnalOpt "Get2L=false" --AnalOpt "SUSY" --AnalOpt "IsAtlfast"        ${1}
	done
done

rm $ROOTCOREBIN/../TarBall.tgz
