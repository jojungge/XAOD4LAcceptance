#!/bin/bash

cd ${ROOTCOREBIN}/../
#~ rc compile
if [ $? -eq 1 ]; then
	exit
fi
Date=`date --iso`
#OutDir=/ptmp/mpp/junggjo9/Cluster/Batch_tmp/${Date}/Test/
OutDir=${ROOTCOREBIN}/../
DS="mc15_13TeV.402211.MGPy8EG_A14N_C1C1_800_10_LLE12k.merge.AOD.e4097_a766_a768_r6282.txt"
XAOD-SUSY4LAnalysis -I ${ROOTCOREBIN}/../MC15XAOD4LSelection/data/mc15_13TeV/BatchFileLists/${DS} -N Test -O ${OutDir}   --GetTau false --GetZ false --GetW false --GetPhoton false --Get2L false --fast --GetReco true --DFOSVeto false --SUSY -Ib 1 -Ie 2
exit
mkdir -p ${OutDir}/Merge/
AllFiles=`ls ${OutDir}/*.root*`  
JobName="Test"
for F in ${AllFiles};do
	if [ ${F/"Trees"} == ${F} ]; then
		echo ${F} >> ${OutDir}/Merge/Umerged.txt
	fi
done
${ROOTCOREBIN}/../CanvasPlot/scripts/Merge.sh "${JobName}" "${OutDir}/Merge/Umerged.txt" "${OutDir}/Merge/" 1000
