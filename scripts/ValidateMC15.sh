#!/bin/bash
Date=$1
OutDir=/ptmp/mpp/${USER}/Cluster/Output/$1/
InDir=/ptmp/mpp/${USER}/Cluster/Output/$1/
#InDir=/ptmp/mpp/${USER}/Grid/Output/$1/
Lumi=1000
if [ $# -lt 1 ];then
	echo "Please give the analysis date"
	exit
fi
if [ $# -ge 2 ];then
	Lumi=$2
fi

LogPlots="Z-Children W-Children gauge_n mu_EtCone mu_PtCone mu_PtVarCone mu_TopoEtCone el_EtCone el_PtCone el_PtVarCone el_TopoEtCone mu_IsoEtCone mu_IsoPtCone mu_IsoPtVarCone mu_IsoTopoEtCone el_IsoEtCone el_IsoPtCone el_IsoPtVarCone el_IsoTopoEtCone"
Regions="tru_ tru_4l- tru_4lnoZ- tru_2l- tru_2lnoZ- rec_ rec_4l- rec_4lnoZ-"
Log=""

for L in ${LogPlots};do
	for R in ${Regions};do
		Log="${Log} --LogY ${R}${L} --LogY Int_${R}${L}"   
	done
done

NLSP="500 700 800 900 1000" # 1100 1200 1300"
NLSP="1000"
LSP="10 50 100 200 290 300 400 490 500 600 690 700 790 800 890 990 1000 1090 1190 1200 1290"
LSP="10"
Label=""
DrawTogether --NoRatio --ComDa --NoSumDa ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "LLE12k_C1C1" -T " "  -O ${OutDir} \
		-D ${InDir}/C1C1_500_400_LLE12k/C1C1_500_400_LLE12k.root -Dn "m_{#tilde{#chi}^{#pm}_{1}}=500 GeV, m_{#tilde{#chi}^{0}_{1}}=400 GeV" \
		-D ${InDir}/C1C1_800_400_LLE12k/C1C1_800_400_LLE12k.root -Dn "m_{#tilde{#chi}^{#pm}_{1}}=800 GeV, m_{#tilde{#chi}^{0}_{1}}=400 GeV" \
		-D ${InDir}/C1C1_1000_400_LLE12k/C1C1_1000_400_LLE12k.root -Dn "m_{#tilde{#chi}^{#pm}_{1}}=1000 GeV, m_{#tilde{#chi}^{0}_{1}}=400 GeV" \
		
		-D ${InDir}/C1C1_1000_400_LLE12k/C1C1_1000_400_LLE12k.root -Dn "m_{#tilde{#chi}^{#pm}_{1}}=1000 GeV, m_{#tilde{#chi}^{0}_{1}}=400 GeV" \
	
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_LLE12k_C1C1"

for N in ${NLSP};do
	for L in ${LSP};do
	if [ ${L} -ge 200 ];then
		break
	fi
	LeptonOverlap -I ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOS/C1C1_${N}_${L}_LLE12k-NDFOS.root -O ${OutDir}/C1C1_${N}_${L}_LLE12k-NDFOS/Histograms/ -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" 
	LeptonOverlap -I ${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root -O ${OutDir}/C1C1_${N}_${L}_LLE12k/Histograms/ -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" 
	
	DrawTogether --NoRatio --ComDa --NoSumDa ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "TRU_LLE12k_C1C1_${N}" -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, #lambda_{12k}#neq 0" -O ${OutDir} \
		-D ${InDir}/TRU_C1C1_${N}_10_LLE12k/TRU_C1C1_${N}_10_LLE12k.root -Dn "m_{#tilde{#chi}^{0}_{1}}=10 GeV" \
		-D ${InDir}/TRU_C1C1_${N}_50_LLE12k/TRU_C1C1_${N}_50_LLE12k.root -Dn "m_{#tilde{#chi}^{0}_{1}}=50 GeV" \
		-D ${InDir}/TRU_C1C1_${N}_600_LLE12k/TRU_C1C1_${N}_600_LLE12k.root -Dn "m_{#tilde{#chi}^{0}_{1}}=600 GeV" 
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_TRU_LLE12k_C1C1_${N}"

DrawTogether ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "DFOS_C1C1_${N}_${L}" -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" -O ${OutDir} \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NIso/C1C1_${N}_${L}_LLE12k-NIso.root -Vn "Optimized m_{SFOS} cut" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSNIso/C1C1_${N}_${L}_LLE12k-NDFOSNIso.root -Vn "Run I m_{SFOS} cut" \
		-D ${InDir}/C1C1_${N}_${L}_LLE12k-NSFOSNIso/C1C1_${N}_${L}_LLE12k-NSFOSNIso.root -Dn "No m_{SFOS} cut"

${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_DFOS_C1C1_${N}_${L}"

DrawTogether ${Label}  ${Log} -l ${Lumi} --ROOTFile --NoTH2 -N "Iso_C1C1_${N}_${L}" -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" -O ${OutDir} --RaTit "Rel. Eff." \
		-D ${InDir}/C1C1_${N}_${L}_LLE12k-NIso/C1C1_${N}_${L}_LLE12k-NIso.root 	-Dn "No isolation requirement" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-Iso/C1C1_${N}_${L}_LLE12k-Iso.root	-Vn "Uncorrected isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root 			-Vn "Corrected isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-RIso/C1C1_${N}_${L}_LLE12k-RIso.root 	-Vn "Recalculated isolation" 
			
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Iso_C1C1_${N}_${L}"
rm -rf ${OutDir}/DrawTogether_Iso_C1C1_${N}_${L}/TruthLevel
rm -rf ${OutDir}/DrawTogether_Iso_C1C1_${N}_${L}/RecoLevel/2l/
continue
DrawTogether ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "Iso_C1C1_${N}_${L}-NDFOS" -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" -O ${OutDir} --RaTit "Rel. Eff." \
		-D ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOS/C1C1_${N}_${L}_LLE12k-NDFOS.root 	-Dn "No isolation requirement" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSIso/C1C1_${N}_${L}_LLE12k-NDFOSIso.root	-Vn "Standard isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSCIso/C1C1_${N}_${L}_LLE12k-NDFOSCIso.root 			-Vn "Corrected Isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSRIso/C1C1_${N}_${L}_LLE12k-NDFOSRIso.root 	-Vn "Recalculated Isolation" 

${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Iso_C1C1_${N}_${L}-NDFOS"
rm -rf ${OutDir}/DrawTogether_Iso_C1C1_${N}_${L}-NDFOS/TruthLevel
rm -rf ${OutDir}/DrawTogether_Iso_C1C1_${N}_${L}-NDFOS/RecoLevel/2l/

DrawTogether ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "Iso1_C1C1_${N}_${L}" -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" -O ${OutDir} --RaTit "Rel. Eff." \
		-D ${InDir}/C1C1_${N}_${L}_LLE12k-NIso/C1C1_${N}_${L}_LLE12k-NIso.root 	-Dn "Default isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root 			-Vn "Corrected isolation" \
		
		#~ -V ${InDir}/C1C1_${N}_${L}_LLE12k-RIso/C1C1_${N}_${L}_LLE12k-RIso.root 	-Vn "Recalculated isolation" 

${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Iso1_C1C1_${N}_${L}"
rm -rf ${OutDir}/DrawTogether_Iso1_C1C1_${N}_${L}/TruthLevel
rm -rf ${OutDir}/DrawTogether_Iso1_C1C1_${N}_${L}/RecoLevel/2l/

DrawTogether ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "Iso1_C1C1_${N}_${L}-NDFOS" -T "m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, m_{#tilde{#chi}^{0}_{1}}=${L} GeV, #lambda_{12k}#neq 0" -O ${OutDir} --RaTit "Rel. Eff." \
		-D ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSIso/C1C1_${N}_${L}_LLE12k-NDFOSIso.root 	-Dn "Uncorrected Isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSCIso/C1C1_${N}_${L}_LLE12k-NDFOSCIso.root -Vn "Corrected Isolation" \
		-V ${InDir}/C1C1_${N}_${L}_LLE12k-NDFOSRIso/C1C1_${N}_${L}_LLE12k--NDFOSRIso.root 	-Vn "Recalculated Isolation" 
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Iso1_C1C1_${N}_${L}"
rm -rf ${OutDir}/DrawTogether_Iso1_C1C1_${N}_${L}-NDFOS/TruthLevel
rm -rf ${OutDir}/DrawTogether_Iso1_C1C1_${N}_${L}-NDFOS/RecoLevel/2l/
	done
done	
		
exit



for N in ${NLSP};do
	ColSum=""
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
		if [ ! -f "${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root" ]; then
			continue
		fi
		Legend="m_{#tilde{#chi}^{0}_{1}}=${L}GeV"
		ColSum="${ColSum} -D ${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root -Dn ${Legend}"
		DrawTogether --ComDa --NoSumDa ${Label}  ${Log} -l ${Lumi} --NoTH2 -N "LLE12k_C1C1_${N}_${L}" -T "#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1} Isolation Correction m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV m_{#tilde{#chi}^{0}_{1}}= ${L} GeV #lambda_{12k}#neq 0" -O ${OutDir} \
			-D ${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root -Dn "W.o. Iso" \
			-V ${InDir}/C1C1_${N}_${L}_LLE12k-Iso/C1C1_${N}_${L}_LLE12k-Iso.root -Vn "W. Iso" \
			-V ${InDir}/C1C1_${N}_${L}_LLE12k-CIso/C1C1_${N}_${L}_LLE12k-CIso.root -Vn "W. Iso (Full cor.)" \
			-V ${InDir}/C1C1_${N}_${L}_LLE12k-RIso/C1C1_${N}_${L}_LLE12k-RIso.root -Vn "W. Iso (Trk rec.)" \
			
			-V ${InDir}/C1C1_${N}_${L}_LLE12k-TIso/C1C1_${N}_${L}_LLE12k-TIso.root -Vn "W. Iso (Track cor.)" \
			
			
			#-V ${InDir}/SU-C1C1_${N}_${L}_LLE12k/SU-C1C1_${N}_${L}_LLE12k.root -Vn "ST" \
			-V ${InDir}/SU-C1C1_${N}_${L}_LLE12k-Iso/SU-C1C1_${N}_${L}_LLE12k-Iso.root -Vn "ST (Iso)" \
			-V ${InDir}/SU-C1C1_${N}_${L}_LLE12k-CIso/SU-C1C1_${N}_${L}_LLE12k-CIso.root -Vn "ST (CIso)" 
		${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_LLE12k_C1C1_${N}_${L}"
	
	done
	#DrawTogether ${ColSum} ${Label}  --NoSumDa --ComDa --NoRatio  ${Log} -l ${Lumi} --NoTH2 -N "LLE12k_C1C1_${N}" -T "#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1} production, m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV, #lambda_{12k}#neq 0" -O ${OutDir} 
	#${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_LLE12k_C1C1_${N}"	
done
exit
LSP="100 200 300 400 500 600 790"
for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi		
		if [ ! -f "${InDir}/C1C1_${N}_${L}_LLEi33/C1C1_${N}_${L}_LLEi33.root" ]; then
			continue
		fi
	Legend="m_{#tilde{#chi}^{0}_{1}}=${L}\ GeV"
	ColSum="${ColSum} -D ${InDir}/C1C1_${N}_${L}_LLE12k/C1C1_${N}_${L}_LLE12k.root -Dn '${Legend}'"
		
	DrawTogether --NoSumDa ${Label} --ComDa ${Log} -l ${Lumi} --NoTH2 -N "LLEi33_C1C1_${N}_${L}" -T "#tilde{#chi}^{+}_{1}#tilde{#chi}^{-}_{1} Isolation Correction m_{#tilde{#chi}^{#pm}_{1}}=${N} GeV m_{#tilde{#chi}^{0}_{1}}= ${L} GeV #lambda_{i33}#neq 0" -O ${OutDir} \
		-D ${InDir}/C1C1_${N}_${L}_LLEi33/C1C1_${N}_${L}_LLEi33.root -Dn "Without Iso" \
		-V ${InDir}/C1C1_${N}_${L}_LLEi33-Iso/C1C1_${N}_${L}_LLEi33-Iso.root -Vn "With Iso (Corrected)" \
		-V ${InDir}/C1C1_${N}_${L}_LLEi33-CIso/C1C1_${N}_${L}_LLEi33-CIso.root -Vn "With Iso" 
	
		#-V ${InDir}/SU-C1C1_${N}_${L}_LLEi33/SU-C1C1_${N}_${L}_LLEi33.root -Vn "ST" \
		-V ${InDir}/SU-C1C1_${N}_${L}_LLEi33-Iso/SU-C1C1_${N}_${L}_LLEi33-Iso.root -Vn "ST (Iso)" \
		-V ${InDir}/SU-C1C1_${N}_${L}_LLEi33-CIso/SU-C1C1_${N}_${L}_LLEi33-CIso.root -Vn "ST (CIso)" 
		${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_LLEi33_C1C1_${N}_${L}"
	done
done
 
exit
