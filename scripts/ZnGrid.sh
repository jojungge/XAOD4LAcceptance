#!/bin/bash

# user-defined parameters
RCArea=${ROOTCOREBIN}"/../"    		#RC folder
WORKDIR=${ROOTCOREBIN}"/bin/"${ROOTCORECONFIG}"/"              #run folder
# some initial output
echo "###############################################################################################"
echo "					 Enviroment variables"
echo "###############################################################################################"
echo "USER"=${USER}
echo "HOME-AREA="${HOME}
echo "X509_USER_PROXY="${X509_USER_PROXY}
echo "ROOTCOREDIR="${ROOTCOREDIR}
echo "ROOTCORECONFIG="${ROOTCORECONFIG}
echo "ROOTCOREOBJ="${ROOTCOREOBJ}
echo "ROOTCOREBIN="${ROOTCOREBIN}
echo "ROOTVER"=${ROOTVER}
echo "RC-Area="${RCArea}
echo "WORKDIR="${WORKDIR}
echo "RUCIO_ACCOUNT"=${RUCIO_ACCOUNT}
echo "EMI_OVERRIDE_JAVA_HOME"=${EMI_OVERRIDE_JAVA_HOME}
echo "HostName="${HOSTNAME}
echo "###############################################################################################"
echo "					Setting up the enviroment"
echo "###############################################################################################"
echo "cd /tmp"
cd /tmp
if [ -z  ${ATLAS_LOCAL_ROOT_BASE} ]; then
 
	echo "source /t2/sw/setupwn_cvmfs.sh"
	source /t2/sw/setupwn_cvmfs.sh
	echo "Setting Up the ATLAS Enviroment:"
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
	
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm" 
	source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh --rootVersion ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm
	echo "Setup ROOTCORE:"
	echo "export PATH=${WORKDIR}/:$PATH"
	export PATH=${WORKDIR}/:$PATH
	echo "export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${RCcfg}/:${LD_LIBRARY_PATH}"
	export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${ROOTCORECONFIG}:${LD_LIBRARY_PATH}
else
	if [ $#  -lt 1 ];then
		exit
	fi
	Date=${1}
	Lumi=${2}
	OutDir="${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/"
	Name="SM_BG"
	Options="LLE12k"
fi
cd ${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/
if [ -d  ${OutDir}/ZnGrid/ ];then
	echo "rm -rf ${OutDir}/ZnGrid/"
	rm -rf ${OutDir}/ZnGrid/
fi
if [ -d  ${OutDir}/ZnGrid/ ];then
	echo "rm -rf ${OutDir}/ZnGridNDFOS/"
	rm -rf ${OutDir}/ZnGridNDFOS/
fi
Label=""
Meff=""
Met=""
for M in $(seq 0 100 1000); do
		Meff="${Meff},${M}"
done
for E in $(seq 0 25 75);do
		Met="${Met},${E}"
done

python Zn_C1C1.py ${Label} --lumi 01 --meff ${Meff} --met ${Met} --OutDir "${OutDir}/ZnGrid_${Options}/One"  --Date ${Date} --Lambda ${Options}
python Zn_C1C1.py ${Label} --lumi 03 --meff ${Meff} --met ${Met} --OutDir "${OutDir}/ZnGrid_${Options}/Three"  --Date ${Date} --Lambda ${Options}
python Zn_C1C1.py ${Label} --lumi 05 --meff ${Meff} --met ${Met} --OutDir "${OutDir}/ZnGrid_${Options}/Five"  --Date ${Date} --Lambda ${Options}
python Zn_C1C1.py ${Label} --lumi 10 --meff ${Meff} --met ${Met} --OutDir "${OutDir}/ZnGrid_${Options}/Ten" --Date ${Date} --Lambda ${Options}
python Zn_C1C1.py ${Label} --lumi 15 --meff ${Meff} --met ${Met} --OutDir "${OutDir}/ZnGrid_${Options}/Fifteen" --Date ${Date} --Lambda ${Options}
python Zn_C1C1.py ${Label} --lumi 20 --meff ${Meff} --met ${Met} --OutDir "${OutDir}/ZnGrid_${Options}/Twenty" --Date ${Date} --Lambda ${Options}
./ValidateMC15.sh ${Date}
