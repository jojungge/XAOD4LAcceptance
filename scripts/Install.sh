#!/bin/bash

if [ -z ${ROOTCOREBIN} ]; then
	echo "Please Setup an ASG RootCore  Base Release >=2.3.21"
	exit
fi

cd ${ROOTCOREBIN}/../
svn co svn+ssh://svn.cern.ch/reps/atlas-jojungge/XAOD4LAcceptance/trunk XAOD4LAcceptance
svn co ${SVNINST}/Institutes/MPI/MDT/analysis/Run2FourLeptons/MC15XAOD4LSelection/trunk MC15XAOD4LSelection
source ${ROOTCOREBIN}/../MC15XAOD4LSelection/scripts/Install.sh


