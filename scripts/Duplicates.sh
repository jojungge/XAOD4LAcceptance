#!/bin/bash

# user-defined parameters
RCArea=${ROOTCOREBIN}"/../"    		#RC folder
WORKDIR=${ROOTCOREBIN}"/bin/"${ROOTCORECONFIG}"/"              #run folder
# some initial output
echo "###############################################################################################"
echo "					 Enviroment variables"
echo "###############################################################################################"
echo "USER"=${USER}
echo "HOME-AREA="${HOME}
echo "X509_USER_PROXY="${X509_USER_PROXY}
echo "ROOTCOREDIR="${ROOTCOREDIR}
echo "ROOTCORECONFIG="${ROOTCORECONFIG}
echo "ROOTCOREOBJ="${ROOTCOREOBJ}
echo "ROOTCOREBIN="${ROOTCOREBIN}
echo "ROOTVER"=${ROOTVER}
echo "RC-Area="${RCArea}
echo "WORKDIR="${WORKDIR}
echo "RUCIO_ACCOUNT"=${RUCIO_ACCOUNT}
echo "EMI_OVERRIDE_JAVA_HOME"=${EMI_OVERRIDE_JAVA_HOME}
echo "HostName="${HOSTNAME}
echo "###############################################################################################"
echo "					Setting up the enviroment"
echo "###############################################################################################"
echo "cd /tmp"
cd /tmp
if [ -z  ${ATLAS_LOCAL_ROOT_BASE} ]; then
 
	echo "source /t2/sw/setupwn_cvmfs.sh"
	source /t2/sw/setupwn_cvmfs.sh
	echo "Setting Up the ATLAS Enviroment:"
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
	
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm" 
	source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh --rootVersion ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm
	echo "Setup ROOTCORE:"
	echo "export PATH=${WORKDIR}/:$PATH"
	export PATH=${WORKDIR}/:$PATH
	echo "export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${RCcfg}/:${LD_LIBRARY_PATH}"
	export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${ROOTCORECONFIG}:${LD_LIBRARY_PATH}
else
	if [ $#  -lt 1 ];then
		exit
	fi
	Date=${1}
	Lumi=${2}
	OutDir="${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/"
	Name="SM_BG"
	Options=""
fi
cd ${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/
echo "python DuplicateEvents.py ${InDir}/${Options} ${OutDir}/${Options}/${Options}.txt"
python DuplicateEvents.py "${InDir}/${Options}" "${OutDir}/${Options}/${Options}.txt"
