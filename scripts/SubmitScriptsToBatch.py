import os
import commands
import math
import sys
import re
#Setting up basic variables
username = os.getenv("USER")
ROOTCOREBIN = os.getenv("ROOTCOREBIN")
ROOTCOREDIR = os.getenv("ROOTCOREDIR")
ROOTCORECONFIG = os.getenv("ROOTCORECONFIG")
ROOTCOREOBJ = os.getenv("ROOTCOREOBJ")
JAVA_HOME = os.getenv ("EMI_OVERRIDE_JAVA_HOME")
CALIBPATH = os.getenv("CALIBPATH")
Rucio = os.getenv("RUCIO_ACCOUNT")
ROOTSYS= os.getenv("ROOTSYS").replace("%s/root/"%os.getenv("ATLAS_LOCAL_ROOT"),"")
ROOTVer = ROOTSYS.replace("-"+ROOTCORECONFIG,"")
MyEmail=""
if os.getenv("JOB_MAIL") is None:
	MyEmail = username+"@mpp.mpg.de" #where to send job status emails
else:
	MyEmail =os.getenv("JOB_MAIL")


DatasetList=""
EnviromentVariables=""
JName = ""
CMS = 13
Lumi = 1000.
LumiData = 165.
RunTime="01:59:59"
BuildJob = False
HoldJob=[]
LogPlots = []
LoadSys = ""
Options=""
BaseFolder="/ptmp/mpp/%s/Cluster"%(username)  
Today=commands.getoutput("date --iso")
Time=commands.getoutput("date +'%Y-%m-%d_%H:%M:%S'")
for a in range (1 , len(sys.argv) ):
	if "--Name" in sys.argv[a] and a < len(sys.argv):
		JName=sys.argv[a+1]
	elif "--CreateBuildJob" in sys.argv[a]:		
		BuildJob=True	
	elif "--RunTime" in sys.argv[a] and a < len(sys.argv):
		RunTime=sys.argv[a+1]	
	elif "--Exec" in sys.argv[a] and a < len(sys.argv):
		Executable=sys.argv[a+1]
	elif "--Lumi" in sys.argv[a] and a < len(sys.argv):
		Lumi = float (sys.argv[a+1])
	elif "--DataLumi" in sys.argv[a] and a < len(sys.argv):
		LumiData = float (sys.argv[a+1])
	elif "--Log" in sys.argv[a] and a < len(sys.argv):
		LogPlots.append (sys.argv[a+1])
	elif "--Options" in sys.argv[a] and a < len(sys.argv):
		Options = "%s %s"%(sys.argv[a+1],Options)
	elif "--Mail" in sys.argv[a] and a < len(sys.argv):
		MyEMail=sys.argv[a+1]
	elif "--HoldJob" in sys.argv[a] and a < len(sys.argv):
		HoldJob.append(sys.argv[a+1])
if len(JName)==0:
	print "Please provide a JobName"
	exit(1)

Options=Options.rstrip()
JobDir ="%s/%s"%(Today,JName)
MyBatchLogs = "%s/Batch_job_logs/%s/"%(BaseFolder,JobDir) 
MyOutDir = "%s/Output/%s"%(BaseFolder,Today)    
MyTmpDir = "%s/Batch_tmp/%s/"%(BaseFolder,Today)
ReleaseCopy = BaseFolder+"/BuildJob/"+JobDir

EnviromentVariables = " -v ROOTVER=%s -v ROOTCOREBIN=%s -v ROOTCOREDIR=%s -v ROOTCORECONFIG=%s -v CALIBPATH=%s -v  ROOTCOREOBJ=%s -v Date=%s"%(ROOTVer , ROOTCOREBIN , ROOTCOREDIR , ROOTCORECONFIG , CALIBPATH , ROOTCOREOBJ , Today  )
LogY = ""
for L in LogPlots:
	LogY=LogY+" --LogY "+L

EnviromentVariables = EnviromentVariables+" -v Lumi="+str(Lumi)+" -v Options='"+Options+"' -v Log='"+LogY+"' -v DataLumi="+str(LumiData)

if os.getenv ("EMI_OVERRIDE_JAVA_HOME")!=None:
	EnviromentVariables = "%s -v EMI_OVERRIDE_JAVA_HOME=%s"%(EnviromentVariables,JAVA_HOME)
#Without Reco information the Job runs faster -> More Files per Jobs can be assigned
os.system("if [[ -d %s ]]; then rm -rf %s; fi"%(MyBatchLogs,MyBatchLogs))    
os.system(" mkdir -p "+MyOutDir)
os.system(" mkdir -p "+MyTmpDir)
os.system(" mkdir -p "+MyBatchLogs)
	
 # this is the actual qsub for 'normal operation'
vmem = 4000

print "JobName:		"+JName
print "TempDir:		"+MyTmpDir
print "OutputDir:		"+MyOutDir
print "Lumi:			%d"%(Lumi)
print "Memory:			"+str(vmem)+" MB"
print "Additional Options:	"+Options
print "Maximal RunTime:	"+RunTime

for J in HoldJob:
	EnviromentVariables=EnviromentVariables+" -hold_jid "+J

if BuildJob == True:
	print "Will first submit a BuildJob"
	KeyWord = "Analysis"
	KeyWord1 = "RootCore"
	RCRelease = ROOTCOREDIR [  ROOTCOREDIR.find(KeyWord) + len(KeyWord) : ROOTCOREDIR.find(KeyWord1) -1 ]
	RCRelease = RCRelease.replace("/" , " ")
	BuildCom="qsub -o "+str(MyBatchLogs)+" -j y -l h_vmem=1500M  -m e -M "+MyEmail+" -l h_rt=01:00:00 -l h_core=0 -N Build_"+JName+" -v RCArea='"+ROOTCOREBIN+"/../' -v Name="+JName+" -v COPYAREA='"+ReleaseCopy+"' -v RCRelease='"+RCRelease+"' -cwd ./Batch_Build.sh"
	os.system(BuildCom)
	EnviromentVariables=EnviromentVariables.replace( ROOTCOREBIN , ReleaseCopy+"/RootCoreBin/")	

EnviromentVariables=EnviromentVariables+" -v Name="+JName+" -v OutDir='"+MyOutDir +"' -v InDir='"+MyTmpDir+"' "

SubCom="qsub -o "+str(MyBatchLogs)+" -j y -l h_vmem="+str(vmem)+"M -l h_rt="+RunTime+" -l h_core=0 -hold_jid 'Build_"+JName+"' -N '"+JName+"' -M '"+MyEmail+"' "+EnviromentVariables+" -cwd "+Executable
print "Sending Batch Job"
os.system(SubCom)

if BuildJob == True:
	print "Finally Send the Clean Up job"
	CleanCom="qsub -o "+str(MyBatchLogs)+" -j y -l h_vmem=200M  -l h_rt=00:30:00 -l h_core=0 -N Clean_"+JName+" -hold_jid "+JName+" -hold_jid Build_"+JName+" -v COPYAREA='"+ReleaseCopy+"' -v Name="+JName+" -cwd ./Batch_Clean.sh"
	os.system(CleanCom)
