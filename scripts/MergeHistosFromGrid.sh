#!/bin/bash
Date=$1
OutDir=$Pgrid/Output/$1/
InDir=$Pgrid/Data/
Lumi=1000
LumiData="165.33"
Analysis="MC15xAOD4LSelection"
if [ $# -lt 1 ];then
	echo "Please give the analysis date"
	exit
fi
if [ $# -ge 2 ];then
	Lumi=$2
fi
#~ if [ -d ${OutDir} ];then
	 #~ echo "rm -rf ${OutDir}"
	 #~ rm -rf ${OutDir}
 #~ fi

BGDir=$Pgrid/Data/user.${RUCIO_ACCOUNT}.${Date}.${Analysis}.SM-BG_SM-BG
#~ if [ -d ${BGDir} ];then
	#~ echo "rm -rf ${BGDir}"
	#~ rm -rf ${BGDir}
#~ fi

SimultanousMerge="ttLLonShell-13TeV ZZ-13TeV-4e  ZZ-13TeV-4mu ZZ-13TeV-4tau ZZ-13TeV-2e2mu  ZZ-13TeV-2e2tau ZZ-13TeV-2mu2tau ZZ-13TeV-2nu "

for s in ${SimultanousMerge};do
	${ROOTCOREBIN}/../CanvasPlot/scripts/GridMerge.sh ${s} ${Date} ${Analysis} ${Lumi}  
done

mkdir -p ${BGDir}
BGList="VH2  ttLL-13TeV tttt Higgs ttW-13TeV ttWW-13TeV Sherpa_VVV  ZZ-13TeV" 
#~ BGList=" VH2 ttLL-13TeV tttt Higgs ttW-13TeV Sherpa_VVV Sherpa_VV"
for a in ${BGList};do
	  cd $Pgrid/Data/
	  ${ROOTCOREBIN}/../CanvasPlot/scripts/GridMerge.sh "${a}" ${Date} ${Analysis} ${Lumi} 2>${OutDir}/${a}.txt
	  Files=`ls -d ${Pgrid}/Data/user.${RUCIO_ACCOUNT}.${Date}.${Analysis}.${a}_${a}*`
	  for b in ${Files};do
		cp ${b}/*root* ${BGDir}
	  echo "Copy ${b} to ${BGDir}"
	  done		
done

${ROOTCOREBIN}/../CanvasPlot/scripts/GridMerge.sh "SM-BG" ${Date} ${Analysis} ${Lumi} 2>${OutDir}/SM.txt


${ROOTCOREBIN}/../CanvasPlot/scripts/GridMerge.sh Sherpa_VV ${Date} ${Analysis} ${Lumi}

#${ROOTCOREBIN}/../CanvasPlot/scripts/GridMerge.sh Data ${Date} ${Analysis} ${LumiData}




 

LogPlots="SR_ev gauge_n Meff Met Meff-Jet Meff-Lep"
Regions="rec_ rec_4l- rec_4lnoZ- rec_4lZ-"
Log=""

for L in ${LogPlots};do
	for R in ${Regions};do
		Log="${Log} --LogY ${R}${L} --LogY Int_${R}${L}"   
	done
done

DrawTogether --Normalize -l ${Lumi} --NoTH2 -N "ttZ" -T "ttZ #sqrt{s}=13 TeV" -O ${OutDir} ${Log} \
	-D ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -Dn "MC15" \
	-V ${OutDir}/ttLLonShell-13TeV/ttLLonShell-13TeV.root -Vn "DC14"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_ttZ/"

DrawTogether --Normalize -l ${Lumi} --NoTH2 -N "VV" -T "DiBoson" -O ${OutDir}  ${Log} \
	-V ${OutDir}/ZZ-13TeV/ZZ-13TeV.root -Vn "Powerheg" \
	-D ${OutDir}/Sherpa_VV/Sherpa_VV.root -Dn "Sherpa"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_VV/"

DrawTogether ${Log} --SumBg --Stack --Normalize -l ${Lumi} --NoTH2 -N "ZZ" -T "Powerheg MC15 vs. MC12" -O ${OutDir}   \
	-D ${OutDir}/ZZ-13TeV/ZZ-13TeV.root -Dn "MC15" \
	-V ${OutDir}/ZZ-13TeV-4e/ZZ-13TeV-4e.root -Vn "ZZ->4e" \
	-V ${OutDir}/ZZ-13TeV-4mu/ZZ-13TeV-4mu.root -Vn "ZZ->4#mu" \
	-V ${OutDir}/ZZ-13TeV-4tau/ZZ-13TeV-4tau.root -Vn "ZZ->4#tau" \
	-V ${OutDir}/ZZ-13TeV-2e2mu/ZZ-13TeV-2e2mu.root -Vn "ZZ->2e2#mu" \
	-V ${OutDir}/ZZ-13TeV-2e2tau/ZZ-13TeV-2e2tau.root -Vn "ZZ->2e2#tau" \
	-V ${OutDir}/ZZ-13TeV-2mu2tau/ZZ-13TeV-2mu2tau.root -Vn "ZZ->2#mu2#tau" \
	-V ${OutDir}/ZZ-13TeV-2nu/ZZ-13TeV-2nu.root -Vn "ZZ->#nuX"	
	
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_ZZ/"

DrawTogether  -l ${Lumi} --Normalize --NoTH2 -l ${LumiData} -N "Data-BG" -T "" -O ${OutDir} --RaTit "Data / SM" \
	-D ${OutDir}/SM-BG/SM-BG.root -Dn "SM MonteCarlo BG" \
	-V ${OutDir}/Data/Data.root -Vn "Data" 
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Data-BG/"
	
DrawTogether --NoCenRatio  --NoSumDa --Stack -l ${Lumi} --Normalize ${Log} --NoTH2  -N "SM-BG" -T "Background composition" -O ${OutDir} --RaTit "Contribution" \
	-D ${OutDir}/SM-BG/SM-BG.root -Dn "Total" \
	-V ${OutDir}/Sherpa_VVV/Sherpa_VVV.root -Vn "VVV" \
	-V ${OutDir}/ZZ-13TeV/ZZ-13TeV.root -Vn "ZZ" \
	-V ${OutDir}/VH/VH.root -Vn "VH" \
	-V ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -Vn "t#bar{t}Z" \
	-V ${OutDir}/Higgs/Higgs.root -Vn "Higgs" \
	-V ${OutDir}/tttt/tttt.root -Vn "tttt" \
	-V ${OutDir}/ttW-13TeV/ttW-13TeV.root -Vn "t#bar{t}W(W)" \
	
	-V ${OutDir}/Sherpa_VV/Sherpa_VV.root -Vn "VV" \
	

${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_SM-BG/"

exit
NLSP="500 700 800 900 1000" # 1100 1200 1300"
LSP="10 50 100 200 400 490 600 690 700 790 800 990 1000 1090 1190 1200 1290"
for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "C1C1_${N}_${L}_LLE12k" ${Date} ${Analysis} ${Lumi} &
	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "C1C1_${N}_${L}_LLE12k-Iso" ${Date} ${Analysis} ${Lumi} &
#	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "SU-C1C1_${N}_${L}_LLE12k" ${Date} ${Analysis} ${Lumi}
#	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "SU-C1C1_${N}_${L}_LLE12k-Iso" ${Date} ${Analysis} ${Lumi}
	done	
done

NLSP="300 400 500 600 700 800"  # 900 1000 1100 1200 1300"
LSP="100 200 290 300 400 490 500 590 600 690 700 790 800 890 900 990"

for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "C1C1_${N}_${L}_LLEi33" ${Date} ${Analysis} ${Lumi}
	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "C1C1_${N}_${L}_LLEi33-Iso" ${Date} ${Analysis} ${Lumi}
#	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "SU-C1C1_${N}_${L}_LLEi33" ${Date} ${Analysis} ${Lumi}
#	$ROOTCOREBIN/../CanvasPlot/scripts/GridMerge.sh "SU-C1C1_${N}_${L}_LLEi33-Iso" ${Date} ${Analysis} ${Lumi}
	done	
done

