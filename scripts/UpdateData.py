import os
import commands
import math
import sys
import re
import random
import subprocess

TestArea = os.getenv("ROOTCOREBIN")+"/../"
ROOTCOREDIR = os.getenv("ROOTCOREDIR")
ROOTCORECONFIG = os.getenv("ROOTCORECONFIG")
ROOTCOREOBJ = os.getenv("ROOTCOREOBJ")
ROOTCOREBIN = os.getenv("ROOTCOREBIN")


def BuildFileList (DS, File=""):
	Path="%s/MC15XAOD4LSelection/data/data15_13TeV/SampleLists"%(TestArea)
	BPath="%s/MC15XAOD4LSelection/data/data15_13TeV/BatchFileLists"%(TestArea)
	ScriptPath ="%s/MC15XAOD4LSelection/scripts/"%(TestArea)
	Time=commands.getoutput("date +'%Y-%m-%d_%H:%M:%S'")
	StatFile ="/tmp/Chk%s_%s"%(Time,DS)
	Ev=0
	if os.path.exists ("%s/%s.txt"%(BPath,DS))==False:
		os.system("python "+ScriptPath+"RucioListBuilder.py "+DS)
		if os.path.exists("./%s.txt"%(DS) ) ==True:
			os.system( "mv ./%s.txt %s/%s.txt"%(DS,BPath,DS) )
		else:
			os.system ( "echo %s >> MISSING.txt"%( DS[0 : DS.find(".")]+":"+DS) )
#	if os.path.exists ("%s/%s.txt"%(BPath,DS))==True:
#		 os.system ("%s/bin/%s/CreateBatchJobSplit -I %s/%s.txt -O /tmp/EvP%s_%s -EpJ 10000000 -EvO %s"%(ROOTCOREBIN,ROOTCORECONFIG ,BPath,DS,Time,DS,StatFile) )
#		 if os.path.exists(StatFile)==True:
#			S = open (StatFile,"r")
#			for L in S:
#				Ev= int(L)
#		 os.system("echo \"%s	%d\" >> DSID.txt"%(DS,Ev) )
	if os.path.exists(Path) == False: 
		os.system("mkdir -p "+Path)
	if len(File) > 0:
		if os.path.exists( "%s/%s"%(Path,File) ) == True:
			os.system("rm  %s/%s"%(Path,File) )
		print "Create File: "+File
		File = open("%s/%s"%(Path,File),'w')
		File.write("%s/\n"%(DS) )
		File.close()	


InputPath = TestArea+"MC15XAOD4LSelection/data/data15_13TeV/SampleLists/"
NewFile = TestArea+"MC15XAOD4LSelection/data/data15_13TeV/SampleLists/Data.txt"
BatchPath = TestArea+"MC15XAOD4LSelection/data/mc15_13TeV/BatchFileLists/"
if os.path.exists(InputPath)==False:
	print "Could not find the Data list"

#~ Periods = [ "A" , "B" , "C" , "D" , "E" , "F" , "G" , "H"  , "VdM" ]
Periods = [ "D" , "E" , "F" , "G" , "H"  , "I" , "J", "VdM" ]

PTags = [ "p2361" ,"p2375" , "p2411" , "p2419", "p2425" ]
PTags = [ "p2411" , "p2419", "p2425" , "p2432" , "p2436" ]
RunNumbers = []
DataSets = []

DataType = "DAOD_SUSY2"

for P in Periods:
	DataSetsPeriod = []
	PeriodRuns = []
	PeriodFile = InputPath+"Period_"+P+".txt"
	ListUpdated = False
	if os.path.exists(PeriodFile):
		print "Read Content of "+PeriodFile
		File = open (PeriodFile, "r")
		for L in File:
			L = L.strip().strip("/")
			print L
			DataSetsPeriod.append(L)
			DataSets.append(L)	
			BuildFileList(L)			
			RunNumbers.append( int (L [ L.find(".") + 1 : L.find(".physics") ]) )
	for T in PTags:
		TN = int( T[ 1 : len(T) ])
		AmiCmd="ami list datasets --type "+DataType+" data15_13TeV.%physics_Main%"+T+"  --data-period "+P
		print AmiCmd
		AMIList = commands.getoutput(AmiCmd)
		for A in AMIList.split():
			if A.find("data15_13TeV") < 0:
				continue
			Run = A [ A.find(".") + 1 : A.find(".physics") ]
			RunCollected = False
			for R in RunNumbers:
				if int(Run) == R:
					RunCollected=True					
					break
			if RunCollected == False:
				ListUpdated = True
				print "Found new DataSet " + A+" from Period "+P
				DataSetsPeriod.append(A)
				DataSets.append(A)
				RunNumbers.append(int(Run))
				PeriodRuns.append(int(Run))
			else:
				for D in DataSetsPeriod:
					if D.find(T) > -1:
						continue
					if D.find(Run) > -1:
						DTN = int(D [ D.rfind("_") + 2: len(D) ] )
						if DTN < TN:
							print "Found newer tag "+T+" for Dataset "+D
							os.system( "echo \'"+D+"\' >> DELETE.txt" )
							ListUpdated = True
							DataSetsPeriod.remove(D)
							DataSets.remove(D)				
							DataSets.append(A)							
							DataSetsPeriod.append(A)							
						break
				
	if ListUpdated == True:
		if os.path.exists(PeriodFile):
			print "Remove the old period File: "+PeriodFile
			os.system ("rm "+PeriodFile)
		File = open (PeriodFile , "w")
		PeriodRuns = sorted(PeriodRuns)
		for D in DataSetsPeriod:
			File.write(D+"/\n")
			BuildFileList(D)
	RunNumbers = sorted (RunNumbers)
	if P!= "D" and P!="VdM":
		RangeFile = InputPath+"/Data_D-"+P+".txt"
		if os.path.exists(RangeFile)==True:
			print "remove the old File "+RangeFile
			os.system ("rm "+RangeFile)
		RangeOut = open(RangeFile,"w")
		for D in DataSets:
			RangeOut.write(D+"/\n")

if os.path.exists(NewFile)==True:
	print "remove the old File"
	os.system ("rm "+NewFile)

Output = open(NewFile,"w")
for D in DataSets:
	Output.write(D+"/\n")
	
