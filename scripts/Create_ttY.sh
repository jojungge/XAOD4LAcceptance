#!/bin/bash

# user-defined parameters
RCArea=${ROOTCOREBIN}"/../"    		#RC folder
WORKDIR=${ROOTCOREBIN}"/bin/"${ROOTCORECONFIG}"/"              #run folder
# some initial output
echo "###############################################################################################"
echo "					 Enviroment variables"
echo "###############################################################################################"
echo "USER"=${USER}
echo "HOME-AREA="${HOME}
echo "X509_USER_PROXY="${X509_USER_PROXY}
echo "ROOTCOREDIR="${ROOTCOREDIR}
echo "ROOTCORECONFIG="${ROOTCORECONFIG}
echo "ROOTCOREOBJ="${ROOTCOREOBJ}
echo "ROOTCOREBIN="${ROOTCOREBIN}
echo "ROOTVER"=${ROOTVER}
echo "RC-Area="${RCArea}
echo "WORKDIR="${WORKDIR}
echo "RUCIO_ACCOUNT"=${RUCIO_ACCOUNT}
echo "EMI_OVERRIDE_JAVA_HOME"=${EMI_OVERRIDE_JAVA_HOME}
echo "HostName="${HOSTNAME}
echo "###############################################################################################"
echo "					Setting up the enviroment"
echo "###############################################################################################"
echo "cd /tmp"
cd /tmp
if [ -z  ${ATLAS_LOCAL_ROOT_BASE} ]; then
 
	echo "source /t2/sw/setupwn_cvmfs.sh"
	source /t2/sw/setupwn_cvmfs.sh
	echo "Setting Up the ATLAS Enviroment:"
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
	
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm" 
	source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh --rootVersion ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm
	echo "Setup ROOTCORE:"
	echo "export PATH=${WORKDIR}/:$PATH"
	export PATH=${WORKDIR}/:$PATH
	echo "export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${RCcfg}/:${LD_LIBRARY_PATH}"
	export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${ROOTCORECONFIG}:${LD_LIBRARY_PATH}
else
	if [ $#  -lt 2 ];then
		exit
	fi
	Date=${1}
	Lumi=${2}
	InDir="/ptmp/mpp/"${USER}"/Cluster/Batch_tmp/"${Date}"/"
	OutDir="/ptmp/mpp/"${USER}"/Cluster/Output/"${Date}"/"
	Name="SM_BG"
	Options="Red-Mad ttLL-13TeV ZZ-13TeV Higgs tttt Sherpa_VVV ttW-13TeV"
fi
TmpDir=${InDir}/${Name}
echo "###############################################################################################"
echo "				JobOptions:"
echo "###############################################################################################"
echo "Name="${Name}
echo "InDir="${InDir}
echo "OutDir="${OutDir}
echo "Options="${Options}
echo "Lumi="${Lumi}

Label=""


##tt Gamma over ttZ
echo "Create_Z-G -Z ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -G  ${OutDir}/ttY/ttY.root -O ${OutDir} -N ttZ-ttY"
ZRegion="Z  noZ"
YRegion=" 2B- 1B-"
MeffC="300 600 700 800 900 1000 1100 1200"
Plot=""
for Z in ${ZRegion};do
	for Y in ${YRegion};do
	 Plot=" --ZPlot rec_4l${Z}-Z-pt --YPlot rec_2l${Z}${Y}Y-pt "
	for M in ${MeffC};do
			Plot="${Plot} --ZPlot rec_4l${Z}-Z_MeffC_${M}-pt --YPlot rec_2l${Z}${Y}Y_MeffC_${M}-pt "
#			Create_Z-G ${Label} -Z ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -G  ${OutDir}/ttY/ttY.root -O ${OutDir} -N "ttZ-ttY" --ZPlot "${Z}Z_MeffC_${M}-pt" --YPlot "${Y}Y_MeffC_${M}-pt" --LogY
#			Create_Z-G ${Label} -Z ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -G  ${OutDir}/ttY/ttY.root -O ${OutDir} -N "ttZ-ttY" --ZPlot "${Z}Z_MeffC_${M}-pt" --YPlot "${Y}Y_MeffC_${M}-pt" --LogY

			done
	echo "Create_Z-G ${Label} -Z ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -G  ${OutDir}/ttY/ttY.root -O ${OutDir} -N \"ttZ-ttY\" --LogY ${Plot} "
	Create_Z-G ${Label} -Z ${OutDir}/ttLL-13TeV/ttLL-13TeV.root -G  ${OutDir}/ttY/ttY.root -O ${OutDir} -N "ttZ-ttY" --LogY ${Plot}
	sleep 2
	done
	
done

if [ -d ${TmpDir} ]; then
	echo "Remove the previous version of "${TmpDir}
	rm -rf ${TmpDir}
fi

if [ -d ${TmpDir}_SUSY2 ]; then
	echo "Remove the previous version of "${TmpDir}_SUSY2
	rm -rf ${TmpDir}_SUSY2
fi
mkdir -p ${TmpDir}

## ttGamma 2 Lepton
#~ DrawTogether ${Label} --NoSumDa --NoCenRatio --Stack -l ${Lumi} --Normalize ${Log} --NoTH2 -N "ttY-${Name}" -T "t#bar{t} #rightarrow2 leptons" -O ${OutDir}  --RaTit "Contribution" \
	-D ${OutDir}/ttY/ttY.root -Dn "t#bar{t}+#gamma" \
	-D ${OutDir}/ttY_ttbar/ttY_ttbar.root -Dn "t#bar{t}" \
	-V ${OutDir}/ttY/ttY.root -Vn "t#bar{t}+#gamma" \
	-V ${OutDir}/ttY_ttbar/ttY_ttbar.root -Vn "t#bar{t}" \

#~ ${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_ttY-${Name}/"

## ttGamma 2 Lepton complete background list


DrawTogether ${Label} --NoSumDa --NoCenRatio --Stack -l ${Lumi} --Normalize ${Log} --NoTH2 -N "BG-ttY-${Name}"  -T "t#bar{t} #rightarrow2 leptons" -O ${OutDir}  --RaTit "Contribution" \
	-D ${OutDir}/ttY/ttY.root -Dn "t#bar{t}+#gamma" \
	-D ${OutDir}/ttY_ttbar/ttY_ttbar.root -Dn "t#bar{t}" \
	-D ${OutDir}/ttY_ZJets/ttY_ZJets.root -Dn "Z+jets" \
	-D ${OutDir}/ttY_WJets/ttY_WJets.root -Dn "W+jets" \
	-V ${OutDir}/ttY/ttY.root -Vn "t#bar{t}+#gamma" \
	-V ${OutDir}/ttY_ttbar/ttY_ttbar.root -Vn "t#bar{t}" \
	-V ${OutDir}/ttY_ZJets/ttY_ZJets.root -Vn "Z+jets" \
	-V ${OutDir}/ttY_WJets/ttY_WJets.root -Vn "W+jets" \
	
DrawTogether ${Label} --NoSumDa --NoCenRatio --Stack -l ${Lumi} --Normalize ${Log} --NoTH2 -N "BG-ttYZ-${Name}"  -T "t#bar{t} #rightarrow2 leptons" -O ${OutDir}  --RaTit "Contribution" \
	-D ${OutDir}/ttY/ttY.root -Dn "t#bar{t}+#gamma" \
	-D ${OutDir}/ttY_ttbar/ttY_ttbar.root -Dn "t#bar{t}" \
	-D ${OutDir}/ttY_ZJets/ttY_ZJets.root -Dn "Z+jets" \
	-D ${OutDir}/ttY_ZYJets/ttY_ZYJets.root -Dn "Z#gamma+jets" \
	-D ${OutDir}/ttY_WJets/ttY_WJets.root -Dn "W+jets" \
	-V ${OutDir}/ttY/ttY.root -Vn "t#bar{t}+#gamma" \
	-V ${OutDir}/ttY_ttbar/ttY_ttbar.root -Vn "t#bar{t}" \
	-V ${OutDir}/ttY_ZJets/ttY_ZJets.root -Vn "Z+jets" \
	-V ${OutDir}/ttY_ZYJets/ttY_ZYJets.root -Vn "Z#gamma+jets" \
	-V ${OutDir}/ttY_WJets/ttY_WJets.root -Vn "W+jets" \

DrawTogether  ${Label} --ROOTFile   --Range --StackDa --ComDa --NoSumDa -l ${DataLumi} --Normalize ${Log} --NoTH2 -N "Data-ttY-DATA-${Name}"  -T " " -O ${OutDir}  --RaTit "Data / MC" \
	-D ${OutDir}/ttY/ttY.root -Dn "t#bar{t}+#gamma" \
	-D ${OutDir}/ttY_ZJets/ttY_ZJets.root -Dn "Z+jets" \
	-D ${OutDir}/ttY_ttbar/ttY_ttbar.root -Dn "t#bar{t}" \
	-V ${OutDir}/ttY_Data25ns/ttY_Data25ns.root -Vn "Data" 

DrawTogether  ${Label} --ROOTFile  --Range --StackDa --ComDa --NoSumDa -l ${DataLumi} --Normalize ${Log} --NoTH2 -N "Data-ttYZ-DATA-${Name}"  -T " " -O ${OutDir}  --RaTit "Data / MC" \
	-D ${OutDir}/ttY-PileUp/ttY-PileUp.root -Dn "t#bar{t}+#gamma" \
	-D ${OutDir}/ttY_ZJets-PileUp/ttY_ZJets-PileUp.root -Dn "Z+jets" \
	-D ${OutDir}/ttY_ttbar-PileUp/ttY_ttbar-PileUp.root -Dn "t#bar{t}" \
	-V ${OutDir}/ttY_Data25ns/ttY_Data25ns.root -Vn "Data" 

	#~ DrawTogether ${Label}  --NoComRatio  --Stack -l  ${DataLumi} --Normalize ${Log} --NoTH2 -N "Data-ttY-DATA-${Name}"  -T "t#bar{t} #rightarrow2 leptons" -O ${OutDir}  --RaTit "MC / Data" \
	-D ${OutDir}/ttY/ttY.root -Dn "t#bar{t}+#gamma" \
	-D ${OutDir}/ttY_ZJets/ttY_ZJets.root -Dn "Z+Jets" \
	-D ${OutDir}/ttY_ttbar/ttY_ttbar.root -Dn "t#bar{t}" \
	-D ${OutDir}/ttY_WJets/ttY_WJets.root -Dn "W+Jets" \
	-V ${OutDir}/ttY_Data25ns/ttY_Data25ns.root -Vn "Data" 

${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_BG-ttY-${Name}/"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Data-ttY-DATA-${Name}"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_BG-ttYZ-${Name}/"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Data-ttYZ-DATA-${Name}"

${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_Data-ttY-DATA-${Name}-PileUp"
