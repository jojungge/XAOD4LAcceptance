import os
import commands
import math
import sys
import re
import random
import subprocess
import ROOT
ROOTCOREDIR = os.getenv("ROOTCOREDIR")
ROOTCORECONFIG = os.getenv("ROOTCORECONFIG")
ROOTCOREOBJ = os.getenv("ROOTCOREOBJ")
ROOTCOREBIN = os.getenv("ROOTCOREBIN")
USER = os.getenv("USER")
Lumi = "01"
DrawLabels = False

	
histos = []
def ScaleAxisLabels(H , s=0.25):
	 H.SetMinimum(0.001)
	 H.GetYaxis().SetLabelFont(43)	 
	 H.GetXaxis().SetLabelFont(43)	 
	 H.GetYaxis().SetLabelSize( 24.)
	 H.GetXaxis().SetTitleSize( 24.)
	 H.GetXaxis().SetLabelSize( 24.)
	 H.GetXaxis().SetTitleSize( s *  H.GetXaxis().GetTitleSize());
	 H.GetYaxis().SetTitleSize( s *  H.GetYaxis().GetTitleSize());
	 H.GetYaxis().SetTitleOffset(0.29 );	
	 
def Prepare2PadCanvas(cname, width=800, height=800 ,  VerticalCanvasSplit=1.0):
	can = ROOT.TCanvas(cname,cname,width,height)
	p1 = ROOT.TPad("p1_"+cname,cname,0.0,VerticalCanvasSplit,1.0,1.0)
	p2 = ROOT.TPad("p2_"+cname,cname,0.0,0.0,1.0,VerticalCanvasSplit)
	p1.SetLeftMargin(0.07)
	p2.SetLeftMargin(0.07)

	p1.SetBottomMargin(0)
	p1.SetTopMargin(0.09)
	p2.Draw()
	p2.SetTopMargin(0)
	p2.SetBottomMargin(0.4)
	p2.SetGridy()
	can.cd()
	p1.Draw()
	p2.Draw()
	histos.append(can)
	histos.append(p1)
	histos.append(p2)
	return [can,p1,p2]
   

def DrawTLatex(x,y,text,size=18,font=43,align=11):
        tex = ROOT.TLatex()
        tex.SetTextAlign(align)
        tex.SetTextSize(size)
        tex.SetTextFont(font)
        tex.SetNDC()
        histos.append(tex)
        tex.DrawLatex(x,y,text)
	       
def DrawAtlas(x,y,align=11 ,Status="Work in Progress"):
      #print "Size is %.2f"%self.Size
      DrawTLatex(x,y,"#font[72]{ATLAS} %s"%Status,18.,43,align)

def DrawLumiSqrtS(x,y,align=11, Lumi=20.3 , SqrtS=13):
	lumiToPrint = "%.0f"%(Lumi)
	DrawTLatex(x,y,"#sqrt{s} = %i TeV, L=%s fb^{-1}"%(SqrtS, lumiToPrint),26.,43,align)
	
def ROOTFILELIST (NLSP=[] , Spacing=100 , Date="2015-10-01" , Lumi="1" , Couplings="LLE12k"):
	InputDir="/ptmp/mpp/"+USER+"/Cluster/Output/"+Date+"/"
	OutList = {}
	for n in range (0, len(NLSP)):	
		LSP = [10 ,50]	
		#This writes the column
		for l in range(Spacing , NLSP[n] + Spacing , Spacing):
			if l < NLSP[n] - 10:
				LSP.append(l)
			else:
				LSP.append(NLSP[n]-10)
				break	
		for l in range(0,len(LSP)):
			DS = "C1C1_"+str(NLSP[n])+"_"+str(LSP[l])+"_"+Couplings
			File = InputDir+DS+"/CmpToBg/BG"+Lumi+"fb_"+DS+".root"
			if os.path.isfile(File):
				print "Append File to OutputList:"+File
				TFile = ROOT.TFile.Open(File , "READ")
				OutList [str(NLSP[n])+"_"+str(LSP[l])] = TFile
				histos.append(TFile)
				
			else:
				print "Error File could not be found: "+File
	return OutList

def GetZnValues ( Histo , MeffCut , MetCut):

	BinX = Histo.GetXaxis().FindBin( MeffCut)
	BinY = Histo.GetYaxis().FindBin( MetCut)
	Zn = Histo.GetBinContent(BinX , BinY )
	ZnMax = 0.
	for b in range( 0 , Histo.GetXaxis().GetNbins() ):
		for b1 in range( 0 , Histo.GetYaxis().GetNbins() ):
			ZnCand = Histo.GetBinContent ( b , b1)
			if ZnCand > ZnMax:
				ZnMax=ZnCand	
	return Zn , ZnMax
	
def GetMassGrid (ZNlist):
	Out = {}
	for cfg in ZNlist.iterkeys():
		massNLSP = int(cfg.split("_")[0])
		massLSP = int(cfg.split("_")[1])
		Out[(massNLSP,massLSP)] = ZNlist[cfg]
	return Out 
	
def GetZnHisto1D(Zns):
	NLSPmasses = [] 
	[ NLSPmasses.append(m[0]) for m in Zns.iterkeys() if not m[0] in NLSPmasses ]
	NLSPmasses = sorted(NLSPmasses)
	LSPmasses = {}
	for mN in NLSPmasses:
		LSPmasses[mN] = sorted([ k[1] for k in Zns.iterkeys() if k[0] == mN])
	
	histo_Zn = ROOT.TH1F("ZnHisto","ZnHisto",len(Zns),0,len(Zns))
	histo_ZnBest = ROOT.TH1F("ZnBestHisto","ZnBestHisto",len(Zns),0,len(Zns))
	histos.append(histo_Zn)
	histos.append(histo_ZnBest)
	ibin = 1
	for mN in NLSPmasses:
		for mL in LSPmasses[mN]:
			BinLabel = "(%i,%i)"%(mN,mL)
			histo_Zn.GetXaxis().SetBinLabel(ibin, BinLabel)
			histo_ZnBest.GetXaxis().SetBinLabel(ibin, BinLabel)
			histo_Zn.SetBinContent(ibin,Zns[(mN,mL)][0])
			histo_ZnBest.SetBinContent(ibin,Zns[(mN,mL)][1])
			ibin = ibin+1
	return [histo_Zn, histo_ZnBest]
	
def GetZnHisto2D(Zns):
	NLSPmasses = [] 
	[ NLSPmasses.append(m[0]) for m in Zns.iterkeys() if not m[0] in NLSPmasses ]
	NLSPmasses = sorted(NLSPmasses)
	LSPmasses = {}
	for mN in NLSPmasses:
		LSPmasses[mN] = sorted([ k[1] for k in Zns.iterkeys() if k[0] == mN])
	
	ZnGr = ROOT.TGraph2D()
	histos.append(ZnGr)
	ZnBestGr = ROOT.TGraph2D()
	histos.append(ZnBestGr)
	for mN in NLSPmasses:
		for mL in LSPmasses[mN]:
			ZnGr.SetPoint(ZnGr.GetN(),mN,mL,Zns[(mN,mL)][0])
			if Zns[(mN,mL)][1] > 0:
				ZnBestGr.SetPoint(ZnBestGr.GetN(),mN,mL,Zns[(mN,mL)][0]/Zns[(mN,mL)][1])
			else:
				ZnBestGr.SetPoint(ZnBestGr.GetN(),mN,mL,0)
	
	#ZnGr.Draw("COLZ")
	#ZnBestGr.Draw("COLZ")
	histo_Zn = ZnGr.GetHistogram()
	histo_Zn.GetXaxis().SetTitle("m_{#tilde{#chi}^{#pm}_{1}} [GeV]")
	TickX = len (NLSPmasses)
	TickY = len( LSPmasses [ NLSPmasses[TickX -1 ] ] )*2
	histo_Zn.GetXaxis().SetNdivisions(TickX)
	histo_Zn.GetYaxis().SetNdivisions(TickY)
	
	histo_Zn.GetYaxis().SetTitle("m_{#tilde{#chi}^{0}_{1}} [GeV]")
	histo_Zn.GetZaxis().SetTitle("Significance in Z_{n}")
	histo_Zn.SetMaximum(5.);
	histo_Zn.SetMinimum(0.);	
	
	histo_ZnBest = ZnBestGr.GetHistogram()
	histo_ZnBest.GetXaxis().SetNdivisions(TickX)
	histo_ZnBest.GetYaxis().SetNdivisions(TickY)
	
	histo_ZnBest.GetXaxis().SetTitle("m_{#tilde{#chi}^{#pm}_{1}} [GeV]")
	histo_ZnBest.GetYaxis().SetTitle("m_{#tilde{#chi}^{0}_{1}} [GeV]")
	histo_ZnBest.GetZaxis().SetTitle("Ratio to best possible Z_{n}")
	histo_ZnBest.SetMinimum(0.)
	histo_ZnBest.SetMaximum(1.)
	
	histos.append(histo_Zn)
	histos.append(histo_ZnBest)
	return [histo_Zn, histo_ZnBest]
	
def DrawLabels (Canvas , MeffCut , MetCut):
	Canvas.cd()
	ROOT.gPad.SetRightMargin(0.22)	
	ROOT.gPad.SetTopMargin(0.1)	
	CutValues="m_{eff}>%i GeV, E_{T}^{miss}>%i GeV"%(int(MeffCut) , int(MetCut))
	x0 = 0.05;
	y0=0.95;
	DrawTLatex(0.22,0.82, CutValues , 26)
	if DrawLabels == True:
		DrawAtlas(x0+0.5,y0,11,"Internal")
	DrawLumiSqrtS(x0,y0,11 ,float(Lumi) , 13)
	
def DrawHistograms ( DataSets , MeffCut , MetCut , Pattern , SG_Yield):
	OutDir=os.getcwd()
	ZnForPoint = {}
	SG_ForPoint = {}
	for D in DataSets:
		H = DataSets[D].Get(Pattern)
		histos.append(H)
		[Zn,MaxMeff] = GetZnValues(H , MeffCut , MetCut)
		ZnForPoint[D] = [Zn,MaxMeff]
		H=DataSets[D].Get(SG_Yield)
		histos.append(H) 
		[Yield , MaxYield]=GetZnValues(H,MeffCut,MetCut)
		SG_ForPoint[D] = [Yield , MaxYield] 
		print "Dataset: "+D+"		Zn: "+str(Zn)+"		MaxZn: "+str(MaxMeff)+"		Yield: "+str(Yield)+"		MaxYield: "+str(MaxYield)
		
	MassList = GetMassGrid(ZnForPoint)
	YieldList = GetMassGrid(SG_ForPoint)
	h3,h4 = GetZnHisto2D(MassList)
	h1,h2 = GetZnHisto2D(YieldList)
	
	OutDir=OutDir+"/"
	if os.path.isdir(OutDir)==False:
		print "Create New directory "+OutDir
		os.system ("mkdir -p "+OutDir)
	os.chdir(OutDir)
	
	
	can2 = ROOT.TCanvas("can2","can2",800,600)
	histos.append(can2)
	can2.cd()	
	h3.Draw("COLZ")
	h3b=h3.Clone("h3b")
	h3b.SetContour(1)
	#h3b.SetContourLevel(0,1.645)
	h3b.SetContourLevel(0,3.)
	h3b.Draw("CONT3same")
	DrawLabels(can2 , MeffCut , MetCut)
	
	
	CanName = "2DSig_Meff_"+str(int(MeffCut))+"_Met"+str(int(MetCut))+"_L"+str(int(Lumi))
	can2.SaveAs(CanName+".pdf")

	can3 = ROOT.TCanvas("can3","can3",800,600)
	histos.append(can2)
	can3.cd()
	h4.Draw("COLZ")
	DrawLabels(can3 , MeffCut , MetCut) 	
	CanName = "2DRat_Meff_"+str(int(MeffCut))+"_Met"+str(int(MetCut))+"_L"+str(int(Lumi))
	can3.SaveAs(CanName+".pdf")
	
	can1 = ROOT.TCanvas("can1","can1",800,600)
	histos.append(can1)
	can1.cd()
	
	h1.SetMaximum( h1.GetBinContent(h1.GetMaximumBin() ) )
	h1.GetZaxis().SetTitle("Number of expected events")
	h1.SetContour(99)
	h1.Draw("COLZ")
	
	DrawLabels(can1 , MeffCut , MetCut)

	CanName = "2DYield_Meff_"+str(int(MeffCut))+"_Met"+str(int(MetCut))+"_L"+str(int(Lumi))
	can1.SaveAs(CanName+".pdf")
	
	can4 = ROOT.TCanvas("can4","can4",800,600)
	histos.append(can4)
	
	h2.GetZaxis().SetTitle("Ratio to the maximum yield")
	h2.SetContour(99)
	h2.Draw("COLZ")
	CanName = "2DCutEff_Meff_"+str(int(MeffCut))+"_Met"+str(int(MetCut))+"_L"+str(int(Lumi))
	DrawLabels(can4 , MeffCut , MetCut)
	can4.SaveAs(CanName+".pdf")

if __name__=='__main__':
	
	ROOT.gROOT.Macro("rootlogon.C")
	ROOT.gStyle.SetPalette(55)
	NLSP121 = [500 , 700 , 800 , 900 , 1000 ] # , 1100 , 1200 , 1300]
	NLSP133 = [200 , 300 , 400 , 500 , 600 , 700 , 800 ] #, 900 ]
	Couplings="LLE12k"
	MeffCuts="0,100"
	MetCuts="0,25"
	AndConjunction=True
	DATE="2015-10-01"
	Region="4lnoZ"
	Type="rec"
	Histo="Meff-Met"
	OutDir=os.getcwd()	
	for a in range (1 , len(sys.argv) ):
		if ("--meff" in sys.argv[a] ) and a < len(sys.argv):
			MeffCuts=sys.argv[a+1]			
		elif ("--met" in sys.argv[a] ) and a < len(sys.argv):
			MetCuts = sys.argv[a+1]	
		elif "--lumi" in sys.argv[a] and a < len(sys.argv):
			Lumi  = sys.argv[a+1]
		elif "--Date" in sys.argv[a] and a < len(sys.argv):
			DATE = sys.argv[a+1]
		elif "--OutDir" in sys.argv[a] and a < len(sys.argv):
			OutDir = sys.argv[a+1]
		elif "--Lambda" in sys.argv[a] and a < len(sys.argv):
			Couplings = sys.argv[a+1]
		elif "--Label" in sys.argv[a]:
			DrawLabels=True
	OutDir=OutDir+"/"
	if os.path.isdir(OutDir)==False:
		print "Create New directory "+OutDir
		os.system ("mkdir -p "+OutDir)
	os.chdir(OutDir)	
	Pattern="Zn_Signal_Int_"+Type+"_"+Region+"-"+Histo
	SG_Yield="Signal_Int_"+Type+"_"+Region+"-"+Histo
	DataSets = ROOTFILELIST (NLSP121 , 200 , DATE , Lumi , Couplings)
	if len(DataSets)==0:
		print "Could not find any input data"
		exit(1)
	for Meff in MeffCuts.split(","):
		if len(Meff) == 0:
			continue
		for Met in MetCuts.split(","):
			if len(Met) == 0:
				continue
			MeffCut=float(Meff)
			MetCut=float(Met)
			DrawHistograms(  DataSets, MeffCut,MetCut, Pattern, SG_Yield)
	

