#!/bin/bash

cd $ROOTCOREBIN/../
#~ rc compile CanvasPlot
#~ if [ $? -eq 1 ]; then
	#~ exit
#~ fi
#~ rc compile XAOD4LAcceptance
#~ if [ $? -eq 1 ]; then
	#~ exit
#~ fi
Lumi="1000."
LumiData="165.33"

SubmitDir=${ROOTCOREBIN}/../MC15XAOD4LSelection/scripts/
PkgDir=${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/

HoldJob=""
Backgrounds="ttLL-13TeV Higgs tttt Sherpa_VVV Sherpa_VV ttW-13TeV tt ZJets-Mad WZ ZZ-13TeV tt-HT tt-TwoLep"
Backgrounds="ttLL-13TeV Higgs tttt Sherpa_VVV Sherpa_VV ttW-13TeV tt ZJets-Mad WZ ZZ-13TeV"

cd ${SubmitDir} 

#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --CMS 13 --Lumi ${LumiData} --Campaign "data15_13TeV" --Datasets "Data_A-D.txt" --NEventsPerJob 20000 --Name Data  --Exec "XAOD-SUSY4LAnalysis"  --Options "DFOSVeto false  SusyOR SigIso true CorrectIso true GetPh true  GetZ true Get2L false"   
#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --CMS 13 --Lumi ${LumiData} --Campaign "data15_13TeV" --Datasets "Data_A-D.txt" --NEventsPerJob 20000  --Exec "XAOD-ttYAnalysis" --Name ttY_Data  --Options "DFOSVeto false  SusyOR SigIso true CorrectIso true GetPh true  GetZ true"   
#~ 
LumiData="1040."
LumiData="3200."

if [ 1 -eq 1 ]; then
	
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --CMS 13 --Lumi ${LumiData} --Campaign "data15_13TeV" --Datasets "Data.txt" --NEventsPerJob 20000  --Exec "XAOD-ttYAnalysis" --Name ttY_Data25ns  --Options "DFOSVeto false SusyOR SigIso true CorrectIso true GetPh true  GetZ true"   
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis"  --Name ttY_ttbar-PileUp --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp true SigIso true CorrectIso true ttbar DFOSVeto true" --Datasets "SUSY2_tt.txt"  
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis"  --Name ttY-PileUp --CMS 13 --Lumi ${Lumi} --NEventsPerJob 5000 --Options "PileUp true SigIso true CorrectIso true IsAtlfast ttY DFOSVeto true" --Datasets "SUSY2_ttY-13TeV.txt"  
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_ZJets-PileUp --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp true SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_ZJetsMerged.txt" 
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_WJets-PileUp --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp true  SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_WJets-Pow.txt"  
	
	#~ #python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_ZJets-PileUp --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp true SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_ZJets-Mad.txt" 
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_ZYJets-PileUp --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp true SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_ZJetsGamma-Sherpa.txt" 
	
	# NoPileUp Samples
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis"  --Name ttY --CMS 13 --Lumi ${Lumi} --NEventsPerJob 5000 --Options "PileUp false SigIso true CorrectIso true IsAtlfast ttY DFOSVeto true" --Datasets "ttY-13TeV.txt"  
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis"  --Name ttY_ttbar --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp false SigIso true CorrectIso true ttbar DFOSVeto true" --Datasets "SUSY2_tt.txt"  
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_ZJets --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp false SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_ZJetsMerged.txt" 
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_ZYJets --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp false SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_ZJetsGamma-Sherpa.txt" 
	#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --Exec "XAOD-ttYAnalysis" --GetTruth false  --Name ttY_WJets --CMS 13 --Lumi ${Lumi} --NEventsPerJob 20000 --Options "PileUp false SigIso true CorrectIso true Bg DFOSVeto true" --Datasets "SUSY2_WJets-Pow.txt"  
	#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Name ttLL-13TeV  --Datasets "ttLL-13TeV.txt"  --NEventsPerJob 10000 --CMS 13 --Lumi ${Lumi} --GetTruth false --Options "DFOSVeto true PileUp false CorrectIso true SusyOR SigIso true GetZ true GetW false GetPhoton true Get2L false GetTau true"
	Hold="Data ttY_Data ttY_ttbar ttY ttY_ZJets ttY_WJets Data25ns ttY_Data25ns ttLL-13TeV"
	for H in ${Hold};do
		HoldJob=" ${HoldJob} --HoldJob ${H}_mergeHistos ${HoldJob} --HoldJob ${H}-PileUp_mergeHistos" 
	done
	LogPlots="jet-pt"
	Regions="rec_2lnoZ1B- rec_2lnoZ2B- "
	Log=""

for L in ${LogPlots};do
	for R in ${Regions};do
		Log="${Log} --Log ${R}${L} --Log Int_${R}${L}"   
	done
done
	cd ${PkgDir}
	python SubmitScriptsToBatch.py ${Log} ${HoldJob} --Name "ttY-BG" --Exec "${PkgDir}/Create_ttY.sh" --Options "${Backgrounds}" --DataLumi ${LumiData}
	HoldJob=""	
	exit
fi

#~ python SubmitToBatch.py ${HoldJob} --CreateBuildJob --CMS 13 --Lumi ${LumiData} --Campaign "data15_13TeV" --Datasets "Data_D-H.txt" --NEventsPerJob 20000 --Name Data25ns  --Exec "XAOD-SUSY4LAnalysis"  --Options "DFOSVeto true  SusyOR SigIso true CorrectIso true GetPh true  GetZ true Get2L false"   
#~ exit
for B in ${Backgrounds};do
	break
	cd ${SubmitDir} 
	python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Name SUSY2_${B}  --Datasets "SUSY2_${B}.txt"  --NEventsPerJob 10000 --CMS 13 --Lumi ${Lumi} --GetTruth false --Options "DFOSVeto true PileUp false CorrectIso true SusyOR SigIso true GetZ false GetW false GetPhoton true Get2L false GetTau true"
	python SubmitToBatch.py   --HoldJob SUSY2_${B}_mergeHistos --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Name SUSY2_${B}-PileUp  --Datasets "SUSY2_${B}.txt"  --NEventsPerJob 10000 --CMS 13 --Lumi ${Lumi} --GetTruth false --Options "DFOSVeto true PileUp true CorrectIso true SusyOR SigIso true GetZ false GetW false GetPhoton true Get2L false GetTau true"
	#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Name SUSY2_${B}-Iso  --Datasets "SUSY2_${B}.txt"  --NEventsPerJob 10000 --CMS 13 --Lumi ${Lumi} --GetTruth false --Options "DFOSVeto true PileUp true CorrectIso false SusyOR SigIso true GetZ false GetW false GetPhoton true Get2L false GetTau true"
	#~ cd ${PkgDir}
	#~ python SubmitScriptsToBatch.py ${Log} --HoldJob "SUSY2_${B}"  --Name "Dup_SUSY2_${B}" --Exec "${PkgDir}/Duplicates.sh" --Options "SUSY2_${B}" 
	HoldJob="${HoldJob} --HoldJob ${B}_mergeHistos --HoldJob SUSY2_${B}_mergeHistos --HoldJob SUSY2_${B}-Iso_mergeHistos --HoldJob SUSY2_${B}-PileUp_mergeHistos  "	
done
LogPlots="SR_ev_runII  Z-Children W-Children  gauge_n mu_EtCone mu_PtCone mu_PtVarCone mu_TopoEtCone el_EtCone el_PtCone el_PtVarCone el_TopoEtCone mu_IsoEtCone mu_IsoPtCone mu_IsoPtVarCone mu_IsoTopoEtCone el_IsoEtCone el_IsoPtCone el_IsoPtVarCone el_IsoTopoEtCone"
Regions="tru_ tru_4l- tru_4lnoZ- tru_2l- tru_2lnoZ- rec_ rec_4l- rec_4lnoZ-"
Log=""

for L in ${LogPlots};do
	for R in ${Regions};do
		Log="${Log} --Log ${R}${L} --Log Int_${R}${L}"   
	done
done
cd ${PkgDir}

Backgrounds="ttLL-13TeV Higgs tttt Sherpa_VVV ZZ-13TeV ttW-13TeV ZJets-Mad WZ tt"
#~ python SubmitScriptsToBatch.py ${Log} ${HoldJob} --Name "SM-BG" --Exec "${PkgDir}/CreateBackground.sh" --Options "${Backgrounds}" --DataLumi ${LumiData}
#~ 
#~ Backgrounds="ttLL-13TeV Higgs tttt Sherpa_VVV Sherpa_VV ttW-13TeV tt ZJets-Mad WZ"
#~ python SubmitScriptsToBatch.py ${Log} ${HoldJob} --Name "SM-BG-Sherpa" --Exec "${PkgDir}/CreateBackground.sh" --Options "${Backgrounds}" --DataLumi ${LumiData}

#~ Backgrounds="ttLL-13TeV Higgs tttt Sherpa_VVV ZZ-13TeV ttW-13TeV ZJets-Mad WZ"
#~ python SubmitScriptsToBatch.py ${Log} ${HoldJob} --Name "SM-BG-WoTT" --Exec "${PkgDir}/CreateBackground.sh" --Options "${Backgrounds}" --DataLumi ${LumiData}
#~ 
#~ Backgrounds="ttLL-13TeV-Iso Higgs-Iso tttt-Iso Sherpa_VVV-Iso Sherpa_VV-Iso ttW-13TeV-Iso tt-Iso ZJets-Mad-Iso WZ-Iso"
#~ python SubmitScriptsToBatch.py ${Log} ${HoldJob} --Name "SM-BG-Iso" --Exec "${PkgDir}/CreateBackground.sh" --Options "${Backgrounds}" --DataLumi ${LumiData}

HoldJob="--HoldJob SM-BG "	
NLSP="400 500 700 800 900 1000" # 1100 1200 1300"
LSP="10 50 100 200 400 490 600 690 700 790 800 890 990 1000 1090 1190 1200 1290"
NLSP="800"
LSP="10"
StandardOptions="GetZ false GetW false GetPhoton false Get2L false SaveTrees false SUSY IsAtlfast GetTau false PileUp false SusyOR"
Couplings="LLE12k-NDFOS LLE12k-NDFOSIso LLE12k-NDFOSNIso LLE12k-NDFOSRIso LLE12k-NDFOSTIso LLE12k LLE12k-NIso LLE12k-RIso LLE12k-Iso"
Couplings="LLE12k"
for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
		break
			#Hold each column before the next columns will be analyzed -> parallel sessions will be 
			HoldJob=" ${HoldJob} ${Hold} "
			Hold="--HoldJob SM-BG"
			for L1 in ${LSP}; do
				if [ ${L1} -ge ${N} ];then
					break								
				fi
				if [ ! -f ${SubmitDir}/../data/mc15_13TeV/SampleLists/Signal/C1C1_${N}_${L1}_LLE12k.txt ];then
					continue
				fi			
				for C in ${Couplings};do
					Hold="${Hold} --HoldJob Clean_C1C1_${N}_${L1}_$C"
				done
			done				
			break
		fi
		if [ ! -f ${SubmitDir}/../data/mc15_13TeV/SampleLists/Signal/C1C1_${N}_${L}_LLE12k.txt ];then
			continue
		fi
		cd ${SubmitDir}
		
		##DFOS related jobs CorrectedIso RcalculatedIso TrackOnlyIso 		
		#~ 
		python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k --Options "DFOSVeto true CorrectIso true SigIso true ${StandardOptions}"  ${1}
			#~ continue
		exit
		if [ ${L} -le 10 ];then
			echo""
		#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-Iso --Options "DFOSVeto true CorrectIso false SigIso true ${StandardOptions}"
		#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NIso --Options "DFOSVeto true CorrectIso false SigIso false ${StandardOptions}"  		
		#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-RIso --Options "DFOSVeto true CorrectIso true RecalPtCone true SigIso true ${StandardOptions}" 
		#~ #python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-TIso --Options "DFOSVeto true CorrectIso true CorrectTopoEt false SigIso true ${StandardOptions}" 
		fi
		if [ ${N} -eq 1000 ];then
		python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-TRU4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name TRU_C1C1_${N}_${L}_LLE12k  --Options "DFOSVeto true ${StandardOptions}"  ${1}
		python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-TRU4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name TRU_C1C1_${N}_${L}_LLE12k-NDFOS   --Options "DFOSVeto false ${StandardOptions}"  ${1}
		fi
		#~ 
		## NoDFOS related jobs CorrectedIso RcalculatedIso TrackOnlyIso 
	#~ 
		#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NDFOS --Options "DFOSVeto false CorrectIso true SigIso true ${StandardOptions}"
			#~ if [ ${L} -le 50 ];then
		#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NDFOSNIso --Options "DFOSVeto false CorrectIso false SigIso false  ${StandardOptions}"
		#~ python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NSFOSNIso --Options "NoRemSFOS CorrectIso false SigIso false  ${StandardOptions}"
		#~ 
		#~ #python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NDFOSIso  --Options "DFOSVeto false CorrectIso false SigIso true ${StandardOptions}" 
		#~ #python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NDFOSIso  --Options "DFOSVeto false CorrectIso false SigIso true ${StandardOptions}" 
		#~ #python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NDFOSRIso --Options "DFOSVeto false CorrectIso true SigIso true RecalPtCone true ${StandardOptions}" 
		#python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Lumi ${Lumi} ${Hold} --Datasets "C1C1_${N}_${L}_LLE12k.txt" --Name C1C1_${N}_${L}_LLE12k-NDFOSTIso --Options "DFOSVeto false CorrectIso true SigIso true CorrectTopoEt false ${StandardOptions}" 
		#~ fi		
		cd ${PkgDir}
			for C in ${Couplings};do
				HoldJob=" ${HoldJob} --HoldJob ZN_C1C1_${N}_${L}_${C}"
				python SubmitScriptsToBatch.py  --HoldJob "SM-BG" --HoldJob C1C1_${N}_${L}_${C}_mergeHistos  ${Log} --Name "ZN_C1C1_${N}_${L}_${C}" --Exec "${PkgDir}/CreateZnPlots.sh" --Options "C1C1 ${N} ${L} ${C}"
			done		
		done
done
cd ${PkgDir}
for C in ${Couplings};do
	python SubmitScriptsToBatch.py  ${HoldJob} ${Log} --Name "ZnGrid_${C}" --Exec "${PkgDir}/ZnGrid.sh" --Options "${C}"	
done
exit

NLSP="300 400 500 600 700 800"  # 900 1000 1100 1200 1300"
for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
				cd ${SubmitDir}
		python SubmitToBatch.py --CreateBuildJob --Exec "XAOD-SUSY4LAnalysis" --Name C1C1_${N}_${L}_LLE12k --Datasets "SUSY2_C1C1_${N}_${L}_LLEi33.txt" --CMS 13 --Lumi ${Lumi} --Options "CorrectIso true SigIso true GetZ false GetW false GetPhoton true Get2L false SaveTrees false SUSY IsAtlfast GetTau false"  ${1}
		cd ${PkgDir}
		python SubmitScriptsToBatch.py  --HoldJob "SM-BG" --HoldJob C1C1_${N}_${L}_LLE12k  --Name "ZN_C1C1_${N}_${L}_LLEi33" --Exec "${PkgDir}/CreateBackground.sh" --Options "C1C1 ${N} ${L} LLE12k"
		done

	done
done
