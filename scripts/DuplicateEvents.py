import os
import commands
import math
import sys
import re
import random
import subprocess

TestArea = os.getenv("ROOTCOREBIN")+"/../"
ROOTCOREDIR = os.getenv("ROOTCOREDIR")
ROOTCORECONFIG = os.getenv("ROOTCORECONFIG")
ROOTCOREOBJ = os.getenv("ROOTCOREOBJ")
ROOTCOREBIN = os.getenv("ROOTCOREBIN")
if len(sys.argv)< 3:
	print "Not enough arguments given to the script usage DuplicateEvents.py <InDir> <OutFile>"
	exit(1)

InputDir=sys.argv[1].rstrip()
OutFile=sys.argv[2]
if os.path.isdir(InputDir)==False:
	print "Error the given path is not a directory: "+InputDir
	exit(1)
InDir = os.listdir(InputDir)

Events = {}
print "####################################################################################################################"
print "						Check the root files for duplicate processed events"
print "####################################################################################################################"
for F in InDir:
	if ".txt" not in F:
		continue
	print "Read in: "+F
	File = open (InputDir+"/"+F)
	for L in File:
		Run,Ev = L.split()
		try:
			Events [ (Run,Ev) ] +=1
		except:
			Events [ (Run,Ev) ] = 0
print "####################################################################################################################"
print "					ReadIn of Files finished will now check if there is any duplicate event"
print "####################################################################################################################"

Out = open(OutFile , "w")
for E in Events.iterkeys():
	N = Events [ (E[0],E[1]) ]
	if  N > 0:
		print "Found Duplicate event "+str(E[1])+" in Run: "+E[0]
		Out.write ("Run: "+E[0]+"	Event: "+E[1]+"	Appeared: "+str(N+1)+" TotalEvents: "+str(len(Events))+" \n")
Out.close()

print "####################################################################################################################"
print "						Skript exited successfully"
print "####################################################################################################################"
