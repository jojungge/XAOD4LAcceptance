import os
import commands
import math
import sys
import re

if  os.getenv("ROOTCOREBIN") == None:
    print "No ROOTCORE... Please setup ROOTCORE"
    exit(1)
User=os.getenv("USER")
RootCoreBin=os.getenv("ROOTCOREBIN")

EVNT_DIR="/ptmp/mpp/%s/Cluster/Simulation/EVNT"%(User)
NTUP_DIR="/ptmp/mpp/%s/Cluster/mc15_13TeV.Simulation/TruthAOD"%(User)

NTUP_DIR="/ptmp/mpp/%s/Grid/Data/DAOD"%(User)

#NTUP_DIR="/ptmp/mpp/%s/Cluster/mc15_13TeV.Simulation/DAOD_DC14"%(User)
#NTUP_DIR="/ptmp/mpp/%s/Datasets/DC14/"%(User)

xSecFile="%s/../XAOD4LAcceptance/scripts/xSection.txt"%(RootCoreBin)
List_DIR="%s/../MC15XAOD4LSelection/data/mc15_13TeV/BatchFileLists/"%(RootCoreBin)
#List_DIR="%s/../MC15XAOD4LSelection/data/mc14_13TeV/BatchFileLists/"%(RootCoreBin)

os.system("mkdir -p "+List_DIR)
print "EVNT_DIR: "+EVNT_DIR
print "NTUP_DIR: "+NTUP_DIR
print "List_DIR: "+List_DIR

os.chdir(NTUP_DIR)
DatasetList=os.listdir(NTUP_DIR)
for Datasets in DatasetList:
	print Datasets
	os.chdir("%s/%s"%(NTUP_DIR,Datasets))
	FileList=os.listdir("%s/%s"%(NTUP_DIR,Datasets))
	
	if os.path.exists("%s/%s.txt"%(List_DIR,Datasets)) == False:
		List = open ("%s/%s.txt"%(List_DIR,Datasets) ,"w" )
		List.close()
	List = open ("%s/%s.txt"%(List_DIR,Datasets) )
	for Files in FileList:		
		if ".root" not in Files:
			continue
		IsInList=False
		for F in List:
			if Files in F:
				IsInList=True					
				break				
		if IsInList==False:
			print "Adding File %s to %s/%s.txt"%(Files,List_DIR,Datasets)
			os.system("echo \"%s/%s/%s\" >> %s/%s.txt"%(NTUP_DIR,Datasets,Files,List_DIR,Datasets))
	List.close()
	if 1==2 and int(Datasets[0:3]) < 900:
		Sample=Datasets.split("_")
		SName="DAOD_WinoPM_%s_%s_%s"%(Sample[3] , Sample[4] , Sample[5])
		if os.path.exists( "%s/../SampleLists/%s.txt"%(List_DIR,SName) )==False:
			print SName
			S = open ("%s/../SampleLists/%s.txt"%(List_DIR,SName), "w")
			S.write("%s/\n"%Datasets)
			S.close()
		
print "FileLists updated... Now Updating the xSection Database"
exit
os.system("rm "+xSecFile)
os.chdir(EVNT_DIR)
DatasetList=commands.getoutput("ls ./")
N=0
xSection=0.
Eff=0.
str0="MetaData: cross-section (nb)="
str1="MetaData: GenFiltEff ="
for Datasets in DatasetList.split():
	Dir="%s/%s"%(EVNT_DIR,Datasets)
	if os.path.isdir(Dir)== False:
		continue
	os.chdir(Dir)
	FileList=commands.getoutput("ls *.log")
	if "No such file or directory" in FileList:
		continue
	if "Herwigpp" in Datasets:
		continue
	N=0
	xSection=0.
	Eff=0.
	for Files in FileList.split():
		N=N+1
		Line=commands.getoutput("grep  \"%s\" %s"%(str0,Files))
		Line1=commands.getoutput("grep \"%s\" %s"%(str1,Files))		
		Pos=Line.find(str0)		
		xSec=Line[(Pos+len(str0)):len(Line)]
		xSec=xSec.strip()		
		xSection=xSection+float(xSec)*1000		
		Pos=Line1.find(str1)		
		FiltEff=Line1[(Pos+len(str1)):len(Line1)]
		FiltEff=FiltEff.strip()		
		if len(FiltEff)==0:
			Eff=Eff+1.
		else:
			Eff=Eff+float(FiltEff.split()[0])
	xSection=xSection/N
	Eff=Eff/N
	kFac=1.
	Entry="%s	%s	%f	%f	%f	1.000000"%(Datasets[0:6],Datasets[7:],xSection,kFac,Eff)
	print Entry
	os.system("echo \"%s\" >> %s"%(Entry,xSecFile))
	
