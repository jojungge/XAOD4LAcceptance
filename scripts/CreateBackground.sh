#!/bin/bash

# user-defined parameters
RCArea=${ROOTCOREBIN}"/../"    		#RC folder
WORKDIR=${ROOTCOREBIN}"/bin/"${ROOTCORECONFIG}"/"              #run folder
# some initial output
echo "###############################################################################################"
echo "					 Enviroment variables"
echo "###############################################################################################"
echo "USER"=${USER}
echo "HOME-AREA="${HOME}
echo "X509_USER_PROXY="${X509_USER_PROXY}
echo "ROOTCOREDIR="${ROOTCOREDIR}
echo "ROOTCORECONFIG="${ROOTCORECONFIG}
echo "ROOTCOREOBJ="${ROOTCOREOBJ}
echo "ROOTCOREBIN="${ROOTCOREBIN}
echo "ROOTVER"=${ROOTVER}
echo "RC-Area="${RCArea}
echo "WORKDIR="${WORKDIR}
echo "RUCIO_ACCOUNT"=${RUCIO_ACCOUNT}
echo "EMI_OVERRIDE_JAVA_HOME"=${EMI_OVERRIDE_JAVA_HOME}
echo "HostName="${HOSTNAME}
echo "###############################################################################################"
echo "					Setting up the enviroment"
echo "###############################################################################################"
echo "cd /tmp"
cd /tmp
if [ -z  ${ATLAS_LOCAL_ROOT_BASE} ]; then
 
	echo "source /t2/sw/setupwn_cvmfs.sh"
	source /t2/sw/setupwn_cvmfs.sh
	echo "Setting Up the ATLAS Enviroment:"
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
	source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
	
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm" 
	source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh --rootVersion ${ROOTVER}-${ROOTCORECONFIG} --skipConfirm
	echo "Setup ROOTCORE:"
	echo "export PATH=${WORKDIR}/:$PATH"
	export PATH=${WORKDIR}/:$PATH
	echo "export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${RCcfg}/:${LD_LIBRARY_PATH}"
	export LD_LIBRARY_PATH=${ROOTCOREBIN}/lib/${ROOTCORECONFIG}:${LD_LIBRARY_PATH}
else
	if [ $#  -lt 2 ];then
		exit
	fi
	Date=${1}
	Lumi=${2}
	InDir="/ptmp/mpp/"${USER}"/Cluster/Batch_tmp/"${Date}"/"
	OutDir="/ptmp/mpp/"${USER}"/Cluster/Output/"${Date}"/"
	Name="SM_BG"
	Options="Red-Mad ttLL-13TeV ZZ-13TeV Higgs tttt Sherpa_VVV ttW-13TeV"
fi
TmpDir=${InDir}/${Name}
echo "###############################################################################################"
echo "				JobOptions:"
echo "###############################################################################################"
echo "Name="${Name}
echo "InDir="${InDir}
echo "OutDir="${OutDir}
echo "Options="${Options}
echo "Lumi="${Lumi}

Label=""

if [ -d ${TmpDir} ]; then
	echo "Remove the previous version of "${TmpDir}
	rm -rf ${TmpDir}
fi

if [ -d ${TmpDir}_SUSY2 ]; then
	echo "Remove the previous version of "${TmpDir}_SUSY2
	rm -rf ${TmpDir}_SUSY2
fi
mkdir -p ${TmpDir}
mkdir -p ${TmpDir}_SUSY2

for O in ${Options};do
	
	
	Files=`ls ${InDir}/SUSY2_${O}/*.root*`
	echo "Copy background SUSY2_${O}"
	for F in ${Files};do		
		cp ${F} ${TmpDir}_SUSY2
	done	

done

echo "Copy of the Files is done. Parse folder content to FileList"

Files=`ls ${TmpDir}_SUSY2/*.root*`
for F in ${Files};do
	echo "echo \"${F}\" >> ${TmpDir}_SUSY2/Files.txt"
	echo "${F}" >> ${TmpDir}_SUSY2/Files.txt
done
# ${ROOTCOREBIN}/../CanvasPlot/scripts/Merge.sh "${Name}"  "${TmpDir}/Files.txt" "${OutDir}/${Name}" "${Lumi}"

${ROOTCOREBIN}/../CanvasPlot/scripts/Merge.sh "${Name}_SUSY2"  "${TmpDir}_SUSY2/Files.txt" "${OutDir}/${Name}_SUSY2" "${Lumi}"
 
## 		ZZ comparison
#~ DrawTogether ${Label} --Normalize -l ${Lumi} --NoTH2 -N "VV" -T "DiBoson MC" -O ${OutDir}  ${Log} \
	-V ${OutDir}/SUSY2_ZZ-13TeV/SUSY2_ZZ-13TeV.root -Vn "Powerheg" \
	-D ${OutDir}/SUSY2_Sherpa_VV/SUSY2_Sherpa_VV.root -Dn "Sherpa"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_VV/"

## 		4 Lepton background DAOD_SUSY
if [[ ${Options} == *"ZZ-13TeV"* ]];then

DrawTogether  ${Label} --NoCenRatio  --NoSumDa --Stack -l ${Lumi} --Normalize ${Log} --NoTH2 -N "${Name}-SUSY2" -T " " -O ${OutDir} --RaTit "Contribution" \
	-V ${OutDir}/SUSY2_Sherpa_VVV/SUSY2_Sherpa_VVV.root -Vn "VVV" \
	-V ${OutDir}/SUSY2_ZZ-13TeV/SUSY2_ZZ-13TeV.root -Vn "ZZ" \
	-V ${OutDir}/SUSY2_tttt/SUSY2_tttt.root -Vn "tttt" \
	-V ${OutDir}/SUSY2_Higgs/SUSY2_Higgs.root -Vn "Higgs" \
	-V ${OutDir}/SUSY2_ttLL-13TeV/SUSY2_ttLL-13TeV.root -Vn "t#bar{t}Z" \
	-V ${OutDir}/SUSY2_ttW-13TeV/SUSY2_ttW-13TeV.root -Vn "t#bar{t}W(W)" \
	-V ${OutDir}/SUSY2_tt/SUSY2_tt.root -Vn "t#bar{t}" \
	-V ${OutDir}/SUSY2_ZJets-Mad/SUSY2_ZJets-Mad.root -Vn "Z+Jets" \
	-V ${OutDir}/SUSY2_WZ/SUSY2_WZ.root -Vn "WZ" \
	-D ${OutDir}/${Name}_SUSY2/${Name}_SUSY2.root -Dn "Total"

DrawTogether --ROOTFile  ${Label} --StackDa --ComDa --NoSumDa -l ${DataLumi} --Normalize ${Log} --NoTH2 -N "${Name}-Data-SUSY2" -T " " -O ${OutDir} --RaTit "Data/ SM" \
	-D ${OutDir}/SUSY2_Sherpa_VVV-PileUp/SUSY2_Sherpa_VVV-PileUp.root -Dn "VVV" \
	-D ${OutDir}/SUSY2_ZZ-13TeV-PileUp/SUSY2_ZZ-13TeV-PileUp.root -Dn "ZZ" \
	-D ${OutDir}/SUSY2_tttt-PileUp/SUSY2_tttt-PileUp.root -Dn "tttt" \
	-D ${OutDir}/SUSY2_Higgs-PileUp/SUSY2_Higgs-PileUp.root -Dn "Higgs" \
	-D ${OutDir}/SUSY2_ttLL-13TeV-PileUp/SUSY2_ttLL-13TeV-PileUp.root -Dn "t#bar{t}Z" \
	-D ${OutDir}/SUSY2_ttW-13TeV-PileUp/SUSY2_ttW-13TeV-PileUp.root -Dn "t#bar{t}W(W)" \
	-D ${OutDir}/SUSY2_tt-PileUp/SUSY2_tt-PileUp.root -Dn "t#bar{t}" \
	-D ${OutDir}/SUSY2_ZJets-Mad-PileUp/SUSY2_ZJets-Mad-PileUp.root -Dn "Z+Jets" \
	-D ${OutDir}/SUSY2_WZ-PileUp/SUSY2_WZ-PileUp.root -Dn "WZ" \
	-V ${OutDir}/Data25ns/Data25ns.root -Vn "Data" 
	
DrawTogether  ${Label} --ROOTFile --Range --StackDa --ComDa --NoSumDa -l ${DataLumi} --Normalize ${Log} --NoTH2 -N "${Name}-Data-SUSY2_RNG" -T " " -O ${OutDir} --RaTit "Data/ SM" \
	-D ${OutDir}/SUSY2_Sherpa_VVV-PileUp/SUSY2_Sherpa_VVV-PileUp.root -Dn "VVV" \
	-D ${OutDir}/SUSY2_ZZ-13TeV-PileUp/SUSY2_ZZ-13TeV-PileUp.root -Dn "ZZ" \
	-D ${OutDir}/SUSY2_tttt-PileUp/SUSY2_tttt-PileUp.root -Dn "tttt" \
	-D ${OutDir}/SUSY2_Higgs-PileUp/SUSY2_Higgs-PileUp.root -Dn "Higgs" \
	-D ${OutDir}/SUSY2_ttLL-13TeV-PileUp/SUSY2_ttLL-13TeV-PileUp.root -Dn "t#bar{t}Z" \
	-D ${OutDir}/SUSY2_ttW-13TeV-PileUp/SUSY2_ttW-13TeV-PileUp.root -Dn "t#bar{t}W(W)" \
	-D ${OutDir}/SUSY2_tt-PileUp/SUSY2_tt-PileUp.root -Dn "t#bar{t}" \
	-D ${OutDir}/SUSY2_ZJets-Mad-PileUp/SUSY2_ZJets-Mad-PileUp.root -Dn "Z+Jets" \
	-D ${OutDir}/SUSY2_WZ-PileUp/SUSY2_WZ-PileUp.root -Dn "WZ" \
	-V ${OutDir}/Data25ns/Data25ns.root -Vn "Data" 
else

DrawTogether  ${Label} --StackDa --ComDa --NoSumDa -l ${DataLumi} --Normalize ${Log} --NoTH2 -N "${Name}-Data-SUSY2" -T " " -O ${OutDir} --RaTit "Data / MC" \
	-D ${OutDir}/SUSY2_Sherpa_VVV-PileUp/SUSY2_Sherpa_VVV-PileUp.root -Dn "VVV" \
	-D ${OutDir}/SUSY2_Sherpa_VV-PileUp/SUSY2_Sherpa_VV-PileUp.root -Dn "ZZ" \
	-D ${OutDir}/SUSY2_ttLL-13TeV-PileUp/SUSY2_ttLL-13TeV-PileUp.root -Dn "t#bar{t}Z" \
	-D ${OutDir}/SUSY2_Higgs-PileUp/SUSY2_Higgs-PileUp.root -Dn "Higgs" \
	-D ${OutDir}/SUSY2_tttt-PileUp/SUSY2_tttt-PileUp.root -Dn "tttt" \
	-D ${OutDir}/SUSY2_ttW-13TeV-PileUp/SUSY2_ttW-13TeV-PileUp.root -Dn "t#bar{t}W(W)" \
	-D ${OutDir}/SUSY2_tt-PileUp/SUSY2_tt-PileUp.root -Dn "t#bar{t}" \
	-D ${OutDir}/SUSY2_ZJets-Mad-PileUp/SUSY2_ZJets-Mad-PileUp.root -Dn "Z+Jets" \
	-D ${OutDir}/SUSY2_WZ-PileUp/SUSY2_WZ-PileUp.root -Dn "WZ" \
	-V ${OutDir}/Data25ns/Data25ns.root -Vn "Data" 

DrawTogether  ${Label} --NoCenRatio  --NoSumDa --Stack -l ${Lumi} --Normalize ${Log} --NoTH2 -N "${Name}-SUSY2" -T " " -O ${OutDir} --RaTit "Contribution" \
	-V ${OutDir}/SUSY2_Sherpa_VVV/SUSY2_Sherpa_VVV.root -Vn "VVV" \
	-V ${OutDir}/SUSY2_Sherpa_VV/SUSY2_Sherpa_VV.root -Vn "ZZ" \
	-V ${OutDir}/SUSY2_ttLL-13TeV/SUSY2_ttLL-13TeV.root -Vn "t#bar{t}Z" \
	-V ${OutDir}/SUSY2_Higgs/SUSY2_Higgs.root -Vn "Higgs" \
	-V ${OutDir}/SUSY2_tttt/SUSY2_tttt.root -Vn "tttt" \
	-V ${OutDir}/SUSY2_ttW-13TeV/SUSY2_ttW-13TeV.root -Vn "t#bar{t}W(W)" \
	-V ${OutDir}/SUSY2_tt/SUSY2_tt.root -Vn "t#bar{t}" \
	-V ${OutDir}/SUSY2_ZJets-Mad/SUSY2_ZJets-Mad.root -Vn "Z+Jets" \
	-V ${OutDir}/SUSY2_WZ/SUSY2_WZ.root -Vn "WZ" \
	-D ${OutDir}/${Name}_SUSY2/${Name}_SUSY2.root -Dn "Total" 
	
fi
	
	
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_${Name}-SUSY2/"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_${Name}-Data-SUSY2/"
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${OutDir}/DrawTogether_${Name}-Data-SUSY2_RNG/"


#~ rm -rf ${OutDir}/${Name}/
#~ mv ${OutDir}/${Name}_SUSY2 ${OutDir}/${Name}
#~ mv ${OutDir}/${Name}/${Name}_SUSY2.root ${OutDir}/${Name}/${Name}.root 

