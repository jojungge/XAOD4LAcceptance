import os
import commands
import math
import sys
import re

if  os.getenv("ROOTCOREBIN") == None:
    print "No ROOTCORE... Please setup ROOTCORE"
    exit(1)
User=os.getenv("USER")
Rucio = os.getenv("RUCIO_ACCOUNT")
RootCoreBin=os.getenv("ROOTCOREBIN")+"/../"

InputPath = RootCoreBin+"MC15XAOD4LSelection/data/mc15_13TeV/SampleLists/BG25ns/"
PileUpPath = RootCoreBin+"MC15XAOD4LSelection/data/mc15_13TeV/PileUpReweighting/25ns/"
SourcePath = "/ptmp/mpp/"+User+"/Grid/Data/PileUp/"


Files = os.listdir(InputPath)

def CopySourceFiles (DS):
	if  os.path.exists(SourcePath+DS) == True:
		print "rm -rf "+SourcePath+DS
		os.system ("rm -rf "+SourcePath+DS)
	os.system ("mkdir -p "+SourcePath+DS)
	PileUpFiles = os.listdir(SourcePath)
	ExistPileUpFiles = False
	#Copy the Files to the Source
	for P in PileUpFiles:
		if os.path.isdir(SourcePath+P) == False or DS not in P or Rucio not in P:
			continue
		Source = os.listdir(SourcePath+P)
		for S in Source:
			if ".root" not in S:
				continue
			ExistPileUpFiles = True
			os.system( "cp "+SourcePath+P+"/"+S+" "+SourcePath+DS)
	return ExistPileUpFiles
	
def DownloadSourceFiles (DS):
	os.chdir(SourcePath)
	for a in range(5,6):
		Day=""
		if a<10:
			Day="0"+str(a)
		else:
			Day=str(a)
		print DS
		os.system ("rucio download user."+Rucio+":user."+Rucio+".2015-12-"+Day+".PileUp."+DS+"_METADATA  --ndownloader 100")
for F in Files:
	if "SUSY" in F or os.path.isfile(InputPath+F)==False:
		continue
	List = open (InputPath+F , "r")
	for DS in List:
		if DS [0]== "#" or len(DS)==1:
			continue
		L = DS.rstrip("\n").rstrip("/")
		L =  L [  0 : L.find(".merge") ]+L [ L.rfind("_"):  len(L) ]	
		if os.path.exists(PileUpPath+L+".root"):
			continue
		DownloadSourceFiles(L)			
		if CopySourceFiles (L) == False:
			DownloadSourceFiles(L)
			if CopySourceFiles (L) == False:
				os.system ("echo \""+DS.rstrip("\n")+"\" >> "+PileUpPath+"/MISSING.txt")
				continue
		os.chdir(SourcePath+L)
		os.system ("hadd "+L+".root *.root")
		os.system ("mv "+SourcePath+L+"/"+L+".root "+PileUpPath)
			
