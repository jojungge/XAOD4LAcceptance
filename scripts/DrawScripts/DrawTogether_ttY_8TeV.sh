#!/bin/bash

if [ $# -lt 1 ];then
	echo "Please give the analysis date"
	exit
fi
ClusterDir=/ptmp/mpp/${USER}/Cluster/Output/$1
CentralPhotonSample_8TeV="$ClusterDir/ttgammaPt80-8TeV/ttgammaPt80-8TeV.root"
if [ -d "${ClusterDir}/Photon-Variation/8-TeV/" ];then
	echo "rm -rf ${ClusterDir}/Photon-Variation/8-TeV/"
	rm -rf ${ClusterDir}/Photon-Variation/8-TeV/
fi
if [ -e ${CentralPhotonSample_8TeV} ]; then
	DrawTogether --NoTH2 --NoInt -N Alpsfactor -Vn Up -Vn Down -D "${CentralPhotonSample_8TeV}" -V "${ClusterDir}/ttgammaPt80-8TeV_alpsUP/ttgammaPt80-8TeV_alpsUP.root"  -V "${ClusterDir}/ttgammaPt80-8TeV_alpsDOWN/ttgammaPt80-8TeV_alpsDOWN.root" -O "${ClusterDir}/Photon-Variation/8-TeV/" 
	DrawTogether --NoTH2 --NoInt -N FSR -Vn More -Vn Less -D "${CentralPhotonSample_8TeV}" -V "${ClusterDir}/ttgammaPt80-8TeV_moreFSR/ttgammaPt80-8TeV_moreFSR.root"  -V "${ClusterDir}/ttgammaPt80-8TeV_lessFSR/ttgammaPt80-8TeV_lessFSR.root" -O "${ClusterDir}/Photon-Variation/8-TeV/" 
	DrawTogether --NoTH2 --NoInt -N Scalefactor -Vn Up -Vn Down -D "${CentralPhotonSample_8TeV}" -V "${ClusterDir}/ttgammaPt80-8TeV_scaleUP/ttgammaPt80-8TeV_scaleUP.root"  -V "${ClusterDir}/ttgammaPt80-8TeV_scaleDOWN/ttgammaPt80-8TeV_scaleDOWN.root" -O "${ClusterDir}/Photon-Variation/8-TeV/" 
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/8-TeV/DrawTogether_Alpsfactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/8-TeV/DrawTogether_FSR"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/8-TeV/DrawTogether_Scalefactor"
else
	echo "No Central Sample found"
fi

