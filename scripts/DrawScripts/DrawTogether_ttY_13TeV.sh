#!/bin/bash

if [ $# -lt 1 ];then
	echo "Please give the analysis date"
	exit
fi
ClusterDir=/ptmp/mpp/${USER}/Cluster/Output/$1
CentralPhotonSamplePt80_13TeV=$ClusterDir/ttgammaPt80-13TeV/ttgammaPt80-13TeV.root
CentralPhotonSamplePt10_13TeV=$ClusterDir/ttgammaPt10-13TeV/ttgammaPt10-13TeV.root
if [ -d "${ClusterDir}/Photon-Variation/13-TeV/" ];then
	echo "rm -rf ${ClusterDir}/Photon-Variation/13-TeV/"
	rm -rf ${ClusterDir}/Photon-Variation/13-TeV/
fi
if [ -e ${CentralPhotonSamplePt80_13TeV} ] && [ -e ${CentralPhotonSamplePt10_13TeV} ];then
	DrawTogether --AllLogY --ComDa --NoSumDa --NoTH2 --NoInt -l 1000 -N "ttY_pt10-80" -T "Comparison of the filtered and unfiltered t#bar{t}#gamma samples" -D ${CentralPhotonSamplePt80_13TeV} -Dn "p_{T}(#gamma) > 80 GeV" -V ${CentralPhotonSamplePt10_13TeV} -Vn "p_{T}(#gamma) > 10 GeV" -O ${ClusterDir} 
fi
if [ -e ${CentralPhotonSamplePt80_13TeV} ];then
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -D "${CentralPhotonSamplePt80_13TeV}" -T "AlpsFactor systematics with p_{T}(#gamma) > 80 GeV" -N Alpsfactor -Vn Up -Vn Down  -V "${ClusterDir}/ttgammaPt80-13TeV_alpsUP/ttgammaPt80-13TeV_alpsUP.root"  -V "${ClusterDir}/ttgammaPt80-13TeV_alpsDOWN/ttgammaPt80-13TeV_alpsDOWN.root" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt80/" 
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -D "${CentralPhotonSamplePt80_13TeV}" -T "FSR systematics with p_{T}(#gamma) > 80 GeV" -N FSR -Vn More -Vn Less  -V "${ClusterDir}/ttgammaPt80-13TeV_moreFSR/ttgammaPt80-13TeV_moreFSR.root"  -V "${ClusterDir}/ttgammaPt80-13TeV_lessFSR/ttgammaPt80-13TeV_lessFSR.root" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt80/" 
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -D "${CentralPhotonSamplePt80_13TeV}" -T "ScaleFactor systematics p_{T}(#gamma) > 80 GeV" -N Scalefactor -Vn Up -Vn Down  -V "${ClusterDir}/ttgammaPt80-13TeV_scaleUP/ttgammaPt80-13TeV_scaleUP.root"  -V "${ClusterDir}/ttgammaPt80-13TeV_scaleDOWN/ttgammaPt80-13TeV_scaleDOWN.root" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt80/" 
	DrawUncertainties -N "ttY" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt80" -C "${CentralPhotonSamplePt80_13TeV}" \
					--Sys "${ClusterDir}/ttgammaPt80-13TeV_scaleUP/ttgammaPt80-13TeV_scaleUP.root" \
					--Sys "${ClusterDir}/ttgammaPt80-13TeV_alpsUP/ttgammaPt80-13TeV_alpsUP.root" \
					--Sys "${ClusterDir}/ttgammaPt80-13TeV_moreFSR/ttgammaPt80-13TeV_moreFSR.root" \
					--Sys "${ClusterDir}/ttgammaPt80-13TeV_scaleDOWN/ttgammaPt80-13TeV_scaleDOWN.root" \
					--Sys "${ClusterDir}/ttgammaPt80-13TeV_alpsDOWN/ttgammaPt80-13TeV_alpsDOWN.root" \
					--Sys "${ClusterDir}/ttgammaPt80-13TeV_lessFSR/ttgammaPt80-13TeV_lessFSR.root"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt80/DrawTogether_Alpsfactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt80/DrawTogether_FSR"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt80/DrawTogether_Scalefactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt80/Systematics_ttY"
fi
if [ -e ${CentralPhotonSamplePt10_13TeV} ];then
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -D "${CentralPhotonSamplePt10_13TeV}" -T "AlpsFactor systematics with p_{T}(#gamma) > 10 GeV" -N Alpsfactor -Vn Up -Vn Down  -V "${ClusterDir}/ttgammaPt10-13TeV_alpsUP/ttgammaPt10-13TeV_alpsUP.root"  -V "${ClusterDir}/ttgammaPt10-13TeV_alpsDOWN/ttgammaPt10-13TeV_alpsDOWN.root" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt10" 
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -D "${CentralPhotonSamplePt10_13TeV}" -T "FSR systematics with p_{T}(#gamma) > 10 GeV" -N FSR -Vn More -Vn Less  -V "${ClusterDir}/ttgammaPt10-13TeV_moreFSR/ttgammaPt10-13TeV_moreFSR.root"  -V "${ClusterDir}/ttgammaPt10-13TeV_lessFSR/ttgammaPt10-13TeV_lessFSR.root" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt10" 
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -D "${CentralPhotonSamplePt10_13TeV}" -T "ScaleFactor systematics p_{T}(#gamma) > 10 GeV" -N Scalefactor -Vn Up -Vn Down  -V "${ClusterDir}/ttgammaPt10-13TeV_scaleUP/ttgammaPt10-13TeV_scaleUP.root"  -V "${ClusterDir}/ttgammaPt10-13TeV_scaleDOWN/ttgammaPt10-13TeV_scaleDOWN.root" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt10" 

	DrawUncertainties -N "ttY" -O "${ClusterDir}/Photon-Variation/13-TeV/Pt10" -C "${CentralPhotonSamplePt10_13TeV}" \
					--Sys "${ClusterDir}/ttgammaPt10-13TeV_scaleUP/ttgammaPt10-13TeV_scaleUP.root" \
					--Sys "${ClusterDir}/ttgammaPt10-13TeV_alpsUP/ttgammaPt10-13TeV_alpsUP.root" \
					--Sys "${ClusterDir}/ttgammaPt10-13TeV_moreFSR/ttgammaPt10-13TeV_moreFSR.root" \
					--Sys "${ClusterDir}/ttgammaPt10-13TeV_scaleDOWN/ttgammaPt10-13TeV_scaleDOWN.root" \
					--Sys "${ClusterDir}/ttgammaPt10-13TeV_alpsDOWN/ttgammaPt10-13TeV_alpsDOWN.root" \
					--Sys "${ClusterDir}/ttgammaPt10-13TeV_lessFSR/ttgammaPt10-13TeV_lessFSR.root"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt10/DrawTogether_Alpsfactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt10/DrawTogether_FSR"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt10/DrawTogether_Scalefactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Photon-Variation/13-TeV/Pt10/Systematics_ttY"
fi
