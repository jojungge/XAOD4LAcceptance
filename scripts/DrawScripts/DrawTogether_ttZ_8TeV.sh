#!/bin/bash
ClusterDir=/ptmp/mpp/${USER}/Cluster/Output/$1
CentralZSample_8TeV=${ClusterDir}/ttZ-8TeV/ttZ-8TeV.root
if [ -d "${ClusterDir}/Z-Variation/8-TeV/" ];then
	echo "rm -rf ${ClusterDir}/Z-Variation/8-TeV/"
	rm -rf ${ClusterDir}/Z-Variation/8-TeV/
fi
if [ -e ${CentralZSample_8TeV} ];then
	DrawTogether --NoTH2 --NoInt -N FSR -Vn More -Vn Less -D "${CentralZSample_8TeV}" -V "${ClusterDir}/ttZ-8TeV_MoreFSR/ttZ-8TeV_MoreFSR.root" -V "${ClusterDir}/ttZ-8TeV_LessFSR/ttZ-8TeV_LessFSR.root" -O "${ClusterDir}/Z-Variation/8-TeV/" 
	DrawTogether --NoTH2 --NoInt -N Scalefactor -Vn Up -Vn Down -D "${CentralZSample_8TeV}" -V "${ClusterDir}/ttZ-8TeV_scalefactUP/ttZ-8TeV_scalefactUP.root" -V "${ClusterDir}/ttZ-8TeV_scalefactDOWN/ttZ-8TeV_scalefactDOWN.root" -O "$ClusterDir/Z-Variation/8-TeV/" 
	DrawTogether --NoTH2 --NoInt -N Alpsfactor -Vn Up -Vn Down -D "${CentralZSample_8TeV}" -V "${ClusterDir}/ttZ-8TeV_alpsfactUP/ttZ-8TeV_alpsfactUP.root" -V "${ClusterDir}/ttZ-8TeV_alpsfactDOWN/ttZ-8TeV_alpsfactDOWN.root" -O "$ClusterDir/Z-Variation/8-TeV/" 
	DrawTogether --NoTH2 --NoInt -N XqCut -Vn Up -Vn Down -D "${CentralZSample_8TeV}" -V "${ClusterDir}/ttZ-8TeV_xqcutUP/ttZ-8TeV_xqcutUP.root" -V "${ClusterDir}/ttZ-8TeV_xqcutDOWN/ttZ-8TeV_xqcutDOWN.root" -O "$ClusterDir/Z-Variation/8-TeV/" 
	DrawUncertainties -N "ttZ" -O "${ClusterDir}/Z-Variation/8-TeV" -C "${CentralZSample_8TeV}" \
					--Sys "${ClusterDir}/ttZ-8TeV_scalefactUP/ttZ-8TeV_scalefactUP.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_MoreFSR/ttZ-8TeV_MoreFSR.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_alpsfactUP/ttZ-8TeV_alpsfactUP.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_xqcutUP/ttZ-8TeV_xqcutUP.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_LessFSR/ttZ-8TeV_LessFSR.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_scalefactDOWN/ttZ-8TeV_scalefactDOWN.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_xqcutDOWN/ttZ-8TeV_xqcutDOWN.root" \
					--Sys "${ClusterDir}/ttZ-8TeV_alpsfactDOWN/ttZ-8TeV_alpsfactDOWN.root"
	
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/8-TeV/DrawTogether_XqCut"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/8-TeV/DrawTogether_Alpsfactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/8-TeV/DrawTogether_FSR"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/8-TeV/DrawTogether_Scalefactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/8-TeV/Systematics_ttZ"
fi
