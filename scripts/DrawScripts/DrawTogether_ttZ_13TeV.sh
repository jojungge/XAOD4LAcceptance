#!/bin/bash
ClusterDir=/ptmp/mpp/${USER}/Cluster/Output/$1
CentralZSample_13TeV=${ClusterDir}/ttZ-13TeV/ttZ-13TeV.root

if [ -d "${ClusterDir}/Z-Variation/13-TeV/" ];then
	echo "rm -rf ${ClusterDir}/Z-Variation/13-TeV/"
	rm -rf ${ClusterDir}/Z-Variation/13-TeV/
fi
if [ -e ${CentralZSample_13TeV} ];then

	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -N  FSR -Vn More -Vn Less -D "${CentralZSample_13TeV}" -V "${ClusterDir}/ttZ-13TeV_MoreFSR/ttZ-13TeV_MoreFSR.root" -V "${ClusterDir}/ttZ-13TeV_LessFSR/ttZ-13TeV_LessFSR.root" -O "${ClusterDir}/Z-Variation/13-TeV/" 
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -N Scalefactor -Vn Up -Vn Down -D "${CentralZSample_13TeV}" -V "${ClusterDir}/ttZ-13TeV_scalefactUP/ttZ-13TeV_scalefactUP.root" -V "${ClusterDir}/ttZ-13TeV_scalefactDOWN/ttZ-13TeV_scalefactDOWN.root" -O "$ClusterDir/Z-Variation/13-TeV/"
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -N Alpsfactor -Vn Up -Vn Down -D "${CentralZSample_13TeV}" -V "${ClusterDir}/ttZ-13TeV_alpsfactUP/ttZ-13TeV_alpsfactUP.root" -V "${ClusterDir}/ttZ-13TeV_alpsfactDOWN/ttZ-13TeV_alpsfactDOWN.root" -O "$ClusterDir/Z-Variation/13-TeV/" 
	DrawTogether --ComDa --NoSumDa --NoTH2 --NoInt -l 1000. -N XqCut -Vn Up -Vn Down -D "${CentralZSample_13TeV}" -V "${ClusterDir}/ttZ-13TeV_xqcutUP/ttZ-13TeV_xqcutUP.root" -V "${ClusterDir}/ttZ-13TeV_xqcutDOWN/ttZ-13TeV_xqcutDOWN.root" -O "$ClusterDir/Z-Variation/13-TeV/" 

	DrawUncertainties -N "ttZ" -O "${ClusterDir}/Z-Variation/13-TeV" -C "${CentralZSample_13TeV}" \
					--Sys "${ClusterDir}/ttZ-13TeV_scalefactUP/ttZ-13TeV_scalefactUP.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_MoreFSR/ttZ-13TeV_MoreFSR.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_alpsfactUP/ttZ-13TeV_alpsfactUP.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_xqcutUP/ttZ-13TeV_xqcutUP.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_LessFSR/ttZ-13TeV_LessFSR.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_scalefactDOWN/ttZ-13TeV_scalefactDOWN.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_xqcutDOWN/ttZ-13TeV_xqcutDOWN.root" \
					--Sys "${ClusterDir}/ttZ-13TeV_alpsfactDOWN/ttZ-13TeV_alpsfactDOWN.root"
	
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/13-TeV/DrawTogether_XqCut"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/13-TeV/DrawTogether_Alpsfactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/13-TeV/DrawTogether_FSR"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/13-TeV/DrawTogether_Scalefactor"
	${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/Z-Variation/13-TeV/Systematics_ttZ"
fi
