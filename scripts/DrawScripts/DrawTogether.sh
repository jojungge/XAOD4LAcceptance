#!/bin/bash


ClusterDir=/ptmp/mpp/${USER}/Cluster/Output/$1
GridDir=/ptmp/mpp/${USER}/Grid/Output/$2




#~ if [ ! -d ${GridDir} ];then
	#~ echo "First merge all background files"
	#~ ${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/MergeHistosFromGrid.sh $2
#~ fi
LumiData="165.33"
# DrawTogether  --Stack -l ${Lumi} --Normalize ${Log} -l ${LumiData} -N "Data-BG" -T "" -O ${ClusterDir} --RaTit "SM / Data " \
	-V ${ClusterDir}/Sherpa_VVV/Sherpa_VVV.root -Vn "VVV" \
	-V ${ClusterDir}/ZZ-13TeV/ZZ-13TeV.root -Vn "ZZ" \
	-V ${ClusterDir}/VH/VH.root -Vn "VH" \
	-V ${ClusterDir}/ttLL-13TeV/ttLL-13TeV.root -Vn "t#bar{t}Z" \
	-V ${ClusterDir}/Higgs/Higgs.root -Vn "Higgs" \
	-V ${ClusterDir}/tttt/tttt.root -Vn "tttt" \
	-V ${ClusterDir}/ttW-13TeV/ttW-13TeV.root -Vn "t#bar{t}W(W)" \
	-D ${ClusterDir}/Data/Data.root -Dn "Data" 
${ROOTCOREBIN}/../CanvasPlot/scripts/SortPlots.sh "${ClusterDir}/DrawTogether_Data-BG/"


NLSP="500 700 800 900 1000" # 1100 1200 1300"
NLSP="900"
LSP="10 50 100 200 400 490 600 690 700 790 800 890 990 1000 1090 1190 1200 1290"
for N in ${NLSP};do
	for L in ${LSP};do
		if [ $L -ge $N ]; then
			break
		fi
	$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "C1C1" ${N} ${L} "LLE12k" &
	done
done
exit







CentralZSample_8TeV=$ClusterDir/ttZ-8TeV/ttZ-8TeV.root
CentralZSample_13TeV=$ClusterDir/ttZ-13TeV/ttZ-13TeV.root
CentralPhotonSample_8TeV=$ClusterDir/ttgammaPt80-8TeV/ttgammaPt80-8TeV.root
CentralPhotonSamplePt80_13TeV=$ClusterDir/ttgammaPt80-13TeV/ttgammaPt80-13TeV.root
CentralPhotonSamplePt10_13TeV=$ClusterDir/ttgammaPt10-13TeV/ttgammaPt10-13TeV.root

${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/DrawScripts/DrawTogether_ttZ_8TeV.sh $1 &
${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/DrawScripts/DrawTogether_ttY_8TeV.sh $1 &
${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/DrawScripts/DrawTogether_ttY_13TeV.sh $1 
${ROOTCOREBIN}/../XAOD4LAcceptance/scripts/DrawScripts/DrawTogether_ttZ_13TeV.sh $1 

Create_Z-G  -G "${CentralPhotonSamplePt80_13TeV}" -Z "${CentralZSample_13TeV}" -O "$ClusterDir/ttZ-ttY/Ypt80" -N Central \
				--SysZ "${ClusterDir}/ttZ-13TeV_scalefactUP/ttZ-13TeV_scalefactUP.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_MoreFSR/ttZ-13TeV_MoreFSR.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_alpsfactUP/ttZ-13TeV_alpsfactUP.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_xqcutUP/ttZ-13TeV_xqcutUP.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_LessFSR/ttZ-13TeV_LessFSR.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_scalefactDOWN/ttZ-13TeV_scalefactDOWN.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_xqcutDOWN/ttZ-13TeV_xqcutDOWN.root" \
				--SysZ "${ClusterDir}/ttZ-13TeV_alpsfactDOWN/ttZ-13TeV_alpsfactDOWN.root" \
				--SysG "${ClusterDir}/ttgammaPt80-13TeV_scaleUP/ttgammaPt80-13TeV_scaleUP.root" \
				--SysG "${ClusterDir}/ttgammaPt80-13TeV_alpsUP/ttgammaPt80-13TeV_alpsUP.root" \
				--SysG "${ClusterDir}/ttgammaPt80-13TeV_moreFSR/ttgammaPt80-13TeV_moreFSR.root" \
				--SysG "${ClusterDir}/ttgammaPt80-13TeV_scaleDOWN/ttgammaPt80-13TeV_scaleDOWN.root" \
				--SysG "${ClusterDir}/ttgammaPt80-13TeV_alpsDOWN/ttgammaPt80-13TeV_alpsDOWN.root" \
				--SysG "${ClusterDir}/ttgammaPt80-13TeV_lessFSR/ttgammaPt80-13TeV_lessFSR.root"
Create_Z-G  -G "${CentralPhotonSamplePt10_13TeV}" -Z "${CentralZSample_13TeV}" -O "$ClusterDir/ttZ-ttY/Ypt10" -N Central \
			--SysZ "${ClusterDir}/ttZ-13TeV_scalefactUP/ttZ-13TeV_scalefactUP.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_scalefactDOWN/ttZ-13TeV_scalefactDOWN.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_MoreFSR/ttZ-13TeV_MoreFSR.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_LessFSR/ttZ-13TeV_LessFSR.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_alpsfactUP/ttZ-13TeV_alpsfactUP.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_alpsfactDOWN/ttZ-13TeV_alpsfactDOWN.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_xqcutUP/ttZ-13TeV_xqcutUP.root" \
			--SysZ "${ClusterDir}/ttZ-13TeV_xqcutDOWN/ttZ-13TeV_xqcutDOWN.root" \
			--SysG "${ClusterDir}/ttgammaPt10-13TeV_scaleUP/ttgammaPt10-13TeV_scaleUP.root" \
			--SysG "${ClusterDir}/ttgammaPt10-13TeV_scaleDOWN/ttgammaPt10-13TeV_scaleDOWN.root" \
			--SysG "${ClusterDir}/ttgammaPt10-13TeV_moreFSR/ttgammaPt10-13TeV_moreFSR.root" \
			--SysG "${ClusterDir}/ttgammaPt10-13TeV_lessFSR/ttgammaPt10-13TeV_lessFSR.root" \
			--SysG "${ClusterDir}/ttgammaPt10-13TeV_alpsUP/ttgammaPt10-13TeV_alpsUP.root" \
			--SysG "${ClusterDir}/ttgammaPt10-13TeV_alpsDOWN/ttgammaPt10-13TeV_alpsDOWN.root" \

Create_Z-G  -G "${CentralPhotonSamplePt10_13TeV}" -Z "${CentralZSample_13TeV}" -O "$ClusterDir/ttZ-ttY/Ypt10" -N Central \
				--SysZUp "${ClusterDir}/ttZ-13TeV_scalefactUP/ttZ-13TeV_scalefactUP.root" \
				--SysZUp "${ClusterDir}/ttZ-13TeV_MoreFSR/ttZ-13TeV_MoreFSR.root" \
				--SysZUp "${ClusterDir}/ttZ-13TeV_alpsfactUP/ttZ-13TeV_alpsfactUP.root" \
				--SysZUp "${ClusterDir}/ttZ-13TeV_xqcutUP/ttZ-13TeV_xqcutUP.root" \
				--SysZDown "${ClusterDir}/ttZ-13TeV_scalefactDOWN/ttZ-13TeV_scalefactDOWN.root" \
				--SysZDown "${ClusterDir}/ttZ-13TeV_LessFSR/ttZ-13TeV_LessFSR.root" \
				--SysZDown "${ClusterDir}/ttZ-13TeV_alpsfactDOWN/ttZ-13TeV_alpsfactDOWN.root" \
				--SysZDown "${ClusterDir}/ttZ-13TeV_xqcutDOWN/ttZ-13TeV_xqcutDOWN.root" \
				--SysGUp "${ClusterDir}/ttgammaPt10-13TeV_scaleUP/ttgammaPt10-13TeV_scaleUP.root" \
				--SysGUp "${ClusterDir}/ttgammaPt10-13TeV_moreFSR/ttgammaPt10-13TeV_moreFSR.root" \
				--SysGUp "${ClusterDir}/ttgammaPt10-13TeV_alpsUP/ttgammaPt10-13TeV_alpsUP.root" \
				--SysGDown "${ClusterDir}/ttgammaPt10-13TeV_scaleDOWN/ttgammaPt10-13TeV_scaleDOWN.root" \
				--SysGDown "${ClusterDir}/ttgammaPt10-13TeV_lessFSR/ttgammaPt10-13TeV_lessFSR.root" \
				--SysGDown "${ClusterDir}/ttgammaPt10-13TeV_alpsDOWN/ttgammaPt10-13TeV_alpsDOWN.root" \
				
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Gluino" 1400 10 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Gluino" 1400 100 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Gluino" 1600 1400 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Gluino" 1200 800 "133-233" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Wino" 650 400 "133-233" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Wino" 800 400 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_Wino" 800 200 "121-122" 

$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_WinoPM" 650 400 "133-233" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_WinoPM" 800 400 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} "DAOD_WinoPM" 800 200 "121-122" 

$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} Gluino 1400 10 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} Gluino 1400 100 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} Gluino 1600 1400 "121-122" 

$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} WinoPM 650 400 "133-233" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} WinoPM 800 400 "121-122" 
$ROOTCOREBIN/../CanvasPlot/scripts/SUSY_Background.sh ${ClusterDir} ${GridDir} WinoPM 800 200 "121-122" 

#Merging the DAOD plots with the Normal Plots
Samples="Gluino_1400_10_121-122 Gluino_1400_100_121-122 Gluino_1600_1400_121-122 WinoPM_800_200_121-122 WinoPM_650_400_133-233"
for s in ${Samples};do
	if [ -d ${ClusterDir}/DAOD_${s}/ ];then
		rm -rf ${ClusterDir}/${s}/Histograms/TruthLevel
		rm -rf ${ClusterDir}/${s}/CmpToBg/TruthLevel
		mv -f ${ClusterDir}/DAOD_${s}/Histograms/TruthLevel ${ClusterDir}/${s}/Histograms/TruthLevel
		mv -f ${ClusterDir}/DAOD_${s}/CmpToBg/TruthLevel ${ClusterDir}/${s}CmpToBg/TruthLevel		
		rm -rf ${ClusterDir}/DAOD_${s}/
	fi 
done
